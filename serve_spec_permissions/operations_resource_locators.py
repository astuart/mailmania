# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.


# When a request comes in, first we need to *authenticate* the user that is requesting (i.e. prove they are who they say)
# THEN we need to *authorize* that user to access the resource that is the target of the request
# SO... we need a way to inspect the request, and identify the resource that is being requested
# This is how to identify the resource being requested, which varies from operation to operation.
# SO this specifies which request field value defines the resource being requested, which in turn allows authorization
#urifields['email']
# the value is a lambda function that returns the correct value from the request_handler_args, which is passed
# in as a parameter at the location that is calling the lambda function.

operation_resource_locators = {
'permissionsVARVARVARVARGET':      (lambda request_handler_args: (request_handler_args['uri_fields'].get('resource_id', None), 'resource_id')),
'permissionsVARVARVARVARPOST':     (lambda request_handler_args: (request_handler_args['uri_fields'].get('resource_id', None), 'resource_id')),
'permissionsVARVARVARVARDELETE':   (lambda request_handler_args: (request_handler_args['uri_fields'].get('resource_id', None), 'resource_id')),
}

