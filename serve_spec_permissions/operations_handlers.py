# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from serve_security.jsonwebtoken import json_web_token as jwtoken
from serve_spec_permissions.operations_servers import permission_delete, permission_query, permission_set
from serve_security.authorization_handlers import requires_serverowner, requires_userowner, requires_listowner, \
    requires_listmember, requires_listmoderator

# When a request comes in, specserver works out which operation is being requested.
# It then executes the list of callables for that operation.
# if auth function returns True, original request will be proxied to upstream application server
# if auth function does not return True, then a 403 Forbidden is returned
# if an inbound operation is not found in this list then a 404 is returned

# a request handler MUST be compatible with the following argument signature:
# func(req, resp, config, *args, uri_fields=None, form_fields=None, **kwargs)

# changes that you make to the response will fall through to the next handler until either all handlers
# complete or one of them returns the response

# the operation names must precisely match those found in the Swagger spec.

# you can immediately return a simplehttperror such as code403Forbidden if you like


operation_handlers = {
    'permissionsVARVARVARVARGET':
        [jwtoken.validate, (requires_serverowner,), permission_query],

    'permissionsVARVARVARVARPOST':
        [jwtoken.validate, (requires_serverowner,), permission_set],

    'permissionsVARVARVARVARDELETE':
        [jwtoken.validate, (requires_serverowner,), permission_delete],
}


