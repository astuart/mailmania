# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import Column, String, Integer, Enum
from serve_spec_permissions.db_global import db_permissions

class Permission(db_permissions.Base):

    __tablename__ = "permissions"

    id = Column(Integer, primary_key=True)
    user_id = Column(String, nullable=False)
    role = Column(String, nullable=False)
    resource_id = Column(String, nullable=False)
    resource_type = Column(Enum('list', 'domain', 'archive','server', name='resource_type'), nullable=False)
    # http://techspot.zzzeek.org/2011/01/14/the-enum-recipe/
    # On backends that support ENUM, a metadata.create_all() emits the appropriate DDL to
    # generate the type. The 'name' field of the Enum is used as the name of the type created in PG:


