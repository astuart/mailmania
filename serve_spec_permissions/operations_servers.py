# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from serve_security.permissions import permissions
from falcon.http_error import HTTPError
import falcon.status_codes as status
import logging

logger = logging.getLogger('root.' + __name__)


def _validate_request_params(uri_fields=None):
    logger.debug(uri_fields)
    valid, reason = permissions().validate_params(  role=uri_fields.get('role', None),
                                                    resource_id=uri_fields.get('resource_id', None),
                                                    resource_type=uri_fields.get('resource_type', None),
                                                    user_identifier=uri_fields.get('user_identifier', None))
    if not valid:
        raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=reason)

def permission_query(**request_handler_args):
    # this a request handler
    uri_fields = request_handler_args['uri_fields']
    req = request_handler_args['req']
    if req.method != 'GET':
        raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
    logger.debug('Request to query permissions: {}'.format(uri_fields))
    _validate_request_params(uri_fields=uri_fields)
    _result =     permissions().query(  role=uri_fields['role'],
                                        resource_id=uri_fields['resource_id'],
                                        resource_type=uri_fields['resource_type'],
                                        user_identifier=uri_fields['user_identifier'])
    # _result is a boolean
    if _result:
        request_handler_args['resp'].status = status.HTTP_204
    else:
        reason = 'Query permissions failed'
        raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=reason)

def permission_set(**request_handler_args):
    # this a request handler
    uri_fields = request_handler_args['uri_fields']
    req = request_handler_args['req']
    if req.method != 'POST':
        raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
    logger.debug('Request to set permissions: {}'.format(uri_fields))
    _validate_request_params(uri_fields=uri_fields)
    _result = permissions().set(role=uri_fields['role'],
                    resource_id=uri_fields['resource_id'],
                    resource_type=uri_fields['resource_type'],
                    user_identifier=uri_fields['user_identifier'])
    logger.debug('_result.status_code SET')
    if _result:
        request_handler_args['resp'].status = status.HTTP_201
    else:
        reason = 'Set permissions failed'
        raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=reason)

def permission_delete(**request_handler_args):
    # this a request handler
    uri_fields = request_handler_args['uri_fields']
    req = request_handler_args['req']
    if req.method != 'DELETE':
        raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
    logger.debug('Request to delete permissions: {}'.format(uri_fields))
    _validate_request_params(uri_fields=uri_fields)
    _result = permissions().delete( role=uri_fields['role'],
                                    resource_id=uri_fields['resource_id'],
                                    resource_type=uri_fields['resource_type'],
                                    user_identifier=uri_fields['user_identifier'])
    if _result:
        request_handler_args['resp'].status = status.HTTP_204
    else:
        reason = 'Delete permissions failed'
        raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=reason)

