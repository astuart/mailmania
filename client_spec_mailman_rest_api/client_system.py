# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import requests
from client_common.utils import build_url



class SystemOPTIONS():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('system', netloc=netloc)
        return requests.options(url, headers=headers)

class SystemGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('system', netloc=netloc)
        return requests.get(url, headers=headers)

class SystemPreferencesGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('system', 'preferences', netloc=netloc)
        return requests.get(url, headers=headers)

class SystemConfigurationGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('system', 'configuration', netloc=netloc)
        return requests.get(url, headers=headers)


class SystemConfigurationVARGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('system', 'configuration', urlvars_dict['section'], netloc=netloc)
        return requests.get(url, headers=headers)

