# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import requests
from client_common.utils import build_url

class ListsGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', netloc=netloc)
        return requests.get(url, headers=headers)

class ListsPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', netloc=netloc)
        return requests.post(url, headers=headers, data=data)

class ListsStylesGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', 'styles', netloc=netloc)
        return requests.get(url, headers=headers)

class ListsVARArchiversGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'archivers', netloc=netloc)
        return requests.get(url, headers=headers)

class ListsVARArchiversPATCH():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'archivers', netloc=netloc)
        return requests.patch(url, headers=headers, data=data)

class ListsVARArchiversPUT():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'archivers', netloc=netloc)
        return requests.put(url, headers=headers, data=data)

class ListsVARConfigGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'config', netloc=netloc)
        return requests.get(url, headers=headers, data=data)

class ListsVARConfigPATCH():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'config', netloc=netloc)
        return requests.patch(url, headers=headers, data=data)

class ListsVARConfigPUT():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'config', netloc=netloc)
        return requests.put(url, headers=headers, data=data)

class ListsVARConfigVARGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'config', urlvars_dict['config_variable'], netloc=netloc)
        return requests.get(url, headers=headers)


class ListsVARConfigVARPATCH():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'config', urlvars_dict['config_variable'], netloc=netloc)
        return requests.patch(url, headers=headers, data=data)

class ListsVARDELETE():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], netloc=netloc)
        return requests.delete(url, headers=headers)

class ListsVARGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], netloc=netloc)
        return requests.get(url, headers=headers)


class ListsVARHeldGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'held', netloc=netloc)
        return requests.get(url, headers=headers)


class ListsVARHeldVARGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'held', urlvars_dict['request_id'], netloc=netloc)
        return requests.get(url, headers=headers)


class ListsVARHeldVARPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'held', urlvars_dict['request_id'], netloc=netloc)
        return requests.post(url, headers=headers, data=data)


class ListsVARRequestsGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'requests', netloc=netloc)
        return requests.get(url, headers=headers)


class ListsVARRequestsVARGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'requests', urlvars_dict['request_id'], netloc=netloc)
        return requests.get(url, headers=headers)


class ListsVARRequestsVARPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'requests', urlvars_dict['request_id'], netloc=netloc)
        return requests.post(url, headers=headers, data=data)


class ListsVARRosterMemberGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'roster', 'member', netloc=netloc)
        return requests.get(url, headers=headers)


class ListsVARRosterModeratorGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'roster', 'moderator', netloc=netloc)
        return requests.get(url, headers=headers)


class ListsVARRosterOwnerGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('lists', urlvars_dict['list_id'], 'roster', 'owner', netloc=netloc)
        return requests.get(url, headers=headers)



