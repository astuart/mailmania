# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import requests
from client_common.utils import build_url

class UsersGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', netloc=netloc)
        print(url)
        return requests.get(url, headers=headers)

class UsersPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', netloc=netloc)
        return requests.post(url, headers=headers, data=data)

class UsersVARAddressesGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', urlvars_dict['user_identifier'], 'addresses', netloc=netloc)
        return requests.get(url, headers=headers)

class UsersVARAddressesPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', urlvars_dict['user_identifier'], 'addresses', netloc=netloc)
        return requests.post(url, headers=headers, data=data)

class UsersVARDELETE():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', urlvars_dict['user_identifier'], netloc=netloc)
        return requests.delete(url, headers=headers)

class UsersVARGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', urlvars_dict['user_identifier'], netloc=netloc)
        return requests.get(url, headers=headers)

class UsersVARLoginPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', urlvars_dict['user_identifier'], 'login')
        return  requests.post(url, data=data, headers=headers)

class UsersVARPATCH():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', urlvars_dict['user_identifier'], netloc=netloc)
        return requests.patch(url, headers=headers, data=data)

class UsersVARPUT():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', urlvars_dict['user_identifier'], netloc=netloc)
        return requests.put(url, headers=headers, data=data)

class UsersVARPreferencesGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', urlvars_dict['user_identifier'], 'preferences', netloc=netloc)
        return requests.get(url, headers=headers)


class UsersVARPreferencesDELETE():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', urlvars_dict['user_identifier'], 'preferences', netloc=netloc)
        return requests.delete(url, headers=headers)


class UsersVARPreferencesPUT():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', urlvars_dict['user_identifier'], 'preferences', netloc=netloc)
        return requests.put(url, headers=headers, data=data)


class UsersVARPreferencesPATCH():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('users', urlvars_dict['user_identifier'], 'preferences', netloc=netloc)
        return requests.patch(url, headers=headers, data=data)

