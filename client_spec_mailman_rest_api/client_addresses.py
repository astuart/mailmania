# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import requests

from client_common.utils import build_url

# when a request comes in, we need to be able to identify which resource is being accessed
# this is so we can check to make sure that the requesting user is allowed to access that resource
# the Swagger spec allows each operation to define its parameters.
# Here we specify which of those parameters is the resource identifier.

class AddressesGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('addresses', netloc=netloc)
        return requests.get(url, headers=headers)

class AddressesVARGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('addresses', urlvars_dict['email'], netloc=netloc)
        return requests.get(url, headers=headers)

class AddressesVARDELETE():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('addresses', urlvars_dict['email'], netloc=netloc)
        return requests.delete(url, headers=headers)

class AddressesVARMembershipsGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('addresses', urlvars_dict['email'], 'memberships', netloc=netloc)
        return requests.get(url, headers=headers)

class AddressesVARPreferencesGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('addresses', urlvars_dict['email'], 'preferences', netloc=netloc)
        return requests.get(url, headers=headers)

class AddressesVARUnverifyPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('addresses', urlvars_dict['email'], 'unverify', netloc=netloc)
        return requests.post(url, headers=headers, data=data)

class AddressesVARUserDELETE():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('addresses', urlvars_dict['email'], 'user', netloc=netloc)
        return requests.delete(url, headers=headers)

class AddressesVARUserGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('addresses', urlvars_dict['email'], 'user', netloc=netloc)
        return requests.get(url, headers=headers)

class AddressesVARUserPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('addresses', urlvars_dict['email'], 'user', netloc=netloc)
        return requests.post(url, headers=headers, data=data)

class AddressesVARUserPUT():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('addresses', urlvars_dict['email'], 'user', netloc=netloc)
        return requests.put(url, headers=headers, data=data)

class AddressesVARVerifyPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('addresses', urlvars_dict['email'], 'verify', netloc=netloc)
        return requests.post(url, headers=headers, data=data)
