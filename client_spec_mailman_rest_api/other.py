# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import requests
from client_common.utils import build_url

def client_request_preflight():
    url = build_url('/')
    r = requests.options(url)
    print(r.status_code)
    assert(r.status_code == 204)
    assert(r.headers['access-control-allow-headers'] == 'X-Auth-Token, Keep-Alive, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, Content-Type')
    assert(r.headers['access-control-allow-methods'] == 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
    assert(r.headers['access-control-allow-origin'] == '*')

