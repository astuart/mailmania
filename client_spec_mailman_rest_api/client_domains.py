# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import requests
from client_common.utils import build_url

class DomainsGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('domains', netloc=netloc)
        return requests.get(url, headers=headers)

class DomainsPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('domains', netloc=netloc)
        return requests.post(url, headers=headers, data=data)

class DomainsVARDELETE():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('domains', urlvars_dict['mail_host'], netloc=netloc)
        return requests.delete(url, headers=headers)

class DomainsVARListsGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('domains', urlvars_dict['mail_host'], 'lists', netloc=netloc)
        return requests.get(url, headers=headers)

class DomainsVAROwnersGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('domains', urlvars_dict['mail_host'], 'owners', netloc=netloc)
        return requests.get(url, headers=headers)

class DomainsVAROwnersPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('domains', urlvars_dict['mail_host'], 'owners', netloc=netloc)
        return requests.post(url, headers=headers, data=data)

class DomainsVAROwnersDELETE():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('domains', urlvars_dict['mail_host'], 'owners', netloc=netloc)
        return requests.delete(url, headers=headers)
