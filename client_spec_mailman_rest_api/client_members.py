# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import requests
from client_common.utils import build_url


class MembersGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('members', netloc=netloc)
        return requests.get(url, headers=headers)

class MembersPOST():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        #role = member, owner, moderator, nonmember
        url = build_url('members', netloc=netloc)
        return requests.post(url, headers=headers, data=data)

class MembersVARAllPreferencesGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('members', urlvars_dict['member_id'], 'all', 'preferences', netloc=netloc)
        return requests.get(url, headers=headers)

class MembersVARDELETE():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('members', urlvars_dict['member_id'], netloc=netloc)
        return requests.delete(url, headers=headers)

class MembersVARGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('members', urlvars_dict['member_id'], netloc=netloc)
        return requests.get(url, headers=headers)

class MembersVARPATCH():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('members', urlvars_dict['member_id'], netloc=netloc)
        return requests.patch(url, headers=headers, data=data)

class MembersVARPreferencesGET():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('members', urlvars_dict['member_id'], 'preferences', netloc=netloc)
        return requests.get(url, headers=headers)



class MembersVARPreferencesDELETE():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('members', urlvars_dict['member_id'], 'preferences', netloc=netloc)
        return requests.delete(url, headers=headers)



class MembersVARPreferencesPUT():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('members', urlvars_dict['member_id'], 'preferences', netloc=netloc)
        return requests.put(url, data=data, headers=headers)



class MembersVARPreferencesPATCH():

    @staticmethod
    def client_request(urlvars_dict=None, data=None, netloc=None, headers=None):
        url = build_url('members', urlvars_dict['member_id'], 'preferences', netloc=netloc)
        return requests.patch(url, data=data, headers=headers)


