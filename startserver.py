# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import logging

import falcon

from spec_server.server import SpecServer
from serve_common.customlogger import setup_custom_logger

logger = setup_custom_logger('root')
logger.setLevel(logging.DEBUG)

logger.debug('Starting Mailman authenticating proxy server.')

# get handlers and settings for the Mailman REST API
from serve_spec_mailman_rest_api.operations_handlers import operation_handlers as handlers_mmrest
from settings import config as config

from serve_spec_mailman_rest_api import initialize
#from serve_spec_permissions.db_global import db_permissions
#from serve_spec_datastore.db_global import db_datastore

# get handlers and settings for the permissions

# get handlers and settings for the permissions
from serve_spec_archives.operations_handlers import operation_handlers as handlers_archives
#from serve_spec_archives.db_global import db_archives
#from serve_spec_archives import setupdbtables

# temporary code
#db_archives.create_tables()
#setupdbtables.setupArchiveTables(db_archives)

"""
# get handlers and settings for the datastore
from serve_spec_datastore.operations_handlers import operation_handlers as handlers_datastore
from serve_spec_mailman_rest_api.settings import config as config_datastore
from serve_spec_datastore.db_global import db_datastore
"""


# The spec_server requires just one set of set of request handlers, so we integrate them
operation_handlers = {}
operation_handlers.update(handlers_mmrest)
#operation_handlers.update(handlers_permissions)
operation_handlers.update(handlers_archives)
#operation_handlers.update(handlers_datastore)

# create database tables if they don't exist
#db_permissions.create_tables()
#db_datastore.create_tables()
class FailedToStart():
    def __call__(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.body = 'Mailman authenticating proxy failed to start, check log files.'

api = application = falcon.API()

initialized_ok = None
try:
    initialize.cleanup_test_mode_resources()
    initialize.create_test_mode_resources()
    initialize.create_prod_mode_users()
    initialized_ok = True
except:
    logger.critical('SERVER FAILED TO START, check log messages.')
    initialized_ok = None

if initialized_ok:
    server = SpecServer(config=config, operation_handlers=operation_handlers)
    for item in config['api-docs.json']:
        # read in our Swagger specs
        with open(item) as f:
            server.load_spec_swagger(f.read())
else:
    logger.critical('SERVER FAILED TO START, check log messages.')
    server = FailedToStart()

api.add_sink(server, r'/')


