# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base
import logging

logger = logging.getLogger('root.' + __name__)


# we use multiple databases
# so, we create an object for each database and put all the sqlalchemy components into that object
# each database object can then be imported globally through the app

class db():

    def __init__(self, database_url=None):
        self.database_url =  database_url
        self.Base = declarative_base()
        self.engine = create_engine(database_url)
        self.session_factory = sessionmaker(bind=self.engine)
        self.Session = scoped_session(self.session_factory)
        self.session = self.Session()
        # to create a session >>> some_session = obj.Session()

    def create_tables(self):
        #create all tables that don't yet exist in the DB.
        logger.debug('Creating tables for {}'.format(self.database_url))
        self.Base.metadata.create_all(self.engine)


