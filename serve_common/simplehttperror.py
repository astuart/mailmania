# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from falcon.http_error import HTTPError
import falcon.status_codes as status

# a handful of simple HTTP errors that can be used without parameters
def code400BadRequest(*args, **kwargs):
    # this is a request handler
    raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=status.HTTP_400)

def code401Unauthorized(*args, **kwargs):
    # this is a request handler
    raise HTTPError(status.HTTP_401, title=status.HTTP_401, description=status.HTTP_401)

def code403Forbidden(*args, **kwargs):
    # this is a request handler
    raise HTTPError(status.HTTP_403, title=status.HTTP_403, description=status.HTTP_403)

def code404NotFound(*args, **kwargs):
    # this is a request handler
    raise HTTPError(status.HTTP_404, title=status.HTTP_404, description=status.HTTP_404)

def code503ServiceUnavailable(*args, **kwargs):
    # this is a request handler
    raise HTTPError(status.HTTP_503, title=status.HTTP_503, description=status.HTTP_503)
