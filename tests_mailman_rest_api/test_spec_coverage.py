# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from tests_common.spec_coverage import SpecCoverBase
from serve_spec_mailman_rest_api.operations_handlers import operation_handlers
from serve_spec_mailman_rest_api.operations_resource_locators import operation_resource_locators
import client_spec_mailman_rest_api
from tests_common.helpers import helpers
from tests_common.fixtures import *

class Test_SpecCoverage(SpecCoverBase):

    requestor_module = client_spec_mailman_rest_api
    excluded_operations = set([
        'ApidocsGET',
    ])
    swagger_spec = helpers.load_swagger_spec('serve_spec_mailman_rest_api/api-docs.json')
    operations = set(helpers.get_operations_from_swagger_spec(swagger_spec))
    operations = operations - excluded_operations

    @pytest.mark.parametrize('operationId', operations)
    def test_is_operation_in_operation_handlers(self, operationId):
        assert(operationId in operation_handlers.keys())

    @pytest.mark.parametrize('operationId', operations)
    def test_is_operation_in_operation_resource_locators(self, operationId):
        assert(operationId in operation_resource_locators.keys())

    @pytest.mark.parametrize('operationId', operations)
    def test_does_operation_have_a_client_accessor_function(self, operationId):
        # do we have a class accessor function?
        try:
            # try to load the class with the same name as the operationid
            getattr(self.requestor_module, operationId)
            assert(True)
        except:
            assert(False)
