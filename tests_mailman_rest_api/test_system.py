# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from tests_common.fixtures import *
from client_spec_mailman_rest_api.client_system import *
from tests_common.common import CommonTests
from tests_common.helpers import helpers

class Test_SystemOPTIONS():

    # permissions required:
    # public

    requestor_class = SystemOPTIONS
    urlvars_dict={}



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self):
        r = SystemOPTIONS.client_request()
        assert(r.status_code == 200)

class Test_SystemGET(CommonTests):

    requestor_class = SystemGET
    urlvars_dict={'section': 'mailman'}

    # permissions required:
    # serverowner



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = SystemGET.client_request(headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = SystemGET.client_request(headers=jwt_header)
        assert(r.status_code == 401)

class Test_SystemPreferencesGET(CommonTests):

    requestor_class = SystemPreferencesGET
    urlvars_dict={'section': 'mailman'}

    # permissions required:
    # serverowner



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = SystemPreferencesGET.client_request(headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = SystemPreferencesGET.client_request(headers=jwt_header)
        assert(r.status_code == 401)

class Test_SystemConfigurationGET(CommonTests):

    requestor_class = SystemConfigurationGET
    urlvars_dict={'section': 'mailman'}

    # permissions required:
    # serverowner



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = SystemConfigurationGET.client_request(headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = SystemConfigurationGET.client_request(headers=jwt_header)
        assert(r.status_code == 401)

class Test_SystemConfigurationVARGET(CommonTests):

    requestor_class = SystemConfigurationVARGET
    urlvars_dict={'section': 'mailman'}

    # permissions required:
    # serverowner

    # available VAR is the sections list from SystemConfigurationGET
    # example:
    # ["antispam", "archiver.mail_archive", "archiver.master", "archiver.mhonarc", "archiver.prototype", "bounces", "database", "devmode", "digests", "language.en", "logging.archiver", "logging.bounce", "logging.config", "logging.database", "logging.debug", "logging.error", "logging.fromusenet", "logging.http", "logging.locks", "logging.mischief", "logging.root", "logging.runner", "logging.smtp", "logging.subscribe", "logging.vette", "mailman", "mta", "nntp", "passwords", "paths.dev", "paths.fhs", "paths.here", "paths.local", "runner.archive", "runner.bad", "runner.bounces", "runner.command", "runner.digest", "runner.in", "runner.lmtp", "runner.nntp", "runner.out", "runner.pipeline", "runner.rest", "runner.retry", "runner.shunt", "runner.virgin", "shell", "styles", "webservice"]



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self):
        jwt_header = helpers.login_as_test_mode_serverowner()
        section = 'mailman'
        r = self.requestor_class.client_request(urlvars_dict={'section': section}, headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        section = 'mailman'
        r = self.requestor_class.client_request(urlvars_dict={'section': section}, headers=jwt_header)
        assert(r.status_code == 401)


