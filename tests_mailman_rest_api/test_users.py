# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from tests_common.fixtures import *
from client_spec_mailman_rest_api.client_users import *
from tests_common.common import CommonTests
from tests_common.helpers import helpers

class Test_UsersGET(CommonTests):

    requestor_class = UsersGET
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    # serverowner

    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        # create user testuser1@mail.example.org
        # login as testuser1@mail.example.org
        # send request with JWT of logged in user
        # assert status code is unauthorised
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 401)

class Test_UsersPOST(CommonTests):

    requestor_class = UsersPOST
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    # permissions required:
    # serverowner

    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self):
        jwt_header = helpers.login_as_test_mode_serverowner()
        data = {'email': 'testuser2@mail.example.org', 'display_name': 'test user 2', 'password': 'password'}
        UsersVARDELETE.client_request(urlvars_dict={'user_identifier': 'testuser2@mail.example.org'}, headers=jwt_header)
        r = self.requestor_class.client_request(data=data, headers=jwt_header)
        UsersVARDELETE.client_request(urlvars_dict={'user_identifier': 'testuser2@mail.example.org'}, headers=jwt_header)
        assert(r.status_code == 201)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        # create user testuser1@mail.example.org
        # login as testuser1@mail.example.org
        # send request with JWT of logged in user
        # assert status code is unauthorised
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        user_identifier = 'testuser2@mail.example.org'
        data = {'email': user_identifier, 'display_name': 'test user 2', 'password': 'password'}
        r = self.requestor_class.client_request(data=data, headers=jwt_header)
        UsersVARDELETE.client_request(urlvars_dict={'user_identifier': 'testuser21@mail.example.org'}, headers=jwt_header)
        assert(r.status_code == 401)

class Test_UsersVARAddressesGET(CommonTests):

    requestor_class = UsersVARAddressesGET
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    # permissions required:
    # user
    # serverowner

    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_UsersVARAddressesPOST(CommonTests):

    requestor_class = UsersVARAddressesPOST
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    ############################THIS METHOD IS A PROBLEM BECAUSE IT HAS OPTIONS PASSED IN THE POST DATA

    # serverowner
    # user

    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        user_id = 'testuser1@mail.example.org'
        email = 'testuser2@mail.example.org'
        data = {'email': email, 'display_name': 'test user 2'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        resp = UsersVARAddressesGET.client_request(urlvars_dict={'user_identifier': email}, headers=jwt_header)
        assert(r.status_code == 201)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        user_id = 'testuser1@mail.example.org'
        email = 'testuser2@mail.example.org'
        data = {'email': email, 'display_name': 'test user 2'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 201)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        user_id = 'testuser1@mail.example.org'
        email = 'testuser2@mail.example.org'
        data = {'email': email, 'display_name': 'test user 2'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 401)

class Test_UsersVARDELETE(CommonTests):

    requestor_class = UsersVARDELETE
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    # serverowner
    # user

    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self):
        jwt_header = helpers.login_as_test_mode_serverowner()
        data = {'email': 'testuser1@mail.example.org', 'display_name': 'test user 1', 'password': 'password'}
        UsersPOST.client_request(data=data, headers=jwt_header)
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 204)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 204)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_UsersVARGET(CommonTests):

    requestor_class = UsersVARGET
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    # serverowner
    # user

    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_UsersVARLoginPOST(CommonTests):

    requestor_class = UsersVARLoginPOST
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    # requires user

    # test owning user can access
    def test_owning_user_can_access(self, requires_testuser1):
        user_identifier = 'testuser1@mail.example.org'
        data = {'cleartext_password': 'password'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data)
        print(r)
        assert(r.status_code == 204)

    # test incorrect password cannot access
    def test_incorrect_password_cannot_access(self, requires_testuser1):
        user_identifier = 'testuser1@mail.example.org'
        data = {'cleartext_password': 'incorrectpassword'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data)
        assert(r.status_code == 401)

    # test empty password cannot access
    def test_empty_password_cannot_access(self, requires_testuser1):
        user_identifier = 'testuser1@mail.example.org'
        data = {'cleartext_password': ''}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data)
        assert(r.status_code == 401)

class Test_UsersVARPATCH(CommonTests):

    requestor_class = UsersVARPATCH
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    # permissions required:
    # serverowner
    # userowner

    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        user_identifier = 'testuser1@mail.example.org'
        data = {'display_name': 'name change', 'cleartext_password': 'changedpassword'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test user can do
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        user_identifier = 'testuser1@mail.example.org'
        data = {'display_name': 'name change', 'cleartext_password': 'changedpassword'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        user_identifier = 'testuser1@mail.example.org'
        data = {'display_name': 'name change', 'cleartext_password': 'changedpassword'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 401)

class Test_UsersVARPUT(CommonTests):

    requestor_class = UsersVARPUT
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}
    data = {'display_name': 'name change', 'cleartext_password': 'changedpassword', 'is_server_owner': 'no'}

    # permissions required:
    # serverowner
    # userowner

    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        user_identifier = 'testuser1@mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=self.data, headers=jwt_header)
        assert(r.status_code == 204)

    # test user can do
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        user_identifier = 'testuser1@mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=self.data, headers=jwt_header)
        assert(r.status_code == 204)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        user_identifier = 'testuser1@mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=self.data, headers=jwt_header)
        assert(r.status_code == 401)

class Test_UsersVARPreferencesGET(CommonTests):

    requestor_class = UsersVARPreferencesGET
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    # permissions required
    # serverowner
    # userowner



    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_UsersVARPreferencesDELETE(CommonTests):

    # deletes all preferences

    requestor_class = UsersVARPreferencesDELETE
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    # permissions required
    # serverowner
    # userowner



    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 204)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 204)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_UsersVARPreferencesPUT(CommonTests):

    # sets all preferences
    requestor_class = UsersVARPreferencesPUT
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    # permissions required:
    # serverowner
    # userowner



    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        user_identifier = 'testuser1@mail.example.org'
        data = {
        'acknowledge_posts': True,
        'delivery_mode': 'plaintext_digests',
        'delivery_status': 'by_user',
        'hide_address': False,
        'preferred_language': 'en',
        'receive_list_copy': False,
        'receive_own_postings': False,
        }
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test user can do
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        user_identifier = 'testuser1@mail.example.org'
        data = {
        'acknowledge_posts': True,
        'delivery_mode': 'plaintext_digests',
        'delivery_status': 'by_user',
        'hide_address': False,
        'preferred_language': 'en',
        'receive_list_copy': False,
        'receive_own_postings': False,
        }
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        user_identifier = 'testuser1@mail.example.org'
        data = {
        'acknowledge_posts': True,
        'delivery_mode': 'plaintext_digests',
        'delivery_status': 'by_user',
        'hide_address': False,
        'preferred_language': 'en',
        'receive_list_copy': False,
        'receive_own_postings': False,
        }
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 401)

class Test_UsersVARPreferencesPATCH(CommonTests):

    # sets specific preferences

    requestor_class = UsersVARPreferencesPATCH
    urlvars_dict={'user_identifier': 'testuser1@mail.example.org'}

    # permissions required:
    # serverowner
    # userowner



    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        user_identifier = 'testuser1@mail.example.org'
        data = {
        'delivery_mode': 'plaintext_digests',
        'hide_address': False,
        'receive_list_copy': False,
        }
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test user can do
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        user_identifier = 'testuser1@mail.example.org'
        data = {
        'acknowledge_posts': True,
        'delivery_status': 'by_user',
        'preferred_language': 'en',
        }
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        user_identifier = 'testuser1@mail.example.org'
        data = {
        'acknowledge_posts': True,
        'delivery_status': 'by_user',
        'preferred_language': 'en',
        }
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 401)

