# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from tests_common.fixtures import *
from client_spec_mailman_rest_api.client_addresses import *
from client_spec_mailman_rest_api.client_users import UsersVARDELETE
from tests_common.common import CommonTests
from tests_common.helpers import helpers

class Test_AddressesGET(CommonTests):

    # permissions required:
    # serverowner

    requestor_class = AddressesGET

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 401)


class Test_AddressesVARGET(CommonTests):

    # permissions required:
    # serverowner
    # userowner

    requestor_class = AddressesVARGET
    urlvars_dict = {'email':  'testuser1@mail.example.org'}



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_AddressesVARDELETE(CommonTests):

    # permissions required:
    # serverowner

    requestor_class = AddressesVARDELETE
    urlvars_dict = {'email':  'testuser1@mail.example.org'}



    # test serverowner can access
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        # remove the address
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 204)

    # test that test1@example.org cannot access
    def test_not_owning_user_cannot_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        # remove the address
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_AddressesVARMembershipsGET(CommonTests):

    # permissions required:
    # serverowner
    # userowner

    requestor_class = AddressesVARMembershipsGET
    urlvars_dict = {'email':  'testuser1@mail.example.org'}

    # test mailman admin user can access
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testlist1_with_subscriber_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that test2@example.org (not a list member) cannot access testuser1@mail.example.org's membership record
    def test_not_owning_user_cannot_access(self, requires_testlist1_with_subscriber_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

    # test that test2@example.org (a list member) cannot access testuser1@mail.example.org's membership record
    def test_other_list_member_cannot_access(self, requires_testlist1_with_subscriber_testuser1_and_member_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_AddressesVARPreferencesGET(CommonTests):

    # permissions required:
    # serverowner
    # userowner

    requestor_class = AddressesVARPreferencesGET
    urlvars_dict = {'email':  'testuser1@mail.example.org'}



    # test mailman admin user can do GET
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_AddressesVARUnverifyPOST(CommonTests):

    # permissions required:
    # serverowner
    # userowner

    requestor_class = AddressesVARUnverifyPOST
    urlvars_dict = {'email':  'testuser1@mail.example.org'}



    # test mailman admin user can access
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        data = {'email': 'testuser1@mail.example.org'}
        # Verify/Unverify ignores POST data but we provide it because requests knows to POST when it has data
        r = AddressesVARVerifyPOST.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        assert(r.status_code == 204)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        data = {'email': 'testuser1@mail.example.org'}
        # Verify/Unverify ignores POST data but we provide it because requests knows to POST when it has data
        r = AddressesVARVerifyPOST.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        assert(r.status_code == 204)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        data = {'email': 'testuser1@mail.example.org'}
        # Verify/Unverify ignores POST data but we provide it because requests knows to POST when it has data
        r = AddressesVARVerifyPOST.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        assert(r.status_code == 401)

class Test_AddressesVARUserDELETE(CommonTests):

    # permissions required:
    # serverowner

    requestor_class = AddressesVARUserDELETE
    urlvars_dict = {'email':  'testuser1@mail.example.org'}



    # test serverowner can access
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        # we need to be able to identify the first user that we create in order to delete it at the end of the test
        _user_id = str(helpers.get_user_id(email='testuser1@mail.example.org', headers=jwt_header))
        # unlink user from address testuser1@mail.example.org
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        data = {'auto_create': True}
        # link address to new user with create option on should create a new user - we do this just to make sure things
        # are cleaned up after the test
        AddressesVARUserPOST.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        # remove the initial user that we created and then unlinked the email address from
        UsersVARDELETE.client_request(urlvars_dict={'user_identifier': _user_id}, headers=jwt_header)
        # remove the address
        AddressesVARDELETE.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 204)

    # test that test1@example.org cannot access
    def test_not_owning_user_cannot_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_AddressesVARUserGET(CommonTests):

    # permissions required:
    # serverowner
    # userowner

    requestor_class = AddressesVARUserGET
    urlvars_dict = {'email':  'testuser1@mail.example.org'}



    # test mailman admin user can access
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_AddressesVARUserPOST(CommonTests):

    ######################################MIGHT NEED FLESHING OUT
    # permissions required:
    # serverowner

    requestor_class = AddressesVARUserPOST
    urlvars_dict = {'email':  'testuser1@mail.example.org'}



    # test serverowner can access
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        # we need to be able to identify the first user that we create in order to delete it at the end of the test
        _user_id = str(helpers.get_user_id(email='testuser1@mail.example.org', headers=jwt_header))
        # unlink user from address testuser1@mail.example.org
        r = AddressesVARUserDELETE.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        data = {'auto_create': True}
        # link address to new user with create option on should c reate a new user
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        # remove the initial user that we created and then unlinked the email address from
        UsersVARDELETE.client_request(urlvars_dict={'user_identifier': _user_id}, headers=jwt_header)
        assert(r.status_code == 201)

    # test that test1@example.org cannot access
    def test_not_owning_user_cannot_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        data = {'auto_create': True}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        assert(r.status_code == 401)

class Test_AddressesVARUserPUT(CommonTests):

    #Link an address to a different user

    requestor_class = AddressesVARUserPUT
    urlvars_dict = {'email':  'testuser1@mail.example.org'}

    # permissions required:
    # serverowner



    # test serverowner can access
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        # we need to be able to identify the first user that we create in order to delete it at the end of the test
        _user_id = str(helpers.get_user_id(email='testuser1@mail.example.org', headers=jwt_header))
        data = {'email': 'testuser2@mail.example.org'}
        # link address to new user with create option on should c reate a new user
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        # remove the initial user that we created and then unlinked the email address from
        UsersVARDELETE.client_request(urlvars_dict={'user_identifier': _user_id}, headers=jwt_header)
        assert(r.status_code == 201)

    # test that test1@example.org cannot access
    def test_not_owning_user_cannot_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        # we need to be able to identify the first user that we create in order to delete it at the end of the test
        _user_id = str(helpers.get_user_id(email='testuser1@mail.example.org', headers=jwt_header))
        data = {'email': 'testuser2@mail.example.org'}
        # link address to new user with create option on should c reate a new user
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        # remove the initial user that we created and then unlinked the email address from
        UsersVARDELETE.client_request(urlvars_dict={'user_identifier': _user_id}, headers=jwt_header)
        assert(r.status_code == 401)

class Test_AddressesVARVerifyPOST(CommonTests):

    # permissions required:
    # serverowner
    # userowner

    requestor_class = AddressesVARVerifyPOST
    urlvars_dict = {'email':  'testuser1@mail.example.org'}



    # test mailman admin user can access
    def test_can_access_with_serverowner_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        data = {'email': 'testuser1@mail.example.org'}
        # Verify/Unverify ignores POST data but we provide it because requests knows to POST when it has data
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        assert(r.status_code == 204)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        data = {'email': 'testuser1@mail.example.org'}
        # Verify/Unverify ignores POST data but we provide it because requests knows to POST when it has data
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        assert(r.status_code == 204)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testuser1, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        data = {'email': 'testuser1@mail.example.org'}
        # Verify/Unverify ignores POST data but we provide it because requests knows to POST when it has data
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header, data=data)
        assert(r.status_code == 401)

