# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from tests_common.fixtures import *
from client_spec_mailman_rest_api.client_domains import *
from tests_common.common import CommonTests
from tests_common.helpers import helpers


class Test_DomainsGET(CommonTests):

    requestor_class = DomainsGET
    urlvars_dict = {'mail_host':  'mail.example.org'}

    # requires serverowner

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 401)

class Test_DomainsPOST(CommonTests):

    requestor_class = DomainsPOST
    urlvars_dict = {'mail_host':  'mail.example.org'}
    postdata = {'mail_host':  'mail.example.org'}

    # requires serverowner

    # test serverowner can create domains
    def test_can_access_with_serverowner_rights(self, setup_remove_domain_mailDOTexampleDOTorg):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(data=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 201)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(data=self.postdata, headers=jwt_header)
        assert(r.status_code == 401)

class Test_DomainsVARDELETE(CommonTests):

    requestor_class = DomainsVARDELETE
    urlvars_dict = {'mail_host':  'mail.example.org'}

    # requires serverowner

    # test serverowner can delete domains
    def test_can_access_with_serverowner_rights(self, requires_domain_mailDOTexampleDOTorg):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 204)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1, requires_domain_mailDOTexampleDOTorg):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_DomainsVARListsGET(CommonTests):

    requestor_class = DomainsVARListsGET
    urlvars_dict = {'mail_host':  'mail.example.org'}

    # requires serverowner
    # requires domainowner

    # test serverowner can get the lists for a domain
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        domain = 'mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test domainowner can get the lists for a domain
    def test_can_access_with_domainowner_rights(self, requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testlist1_with_subscriber_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_DomainsVAROwnersGET(CommonTests):

    requestor_class = DomainsVAROwnersGET
    urlvars_dict = {'mail_host':  'mail.example.org'}

    # requires serverowner
    # requires domainowner

    # requires fixture that creates a domain and adds owners to it

    # test serverowner can get the owners of a domain
    def test_can_access_with_serverowner_rights(self, requires_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test domainowner can get the owners of a domain
    def test_can_access_with_domainowner_rights(self, requires_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1_with_role_domainowner, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)


class Test_DomainsVAROwnersDELETE(CommonTests):

    requestor_class = DomainsVAROwnersDELETE
    urlvars_dict = {'mail_host':  'mail.example.org'}

    # requires serverowner
    # requires domainowner

    # test serverowner can deleteowners of a domain
    def test_can_access_with_serverowner_rights(self, requires_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 204)

    # test serverowner can deleteowners of a domain
    def test_can_access_with_domainowner_rights(self, requires_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 204)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self,  requires_testuser1_with_role_domainowner, requires_testuser2):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)


class Test_DomainsVAROwnersPOST(CommonTests):

    requestor_class = DomainsVAROwnersPOST
    urlvars_dict = {'mail_host':  'mail.example.org'}

    # requires serverowner
    # requires domainowner

    # test serverowner can add owners to a domain
    def test_can_access_with_serverowner_rightsX(self, requires_testuser1, requires_domain_mailDOTexampleDOTorg):
        jwt_header = helpers.login_as_test_mode_serverowner()
        data = {'owner': 'testuser1@mail.example.org'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test domainowner can add owners to a domain
    def test_can_access_with_domainowner_rights(self, requires_testuser2, requires_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        data = {'owner': 'testuser2@mail.example.org'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self,  requires_testuser1, requires_domain_mailDOTexampleDOTorg):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        data = {'owner': 'testuser2@mail.example.org'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=data, headers=jwt_header)
        assert(r.status_code == 401)

