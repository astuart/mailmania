# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from tests_common.fixtures import *
from tests_common.common import CommonTests
from client_spec_mailman_rest_api.client_queues import *
from tests_common.helpers import helpers

class Test_QueuesGET(CommonTests):

    requestor_class = QueuesGET
    urlvars_dict={}

    # permissions required:
    # serverowner



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 401)

class Test_QueuesVARGET(CommonTests):

    # permissions required:
    # serverowner

    requestor_class = QueuesVARGET
    urlvars_dict={'queue_name': 'bad'}



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1,
                                                requires_message_injected_into_bad_queue,
                                                ):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    # test serverowner can do
    def test_cannot_access_with_user_rights(self, requires_testuser1,
                                                requires_testlist1,
                                                requires_message_injected_into_bad_queue,
                                                ):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_QueuesVARPOST(CommonTests):

    # permissions required:
    # serverowner

    requestor_class = QueuesVARPOST
    urlvars_dict={'queue_name': 'bad'}

    data = {'list_id': 'testlist1.mail.example.org',
            'text': """\
            From: testuser1@mail.example.org
            To: testuser2@mail.example.org
            Subject: Testing

            """
            }



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1, requires_message_injected_into_bad_queue):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(data=self.data, urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 201)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1, requires_testlist1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        self.urlvars_dict['queue_name'] = 'bad'
        r = self.requestor_class.client_request(data=self.data, urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_QueuesVARVARDELETE(CommonTests):

    requestor_class = QueuesVARVARDELETE
    urlvars_dict={'queue_name': 'bad', 'file_name': 'some_fake_filename'}

    data = {'list_id': 'testlist1.mail.example.org',
            'text': """\
            From: testuser1@mail.example.org
            To: testuser2@mail.example.org
            Subject: Testing

            """
            }



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testuser1, requires_testlist1,
                                                requires_message_injected_into_bad_queue):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id =  'testlist1.mail.example.org'
        text =  """\
                From: testuser1@mail.example.org
                To: testuser2@mail.example.org
                Subject: Testing

                """

        #inject_message_into_queue
        data = {}
        data['list_id'] =  list_id
        data['text'] =  text
        urlvars_dict = {}
        urlvars_dict['queue_name'] = 'bad'
        QueuesVARPOST.client_request(urlvars_dict=urlvars_dict, data=data,  headers=jwt_header)

        # there should now be at least one file_name in queue. Let's just assume its ours.
        queue_filelist= QueuesVARGET.client_request(urlvars_dict=urlvars_dict,  headers=jwt_header)
        queue_filelist = queue_filelist.json()
        print(queue_filelist)
        queue_filelist = queue_filelist.get('files', [])
        file_name = queue_filelist[0]
        print(file_name)
        r = self.requestor_class.client_request(urlvars_dict={'queue_name': 'bad', 'file_name' : file_name}, headers=jwt_header)
        assert(r.status_code == 204)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1, requires_testlist1,
                                            requires_message_injected_into_bad_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)


