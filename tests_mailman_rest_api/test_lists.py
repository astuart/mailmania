# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from tests_common.fixtures import *
from tests_common.common import CommonTests
from client_spec_mailman_rest_api.client_lists import *
from tests_common.helpers import helpers

class Test_ListsGET(CommonTests):

    requestor_class = ListsGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # permissions required:
    # serverowner



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testlist1_with_subscriber_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsPOST(CommonTests):

    requestor_class = ListsPOST
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # permissions required:
    # serverowner
    # domainowner

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self,
                                                setup_remove_list_testlist1DOTmailDOTexampleDOTorg,
                                                setup_remove_domain_mailDOTexampleDOTorg,
                                                requires_domain_mailDOTexampleDOTorg):
        jwt_header = helpers.login_as_test_mode_serverowner()
        fqdn_listname = 'testlist1@mail.example.org'
        style_name=''
        data = {'fqdn_listname': fqdn_listname, 'style_name': style_name}
        r = self.requestor_class.client_request(data=data, urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 201)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                                setup_remove_list_testlist1DOTmailDOTexampleDOTorg,
                                                setup_remove_domain_mailDOTexampleDOTorg,
                                requires_domain_mailDOTexampleDOTorg_with_user_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        fqdn_listname = 'testlist1@mail.example.org'
        style_name=''
        data = {'fqdn_listname': fqdn_listname, 'style_name': style_name}
        r = self.requestor_class.client_request(data=data, urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 201)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        fqdn_listname = 'testlist1@mail.example.org'
        style_name=''
        data = {'fqdn_listname': fqdn_listname, 'style_name': style_name}
        r = self.requestor_class.client_request(data=data, urlvars_dict=self.urlvars_dict, headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsStylesGET(CommonTests):

    requestor_class = ListsStylesGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}



    # test ordinary user can do
    def test_can_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 200)

class Test_ListsVARArchiversGET(CommonTests):

    requestor_class = ListsVARArchiversGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # serverowner
    # domain admin
    # list admin



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self, 
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self, 
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsVARArchiversPATCH(CommonTests):

    requestor_class = ListsVARArchiversPATCH
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # serverowner
    # domain admin
    # list admin



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = ListsVARArchiversPATCH.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsVARArchiversPUT(CommonTests):

    requestor_class = ListsVARArchiversPUT
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # serverowner
    # domain admin
    # list admin



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        data = {'mail-archive': True, 'prototype': True, 'mhonarc': True}
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=data, headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsVARConfigGET(CommonTests):

    requestor_class = ListsVARConfigGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # serverowner
    # domainowner
    # listowner
    # listmoderator
    # listmember




    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test list member can do
    def test_can_access_with_listmember_rightsX(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsVARConfigPUT(CommonTests):

    requestor_class = ListsVARConfigPUT
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # serverowner
    # domainowner
    # listowner
    # listmoderator

    data = {
        'acceptable_aliases': ['one@example.com', 'two@example.com'],
        'admin_immed_notify': False,
        'admin_notify_mchanges': True,
        'administrivia': False,
        'advertised': False,
        'anonymous_list': True,
        'archive_policy': 'never',
        'autorespond_owner': 'respond_and_discard',
        'autorespond_postings': 'respond_and_continue',
        'autorespond_requests': 'respond_and_discard',
        'autoresponse_grace_period': '45d',
        'autoresponse_owner_text': 'the owner',
        'autoresponse_postings_text': 'the mailing list',
        'autoresponse_request_text': 'the robot',
        'display_name': 'Fnords',
        'description': 'This is my mailing list',
        'include_rfc2369_headers': False,
        'allow_list_posts': False,
        'digest_size_threshold': 10.5,
        'posting_pipeline': 'virgin',
        'filter_content': True,
        'first_strip_reply_to': True,
        'convert_html_to_plaintext': True,
        'collapse_alternatives': False,
        'reply_goes_to_list': 'point_to_list',
        'reply_to_address': 'bee@example.com',
        'send_welcome_message': False,
        'subject_prefix': '[ant]',
        'subscription_policy': 'moderate',
        'welcome_message_uri': 'mailman:///welcome.txt',
        'default_member_action': 'hold',
        'default_nonmember_action': 'discard',
        }




    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 204)


    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 204)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 204)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 204)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsVARConfigPATCH(CommonTests):

    requestor_class = ListsVARConfigPATCH
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # serverowner
    # domainowner
    # listowner
    # listmoderator

    data = {
        'acceptable_aliases': ['one@example.com', 'two@example.com'],
        'admin_immed_notify': False,
        'admin_notify_mchanges': True,
        'administrivia': False,
        'advertised': False,
        'anonymous_list': True,
        'archive_policy': 'never',
        'autorespond_owner': 'respond_and_discard',
        'autorespond_postings': 'respond_and_continue',
        'autorespond_requests': 'respond_and_discard',
        'autoresponse_grace_period': '45d',
        'autoresponse_owner_text': 'the owner',
        'autoresponse_postings_text': 'the mailing list',
        'autoresponse_request_text': 'the robot',
        'display_name': 'Fnords',
        'description': 'This is my mailing list',
        'include_rfc2369_headers': False,
        'allow_list_posts': False,
        'digest_size_threshold': 10.5,
        'posting_pipeline': 'virgin',
        'filter_content': True,
        'first_strip_reply_to': True,
        'convert_html_to_plaintext': True,
        'collapse_alternatives': False,
        'reply_goes_to_list': 'point_to_list',
        'reply_to_address': 'bee@example.com',
        'send_welcome_message': False,
        'subject_prefix': '[ant]',
        'welcome_message_uri': 'mailman:///welcome.txt',
        'default_member_action': 'hold',
        'default_nonmember_action': 'discard',
        }




    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 204)


    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 204)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 204)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 204)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = ListsVARConfigPATCH.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, data=self.data, headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsVARConfigVARGET(CommonTests):

    requestor_class = ListsVARConfigVARGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org',
                  'config_variable': 'anonymous_list',
    }

    #requires:
    #serverowner
    #domainowner
    #listowner
    #listmoderator
    #listmember

    # test serverowner can do
    def do_test(self, jwt_header=None):
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        return r.status_code

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        assert(self.do_test(jwt_header=jwt_header) == 200)


    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

class Test_ListsVARConfigVARPATCH(CommonTests):

    requestor_class = ListsVARConfigVARPATCH
    urlvars_dict={'list_id': 'testlist1.mail.example.org',
                  'config_variable': 'anonymous_list',
    }

    data = {
        'display_name': 'Fnords',
        }

    #requires:
    #serverowner
    #domainowner
    #listowner

    # test serverowner can do
    def do_test(self, jwt_header=None):
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data=self.data, headers=jwt_header)
        return r.status_code

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        assert(self.do_test(jwt_header=jwt_header) == 204)


    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 204)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 204)

    # test listmoderator can do
    def test_cannot_access_with_listmoderator_rights(self,
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

class Test_ListsVARDELETE(CommonTests):

    requestor_class = ListsVARDELETE
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # serverowner
    # domain admin
    # list admin



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 204)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 204)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = ListsVARDELETE.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 204)

    # test listmoderator can do
    def test_cannot_access_with_listmoderator_rights(self,
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsVARGET(CommonTests):

    requestor_class = ListsVARGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # serverowner
    # domainowner
    # listowner
    # listmoderator



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test list member cannot do
    def test_can_access_with_listmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsVARHeldGET(CommonTests):

    requestor_class = ListsVARHeldGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    #requires:
    #serverowner
    #domainowner
    #listowner
    #listmoderator

    # test serverowner can do
    def do_test(self, jwt_header=None):
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        return r.status_code

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self,
                                            requires_testlist1,
                                            set_testlist1_to_default_hold_messages,
                                            set_testlist1_to_enable_mail_archive,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_test_mode_serverowner()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_with_role_domainowner,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_owner_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_moderator_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_nonmember_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_member_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self,
                                            requires_testuser1,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

class Test_ListsVARHeldVARGET(CommonTests):

    requestor_class = ListsVARHeldVARGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org',
                  'request_id': 'fake_request_id'}

    #requires:
    #serverowner
    #domainowner
    #listowner
    #listmoderator


    def do_test(self, jwt_header=None):
        list_id = 'testlist1.mail.example.org'
        # we need a valid request_id to successfully complete test
        held_messages = helpers.get_list_held_messages_testlist1ATmailDOTexampleDOTorg(headers=jwt_header)
        response_data = held_messages.json()
        entries = response_data.get('entries', None)
        request_id = 'fake_request_id'
        if entries:
            request_id = str(entries[0].get('request_id', None))
        urlvars_dict = {}
        urlvars_dict['list_id'] = list_id
        urlvars_dict['request_id'] = str(request_id)
        # do test with request_id
        r = self.requestor_class.client_request(urlvars_dict=urlvars_dict, headers=jwt_header)
        return r.status_code

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self,
                                            requires_testlist1,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_test_mode_serverowner()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_with_role_domainowner,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_owner_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_moderator_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_nonmember_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_member_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self,
                                            requires_testuser1,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

class Test_ListsVARHeldVARPOST(CommonTests):

    requestor_class = ListsVARHeldVARPOST
    urlvars_dict={'list_id': 'testlist1.mail.example.org',
                  'request_id': 'some_fake_request_id'}

    #requires:
    #serverowner
    #domainowner
    #listowner
    #listmoderator

    def do_test(self, jwt_header=None):
        list_id = 'testlist1.mail.example.org'
        # we need a valid request_id to successfully complete test
        held_messages = helpers.get_list_held_messages_testlist1ATmailDOTexampleDOTorg(headers=jwt_header)
        response_data = held_messages.json()
        entries = response_data.get('entries', None)
        request_id = 'fake_request_id'
        if entries:
            request_id = str(entries[0].get('request_id', None))
        urlvars_dict = {}
        urlvars_dict['list_id'] = list_id
        urlvars_dict['request_id'] = str(request_id)
        # do test with request_id
        r = self.requestor_class.client_request(urlvars_dict=urlvars_dict, data={'action': 'discard'},
                                           headers=jwt_header)
        return r.status_code

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self,
                                            requires_testlist1,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_test_mode_serverowner()
        assert(self.do_test(jwt_header=jwt_header) == 204)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_with_role_domainowner,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 204)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_owner_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 204)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_moderator_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 204)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_nonmember_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_member_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self,
                                            requires_testuser1,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

class Test_ListsVARRequestsGET(CommonTests):

    requestor_class = ListsVARRequestsGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}


    """
    #    'ListsVARRequestsGET',
        # gets all membership requests for list
        fixture
            create list
            create user
            subscribe user to list
        test
            do ListsVARRequestsGET and ensure there is one request
        fin
            delete user
            delete list
    http://localhost:9001/3.0/members', {
        ...           'list_id': 'ant.example.com',
        ...           'subscriber': 'eperson@example.com',
        ...           'display_name': 'Elly Person',
        ...           })
    """

    #requires:
    #serverowner
    #domainowner
    #listowner
    #listmoderator

    # test serverowner can do
    def do_test(self, jwt_header=None):
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        return r.status_code

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 200)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

class Test_ListsVARRequestsVARGET(CommonTests):

    """
    #    'ListsVARRequestsVARGET',
        fixture
            create list
            create user
            subscribe user to list
        test
            do ListsVARRequestsGET and get request_id
            do ListsVARRequestsVARGET and get the request associated with request_id
        fin
            delete user
            delete list
    """

    requestor_class = ListsVARRequestsVARGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org',
                  'request_id': 'fake_request_id'}

    #requires:
    #serverowner
    #domainowner
    #listowner
    #listmoderator

    def do_test(self, jwt_header=None):
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        return r.status_code

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        assert(self.do_test(jwt_header=jwt_header) == 404)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 404)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 404)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 404)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

class Test_ListsVARRequestsVARPOST(CommonTests):

    """
    #    'ListsVARRequestsVARPOST',
        fixture
            create list
            create user
            subscribe user to list
        test
            do ListsVARRequestsGET and get request_id
            do ListsVARRequestsVARGET and get the request associated with request_id
            do ListsVARRequestsVARPOST to dispose of the request
            do ListsVARRequestsGET and get request_id and ensure number of requests = 0
        fin
            delete user
            delete list
    """

    requestor_class = ListsVARRequestsVARPOST
    urlvars_dict={'list_id': 'testlist1.mail.example.org',
                  'request_id': 'fake_request_id'}

    #requires:
    #serverowner
    #domainowner
    #listowner
    #listmoderator

    def do_test(self, jwt_header=None):
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, data={'action': 'discard'},
                                           headers=jwt_header)
        return r.status_code

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        assert(self.do_test(jwt_header=jwt_header) == 404)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 404)

    # test listowner can do
    def test_can_access_with_listowner_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 404)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 404)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_nonmember_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test list member cannot do
    def test_cannot_access_with_listmember_rights(self,
                                            requires_testlist1_with_subscriber_testuser1_member_role,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self,
                                            requires_testuser1,
                                            set_testlist1_to_default_hold_messages,
                                            requires_message_injected_into_in_queue):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)


class Test_ListsVARRosterMemberGET(CommonTests):

    requestor_class = ListsVARRosterMemberGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # serverowner
    # domainowner
    # listowner
    # listmoderator




    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self, 
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listowner can do
    def test_can_access_with_listowner_rights(self, 
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self, 
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test list member cannot do
    def test_can_access_with_listmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsVARRosterModeratorGET(CommonTests):

    requestor_class = ListsVARRosterModeratorGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # serverowner
    # domainowner
    # listowner
    # listmoderator




    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self, 
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listowner can do
    def test_can_access_with_listowner_rights(self, 
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self, 
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test list member cannot do
    def test_can_access_with_listmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

class Test_ListsVARRosterOwnerGET(CommonTests):

    requestor_class = ListsVARRosterOwnerGET
    urlvars_dict={'list_id': 'testlist1.mail.example.org'}

    # serverowner
    # domainowner
    # listowner
    # listmoderator

    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self, 
                                requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listowner can do
    def test_can_access_with_listowner_rights(self, 
                                requires_testlist1_with_subscriber_testuser1_owner_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test listmoderator can do
    def test_can_access_with_listmoderator_rights(self, 
                                requires_testlist1_with_subscriber_testuser1_moderator_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test list nonmember cannot do
    def test_cannot_access_with_listnonmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_nonmember_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test list member cannot do
    def test_can_access_with_listmember_rights(self,
                                requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test user not subscribed to list cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        r = self.requestor_class.client_request(urlvars_dict={'list_id':list_id}, headers=jwt_header)
        assert(r.status_code == 401)


