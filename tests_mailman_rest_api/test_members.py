# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from tests_common.fixtures import *
from client_spec_mailman_rest_api.client_members import *
from tests_common.common import CommonTests
from tests_common.helpers import helpers

class Test_MembersGET(CommonTests):

    requestor_class = MembersGET
    urlvars_dict={'member_id': 'member_id_doesnt_matter'}

    # permissions required:
    # serverowner



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1):
        jwt_header = helpers.login_as_test_mode_serverowner()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 200)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testlist1_with_subscriber_testuser1):
        # create user testuser1@mail.example.org
        # login as testuser1@mail.example.org
        # send request with JWT of logged in user
        # assert status code is unauthorised
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(headers=jwt_header)
        assert(r.status_code == 401)

class Test_MembersPOST(CommonTests):

    requestor_class = MembersPOST
    urlvars_dict={'member_id': 'member_id_doesnt_matter'}

    # permissions required:
    # serverowner
    # domainowner
    # listowner



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_user_testuser1_not_subscribed):
        jwt_header = helpers.login_as_test_mode_serverowner()
        list_id = 'testlist1.mail.example.org'
        subscriber='testuser1@mail.example.org'
        data = {'list_id': list_id, 'subscriber': subscriber}
        r = self.requestor_class.client_request(data=data, headers=jwt_header)
        assert(r.status_code == 202)

    # test domainowner can do
    def test_can_access_with_domainowner_rights(self, requires_testuser1_with_role_domainowner,
                                                requires_testlist1_without_domain,
                                requires_testuser2):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        subscriber='testuser2@mail.example.org'
        data = {'list_id': list_id, 'subscriber': subscriber}
        r = self.requestor_class.client_request(data=data, headers=jwt_header)
        assert(r.status_code == 202)


    # test listowner can do
    def test_can_access_with_listowner_rights(self, requires_testlist1_with_subscriber_testuser1_owner_role,
                                requires_testuser2):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        subscriber='testuser2@mail.example.org'
        data = {'list_id': list_id, 'subscriber': subscriber}
        r = self.requestor_class.client_request(data=data, headers=jwt_header)
        assert(r.status_code == 202)

    # test ordinary user cannot do
    def test_cannot_access_with_user_rights(self, requires_testuser1):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        list_id = 'testlist1.mail.example.org'
        subscriber='testuser1@mail.example.org'
        data = {'list_id': list_id, 'subscriber': subscriber}
        r = self.requestor_class.client_request(data=data, headers=jwt_header)
        assert(r.status_code == 401)

class Test_MembersVARAllPreferencesGET(CommonTests):

    requestor_class = MembersVARAllPreferencesGET
    urlvars_dict={'member_id': 'member_id_doesnt_matter'}

    # serverowner
    # userowner



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test domainowner cannot do
    def test_cannot_access_with_domainowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role,
                requires_testuser2_with_role_domainowner ):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test listowner cannot do
    def test_cannot_access_with_listowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role,
                requires_testuser2_with_role_listowner ):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test userowner can do
    def test_can_access_with_user_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 200)

class Test_MembersVARDELETE(CommonTests):

    requestor_class = MembersVARDELETE
    urlvars_dict={'member_id': 'member_id_doesnt_matter'}

    # serverowner
    # userowner



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 204)

    # test domainowner cannot do
    def test_cannot_access_with_domainowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role,
                requires_testuser2_with_role_domainowner ):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test listowner cannot do
    def test_cannot_access_with_listowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role,
                requires_testuser2_with_role_listowner ):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test ordinary user can
    def test_can_access_with_user_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 204)

class Test_MembersVARGET(CommonTests):

    requestor_class = MembersVARGET
    urlvars_dict={'member_id': 'member_id_doesnt_matter'}

    # serverowner
    # userowner



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test domainowner cannot do
    def test_cannot_access_with_domainowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role,
                requires_testuser2_with_role_domainowner ):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test listowner cannot do
    def test_cannot_access_with_listowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role,
                requires_testuser2_with_role_listowner ):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test ordinary user can
    def test_can_access_with_user_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 200)

class Test_MembersVARPATCH(CommonTests):

    requestor_class = MembersVARPATCH
    urlvars_dict={'member_id': 'member_id_doesnt_matter'}

    # serverowner
    # userowner



    # test serverowner can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        data = {'delivery_mode': 'plaintext_digests'}
        r = self.requestor_class.client_request(data=data, urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 204)

    # test domainowner cannot do
    def test_cannot_access_with_domainowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role,
                requires_testuser2_with_role_domainowner ):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        data = {'delivery_mode': 'plaintext_digests'}
        r = self.requestor_class.client_request(data=data, urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test listowner cannot do
    def test_cannot_access_with_listowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role,
                requires_testuser2_with_role_listowner ):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        data = {'delivery_mode': 'plaintext_digests'}
        r = self.requestor_class.client_request(data=data, urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 401)

    # test ordinary user can
    def test_can_access_with_user_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        data = {'delivery_mode': 'plaintext_digests'}
        r = self.requestor_class.client_request(data=data, urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 204)

class Test_MembersVARPreferencesGET(CommonTests):

    requestor_class = MembersVARPreferencesGET
    urlvars_dict={'member_id': 'member_id_doesnt_matter'}

    # permissions required
    # serverowner
    # userowner



    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 200)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testlist1_with_subscriber_testuser1_member_role, requires_testuser2):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 401)

class Test_MembersVARPreferencesDELETE(CommonTests):

    # deletes all preferences

    requestor_class = MembersVARPreferencesDELETE
    urlvars_dict={'member_id': 'member_id_doesnt_matter'}

    # permissions required
    # serverowner
    # userowner



    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 204)

    # test that testuser1@mail.example.org can access
    def test_owning_user_can_access(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 204)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testlist1_with_subscriber_testuser1_member_role, requires_testuser2):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, headers=jwt_header)
        assert(r.status_code == 401)

class Test_MembersVARPreferencesPUT(CommonTests):

    # sets all preferences
    requestor_class = MembersVARPreferencesPUT
    urlvars_dict={'member_id': 'member_id_doesnt_matter'}

    # permissions required:
    # serverowner
    # userowner



    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        data = {
        'acknowledge_posts': True,
        'delivery_mode': 'plaintext_digests',
        'delivery_status': 'by_user',
        'hide_address': False,
        'preferred_language': 'en',
        'receive_list_copy': False,
        'receive_own_postings': False,
        }
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test user can do
    def test_owning_user_can_access(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        data = {
        'acknowledge_posts': True,
        'delivery_mode': 'plaintext_digests',
        'delivery_status': 'by_user',
        'hide_address': False,
        'preferred_language': 'en',
        'receive_list_copy': False,
        'receive_own_postings': False,
        }
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testlist1_with_subscriber_testuser1_member_role, requires_testuser2):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        data = {
        'acknowledge_posts': True,
        'delivery_mode': 'plaintext_digests',
        'delivery_status': 'by_user',
        'hide_address': False,
        'preferred_language': 'en',
        'receive_list_copy': False,
        'receive_own_postings': False,
        }
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, data=data, headers=jwt_header)
        assert(r.status_code == 401)

class Test_MembersVARPreferencesPATCH(CommonTests):

    # sets specific preferences

    requestor_class = MembersVARPreferencesPATCH
    urlvars_dict={'member_id': 'member_id_doesnt_matter'}

    # permissions required:
    # serverowner
    # userowner



    # test mailman admin user can do
    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        data = {
        'delivery_mode': 'plaintext_digests',
        'hide_address': False,
        'receive_list_copy': False,
        }
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test user can do
    def test_owning_user_can_access(self, requires_testlist1_with_subscriber_testuser1_member_role):
        jwt_header = helpers.login_as_testuser1ATmailDOTexampleDOTorg()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        data = {
        'acknowledge_posts': True,
        'delivery_status': 'by_user',
        'preferred_language': 'en',
        }
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, data=data, headers=jwt_header)
        assert(r.status_code == 204)

    # test that test2@example.org cannot access testuser1@mail.example.org's user record
    def test_not_owning_user_cannot_access(self, requires_testlist1_with_subscriber_testuser1_member_role, requires_testuser2):
        jwt_header = helpers.login_as_test_mode_serverowner()
        member_id = helpers.get_member_id(email='testuser1@mail.example.org', list_id='testlist1.mail.example.org', headers=jwt_header)
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        data = {
        'acknowledge_posts': True,
        'delivery_status': 'by_user',
        'preferred_language': 'en',
        }
        r = self.requestor_class.client_request(urlvars_dict={'member_id': member_id}, data=data, headers=jwt_header)
        assert(r.status_code == 401)
        
        