# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import Column, String, Integer, Enum


# call this when database is created


#http://zarko-gajic.iz.hr/sqlite-referential-integrity-with-full-text-search-virtual-tables-used-in-a-delphi-application/

def setupArchiveTables(db):

    import sqlite3
    conn = sqlite3.connect('/home/ubuntu/crowdwave/src/crowdwave/archives.db')
    with open('serve_spec_archives/schema.sql') as f:
        conn.executescript(f.read())
    conn.commit()

