# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from base64 import b32encode
from hashlib import sha1
import re

def make_message_id_hash(message_id):
    # Mailman uses a hash of the message-id to identify messages.
    # we use this hash exclusively within the archives as the message.id
    # this function creates a message_id_hash that is (should be) the same as the Mailman hash
    if isinstance(message_id, bytes):
        message_id = message_id.decode('ascii')
    message_id = message_id.strip()
    # if message_id is enclosed in angle brackets, remove them
    message_id = re.sub(r'^<|>$', '', message_id)
    return b32encode(sha1(message_id.encode('utf-8')).digest()).decode('ascii')
