# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from serve_security.permissions import permissions
from falcon.http_error import HTTPError
import falcon.status_codes as status
from serve_spec_archives.uploadhandler import UploadToArchive
import logging

logger = logging.getLogger('root.' + __name__)

__all__ = [
    'ArchivesVARMessagesGET',
    'ArchivesVARMessagesPOST',
    'ArchivesVARMessagesVARGET',
    'ArchivesVARMessagesVARDELETE',
    'ArchivesVARFilesVARVARGET',
    'ArchivesVARFilesVARVARPOST',
    'ArchivesVARFilesVARVARDELETE',
    'ArchivesVARNotesMessageVARGET',
    'ArchivesVARNotesMessageVARPOST',
    'ArchivesVARFoldersVARDELETE',
    'ArchivesVARFoldersVARGET',
    'ArchivesVARFoldersVARPOST',
    'ArchivesVARFoldersGET',
    'ArchivesVARFoldersPOST',
    'ArchivesVARNotesVARDELETE',
    'ArchivesVARNotesVARGET',
]


def archive_query(**request_handler_args):
    pass

def archive_message_get_info(**request_handler_args):
    pass


def message_delete(**request_handler_args):
    # this a request handler
    uri_fields = request_handler_args['uri_fields']
    req = request_handler_args['req']
    if req.method != 'DELETE':
        raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
    logger.debug('DELETE message: {}'.format(uri_fields['message_id_hash']))
    _result = permissions().delete(message_id_hash=uri_fields['message_id_hash'])
    if _result:
        request_handler_args['resp'].status = status.HTTP_204
    else:
        reason = 'Delete permissions failed'
        raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=reason)

class ArchivesVARMessagesGET():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'GET':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARMessagesPOST():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'POST':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        self.handle_request()

    def handle_request(self):
        logger.debug('UploadToArchive(**self.request_handler_args)')
        UploadToArchive(**self.request_handler_args)

class ArchivesVARMessagesVARGET():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'GET':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARMessagesVARDELETE():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'DELETE':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARFilesVARVARGET():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'GET':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARFilesVARVARPOST():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'POST':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        self.handle_request()

    def handle_request(self):
        logger.debug('UploadToArchive(**self.request_handler_args)')
        UploadToArchive(**self.request_handler_args)


class ArchivesVARFilesVARVARDELETE():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'DELETE':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARNotesMessageVARGET():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'GET':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARNotesMessageVARPOST():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'POST':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARFoldersVARDELETE():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'DELETE':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARFoldersVARGET():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'GET':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARFoldersVARPOST():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'POST':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARFoldersGET():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'GET':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARFoldersPOST():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'POST':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARNotesVARDELETE():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'DELETE':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))
        
    def handle_request(self):
        pass

class ArchivesVARNotesVARGET():

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method != 'GET':
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        logger.debug('Request : {}'.format(self.__class__.__name__))


