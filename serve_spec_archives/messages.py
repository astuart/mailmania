# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

#todo session to Session

class UploadToArchive(object):

    """
    Saves message files to the archive.

    The archive accepts:
    - only one file at a time
    1: uploads of Mailman emails via the ArchivesVARMessagesPOST operation
        - a message record is created for each email
        - the source is saved as a binary file
    2: uploads of binary files via the ArchivesVARFilesVARVARPOST operation
        - a message record must exist before binary files can be added to it
        - a binary file record is created for each file
        - if the file is a text file then it is full text indexed.
    """

    def __init__(self, **request_handler_args):
        logger.debug('init')
        self.maxbytes = None
        # TODO get maxbytes from config
        self.request_handler_args = request_handler_args
        self.req = request_handler_args['req']
        self.resp = request_handler_args['resp']
        self.uri_fields = request_handler_args['uri_fields']
        self.operation_id = request_handler_args['operation_id']
        self.list_id_url = self.uri_fields['list_id']
        self.content_type = self.req.content_type
        self.encoding = ''
        self.setup_database()
        self.get_post_data_as_file(max_bytes=self.maxbytes)
        self.get_file_size()
        self.process_upload()

    def process_upload(self):
        logger.debug('processing upload')
        if self.operation_id == 'ArchivesVARMessagesPOST':
            # Emails from Mailman are uploaded to the archive via this operation.
            self.handle_upload_email()
        elif self.operation_id == 'ArchivesVARFilesVARVARPOST':
            # Email files are uploaded to the archive via this operation.
            self.message_id_hash = self.uri_fields['message_id_hash']
            if not self.does_message_exist():
                print('MESSAGE NOT FOUND, 404')
                # cannot upload files to a message that does not exist
                raise HTTPError(status.HTTP_404, title=status.HTTP_404, description=status.HTTP_404)
            self.file_name = self.uri_fields['file_name']
            self.handle_upload_file()
        else:
            # Unknown operation.
            raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=status.HTTP_400)

    def does_message_exist(self):
        #look up this message_id from the messages table
        s = select([self.table_messages.c.message_id_hash]).where(self.table_messages.c.message_id_hash==self.message_id_hash)
        result = self.conn.execute(s)
        message_key = result.first()
        if message_key:
            if message_key[0] == self.message_id_hash:
                return True

    def make_message_id_hash(self, message_id):
        # Mailman uses a hash of the message-id to identify messages.
        # we use this hash exclusively within the archives as the message.id
        # this function creates a message_id_hash that is (should be) the same as the Mailman hash
        if isinstance(message_id, bytes):
            message_id = message_id.decode('ascii')
        message_id = message_id.strip()
        # if message_id is enclosed in angle brackets, remove them
        message_id = re.sub(r'^<|>$', '', message_id)
        return b32encode(sha1(message_id.encode('utf-8')).digest()).decode('ascii')

    def parse_email(self):
        self.file.seek(0)
        # there is a bug in Python 3.4 that closes the input file used to parse an email, so we copy before parsing.
        self.email_file = io.BytesIO(self.file.read())
        try:
            self.msg = BytesHeaderParser(policy=policy.default).parse(self.email_file)
        except:
            description = 'Problem parsing email'
            raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=description)

        # emails must have an Message-Id header
        if not self.msg.get('message-id', None):
            description = 'Upload failed.  Email does not contain a message-id header.'
            raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=description)

        # emails must have a date header
        if not self.msg.get('date', None):
            description = 'Upload failed.  Email does not contain a date header.'
            raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=description)

        # respect the "no archive" headers
        if self.msg.get('X-No-Archive', None) or (self.msg.get('x-archive', '').lower() == 'no'):
            description = 'Message includes X-No-Archive or X-Archive and will not be archived.'
            raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=description)

        self.message_id_hash = self.make_message_id_hash(self.msg.get('message-id'))

        msg_data_fields = [ 'message_id_header',
                            'message_id_hash',
                            'in_reply_to',
                            'references_header',
                            'list_id',
                            'archived_at',
                            'denormalized',
                            'subject',
                            'from_address',
                            'from_address_full',
                            'from_address_display_name',
                            'date',
                            'date_tzinfo',
                            'date_iso8601',
                            'date_mmyyyy',
                            'date_mmm_yyyy',
                            'date_dd_mmm_yyyy',
                            'date_dd_ddd_mmm_yyyy_hhmmxx',
                            'date_utc',
                            'date_numday',
                            'date_nummonth',
                            'date_numyear',
                            'date_epochseconds',
                            'date_epochday',
                            'date_epochweek',
                            'date_epochmonth'
                            ]
        self.msgdata  = {}
        self.msgdata['message_id_header'] = self.msg.get('message-id')
        self.msgdata['message_id_hash'] = self.make_message_id_hash(self.msgdata['message_id_header'])
        self.msgdata['in_reply_to'] = self.msg.get('in-reply-to', '')
        self.msgdata['references_header'] = self.msg.get('references', '')
        """
        Mailman adds a list-id header which we store. It identifies the mailing list that the email came from.
        we do not require a message to have a list-id header, but if it does, it must match the URL list-id
        this is to avoid mix ups where somehow messages from the wrong list are going into the archive.
        """
        if self.msg.get('list-id', None):
            self.msgdata['list_id'] = self.msgdata['list_id'].strip()
            # if list_id is enclosed in angle brackets, remove them
            self.msgdata['list_id'] = re.sub(r'^<|>$', '', self.msgdata['list_id'])
            # check that the mailing list id in the email matches the mailing list id from the inbound URL
            if self.msgdata['list_id'] != self.list_id_url:
                description = 'email list_id does not match url list_id and will not be archived.'
                raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=description)
        self.msgdata['list_id'] = self.uri_fields['list_id']
        # Mailman adds an archived-at header which we store. It identifies the URL location of the archived email.
        self.msgdata['archived_at'] = self.msg.get('archived-at', '')
        self.msgdata['subject'] = self.msg.get('subject', '')
        from_username = self.msg['from'].addresses[0].username
        from_domain = self.msg['from'].addresses[0].domain
        self.msgdata['from_address'] = from_username + '@' + from_domain
        self.msgdata['from_address_full'] = self.msg['from']
        self.msgdata['from_address_display_name'] = self.msg['from'].addresses[0].display_name
        msg_date = self.msg['date'].datetime
        msg_datetuple = msg_date.timetuple()
        self.msgdata['date'] = msg_date
        self.msgdata['date_tzinfo'] = str(msg_date.tzinfo)
        self.msgdata['date_iso8601'] = str(msg_date.isoformat())
        self.msgdata['date_mmyyyy'] = str(msg_date.strftime('%m%Y'))
        self.msgdata['date_mmm_yyyy'] = str(msg_date.strftime('%b %Y'))
        self.msgdata['date_dd_mmm_yyyy'] = str(msg_date.strftime('%d %b %Y'))
        self.msgdata['date_dd_ddd_mmm_yyyy_hhmmxx'] = str(msg_date.strftime('%a %d %b %Y %H:%M%p'))
        self.msgdata['date_utc'] = msg_date.astimezone()
        self.msgdata['date_numday'] = str(msg_datetuple[2]).zfill(2)
        nummonth = msg_datetuple[1]
        self.msgdata['date_nummonth'] = str(nummonth).zfill(2)
        numyear = msg_datetuple[0]
        self.msgdata['date_numyear'] = str(numyear)
        self.msgdata['date_epochseconds'] = int(calendar.timegm(msg_datetuple))
        self.msgdata['date_epochday'] = int(calendar.timegm(msg_datetuple)/86400)
        self.msgdata['date_epochweek'] = int(calendar.timegm(msg_datetuple)/86400/7)
        epochmonth = ((numyear - 1970 - 1) * 12) + nummonth
        self.msgdata['date_epochmonth'] = int(epochmonth)
        denormalized_fields = [ 'message_id_hash',
                            'list_id',
                            'archived_at',
                            'subject',
                            'from_address',
                            'from_address_full',
                            'from_address_display_name',
                            'date_tzinfo',
                            'date_iso8601',
                            'date_epochseconds',
                            ]
        denormalized = {x: self.msgdata[x] for x in denormalized_fields}
        self.msgdata['denormalized'] = json.dumps(denormalized)
        print(json.dumps(denormalized, indent=4))


    def save_message_to_database(self):
        # OR REPLACE gets us upsert functionality in sqlite
        print('Saving message {}'.format(self.msgdata['message_id_hash']))
        ins = self.table_messages.insert()\
            .prefix_with("OR REPLACE")\
            .values(**self.msgdata)
        print(str(ins))
        result = self.conn.execute(ins)
        return result.inserted_primary_key

    def handle_upload_email(self):
        """
        We do the barest minimum to land an email file into the database:
        - Parse the headers and get the message-id so we can create a hash to use as message.id
        - We also grab the list-id and archived-at headers
        - We then add this email to the messages table and save the email source to the message_binary_files table.
        """
        print('handle_upload_email')
        print(self.content_type)
        self.parse_email()
        with self.conn.begin() as transaction:
            # create message record
            result = self.save_message_to_database()
            # break the email addresses out into the appropriate tables
            self.process_message_addresses()
            # save email source as binary file
            if result:
                self.file_name = self.message_id_hash + '.eml'
                self.content_type= 'message/rfc822'
                id = self.save_binary_file_to_database()
                print(id)
                self.update_message_add_source_file_key(id)

    def handle_upload_file(self):
        if not self.does_message_exist():
            # the message_id specified in the route must exist in the database
            raise HTTPError(status.HTTP_404, title=status.HTTP_404, description=status.HTTP_404)
        self.save_binary_file_to_database()
        # if file is a text file, add to FTS
        if self.content_type == 'text/plain':
            self.add_file_to_fulltext_index()
        self.resp.status = status.HTTP_201
        # finished

    def get_file_size(self):
        self.file.seek(0, 2)
        self.file_size = self.file.tell()

    def setup_database(self):
        self.Base = automap_base()
        self.engine = create_engine("sqlite:////home/ubuntu/crowdwave/src/crowdwave/archives.db")
        self.conn = self.engine.connect()

        # ensure that sqlite foreign keys are switched on
        def _fk_pragma_on_connect(dbapi_con, con_record):
            print('_fk_pragma_on_connect')
            dbapi_con.execute('pragma foreign_keys=ON')
        event.listen(self.engine, 'connect', _fk_pragma_on_connect)

        self.Base.prepare(self.engine, reflect=True)     # reflect the tables
        self.meta = MetaData()
        self.table_messages = Table('messages', self.meta, autoload=True, autoload_with=self.engine)
        self.table_message_binary_files = Table('message_binary_files', self.meta,
                                                autoload=True, autoload_with=self.engine)
        self.table_addresses = Table('addresses', self.meta, autoload=True, autoload_with=self.engine)
        self.table_recipients = Table('recipients', self.meta, autoload=True, autoload_with=self.engine)

    def get_post_data_as_file(self, max_bytes=None):
        """
        This reads the POST data as file up to max_bytes size.
        Careful this is not the same as a multipart file upload handler, it assumes that the entire post data is the file.
        The request stream is consumed in the process and cannot be rewound.

        max_bytes = 0 means no limit on file size
        """
        self.file = io.BytesIO()
        logger.debug('get_post_data_as_file')
        max_bytes = max_bytes or 0
        bytes_read = 0
        chunksize = 65536
        while True:
            chunk = self.req.stream.read(chunksize)
            bytes_read += len(chunk)
            print(bytes_read)
            if (bytes_read > max_bytes) and (max_bytes != 0):
                raise HTTPError(status.HTTP_413, title=status.HTTP_413, description=status.HTTP_413)
            if not chunk:
                break
            self.file.write(chunk)
        self.file.seek(0)


    def process_message_addresses(self):
        """
        Processes each of the address headers in the email and saves each email address to the database.
        """
        address_headers = ['to', 'from', 'cc', 'reply-to', 'delivered-to']
        for header_name in address_headers:
            header_data = self.msg.get(header_name, None)
            if header_data:
                for address in header_data.addresses:
                    address_id = self.save_address_to_database(\
                        **{ 'full_address': str(address),
                        'display_name': address.display_name,
                        'username': address.username,
                        'domain': address.domain,
                        'address': address.username + '@' + address.domain}
                    )
                    self.link_message_to_address(address_id=address_id, header_name=header_name)

    def link_message_to_address(self, address_id=None, header_name=None):
        """
        Creates a recipient record linking a message to an address
        """

        # OR REPLACE gets us upsert functionality in sqlite
        ins = self.table_recipients.insert()\
            .prefix_with("OR REPLACE")\
            .values(    message_id_hash = self.message_id_hash,
                        address_id = address_id,
                        header_name = header_name,
                        )
        print('Saving recipient {}'.format(address_id))
        print(str(ins))
        result = self.conn.execute(ins)
        return result.inserted_primary_key[0]

    def save_address_to_database(self, **address):
        """
        Saves one address to the database.
        """

        # OR REPLACE gets us upsert functionality in sqlite
        ins = self.table_addresses.insert()\
            .prefix_with("OR REPLACE")\
            .values(    full_address=address['full_address'],
                        display_name=address['display_name'],
                        username = address['username'],
                        domain = address['domain'],
                        address=address['address'],
                        )
        print('Saving address {}'.format(address['address']))
        print(str(ins))
        result = self.conn.execute(ins)
        return result.inserted_primary_key[0]

    def update_message_add_source_file_key(self, id):
        # updates a message so that it includes a foreign key pointing to its binary source file
        upd = self.table_messages.update().values(binary_source_file_id=id)
        result = self.conn.execute(upd)
        return result

    def save_binary_file_to_database(self):
        self.file.seek(0)
        ins = self.table_message_binary_files.insert()\
            .prefix_with("OR REPLACE")\
            .values(    message_id_hash=self.message_id_hash,
                        content_type=self.content_type,
                        encoding=self.encoding,
                        file_data=self.file.read(),
                        file_name=self.file_name,
                        file_size=self.file_size,
                        )
        print(str(ins))
        print('Inserting binary file {}'.format(self.message_id_hash))
        result = self.conn.execute(ins)
        print('Finished inserting binary file {}'.format(self.message_id_hash))
        return result.inserted_primary_key[0]

    def add_file_to_fulltext_index(self):
        content = self.file.read().decode('utf-8')
        """
        fts_data = self.Fts_data(message_id=self.message_id,
                                      content=content,
                                      file_name=self.file_name)
        self.session.add(fts_data)
        self.session.commit()

        """
        ftsdata = Table('fts_data', self.meta, autoload=True, autoload_with=self.engine)
        content = self.file.read().decode('utf-8')
        ins = ftsdata.insert().prefix_with("OR REPLACE").values(message_id_hash=self.message_id_hash,
                                      content=content,
                                      file_name=self.file_name)
        #print(str(ins))
        result = self.conn.execute(ins)

def archive_message_get_file(**request_handler_args):
    pass

def archive_message_delete_file(**request_handler_args):
    pass

"""
def _validate_request_params(uri_fields=None):
    logger.debug(uri_fields)
    valid, reason = permissions().validate_params(  role=uri_fields.get('role', None),
                                                    resource_id=uri_fields.get('resource_id', None),
                                                    resource_type=uri_fields.get('resource_type', None),
                                                    user_identifier=uri_fields.get('user_identifier', None))
    if not valid:
        raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=reason)

def permission_query(**request_handler_args):
    # this a request handler
    uri_fields = request_handler_args['uri_fields']
    req = request_handler_args['req']
    if req.method != 'GET':
        raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
    logger.debug('Request to query permissions: {}'.format(uri_fields))
    _validate_request_params(uri_fields=uri_fields)
    _result =     permissions().query(  role=uri_fields['role'],
                                        resource_id=uri_fields['resource_id'],
                                        resource_type=uri_fields['resource_type'],
                                        user_identifier=uri_fields['user_identifier'])
    # _result is a boolean
    if _result:
        request_handler_args['resp'].status = status.HTTP_204
    else:
        reason = 'Query permissions failed'
        raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=reason)

def permission_set(**request_handler_args):
    # this a request handler
    uri_fields = request_handler_args['uri_fields']
    req = request_handler_args['req']
    if req.method != 'POST':
        raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
    logger.debug('Request to set permissions: {}'.format(uri_fields))
    _validate_request_params(uri_fields=uri_fields)
    _result = permissions().set(role=uri_fields['role'],
                    resource_id=uri_fields['resource_id'],
                    resource_type=uri_fields['resource_type'],
                    user_identifier=uri_fields['user_identifier'])
    logger.debug('_result.status_code SET')
    if _result:
        request_handler_args['resp'].status = status.HTTP_201
    else:
        reason = 'Set permissions failed'
        raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=reason)

def permission_delete(**request_handler_args):
    # this a request handler
    uri_fields = request_handler_args['uri_fields']
    req = request_handler_args['req']
    if req.method != 'DELETE':
        raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
    logger.debug('Request to delete permissions: {}'.format(uri_fields))
    _validate_request_params(uri_fields=uri_fields)
    _result = permissions().delete( role=uri_fields['role'],
                                    resource_id=uri_fields['resource_id'],
                                    resource_type=uri_fields['resource_type'],
                                    user_identifier=uri_fields['user_identifier'])
    if _result:
        request_handler_args['resp'].status = status.HTTP_204
    else:
        reason = 'Delete permissions failed'
        raise HTTPError(status.HTTP_400, title=status.HTTP_400, description=reason)
"""
