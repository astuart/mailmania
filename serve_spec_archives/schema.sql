
PRAGMA schema_version = 1;
PRAGMA foreign_keys = ON;
-- ##########################MESSAGES
-- the id field contains the X_message_id_hash_Hash
CREATE TABLE messages (
  message_id_hash TEXT PRIMARY KEY,
  in_reply_to TEXT,
  references_header TEXT,
  from_address TEXT COLLATE NOCASE,
  from_address_full  TEXT COLLATE NOCASE,
  from_address_display_name  TEXT COLLATE NOCASE,
  subject TEXT,
  message_id_header TEXT NOT NULL,
  list_id TEXT NOT NULL,
  archived_at TEXT,
  denormalized TEXT,
  binary_source_file_id INTEGER,
  spam INTEGER DEFAULT 0,
  has_attachments INTEGER DEFAULT 0,
  has_notes INTEGER DEFAULT 0,
  date DATETIME,
  date_tzinfo TEXT,
  date_iso8601 TEXT,
  date_mmyyyy TEXT,
  date_mmm_yyyy TEXT,
  date_dd_mmm_yyyy TEXT,
  date_dd_ddd_mmm_yyyy_hhmmxx TEXT,
  date_utc DATETIME,
  date_numday TEXT,
  date_nummonth TEXT,
  date_numyear TEXT,
  date_epochseconds INTEGER,
  date_epochday INTEGER,
  date_epochweek INTEGER,
  date_epochmonth INTEGER,
  sticky INTEGER,
  flagged_for_moderation INTEGER DEFAULT 0,
  processing_state TEXT,
  processing_start_time INTEGER,
  processing_failed_attempts INTEGER,
  encoding TEXT,
  root_status INTEGER DEFAULT -1,
  conversation_position INTEGER DEFAULT -1,
  conversation_id INTEGER DEFAULT -1,
  fuzzy_ancestor INTEGER DEFAULT NULL,
  FOREIGN KEY(binary_source_file_id) REFERENCES message_binary_files(id)
);

CREATE INDEX date_sent_index ON messages(date);
CREATE INDEX message_conversation_id_conversation_position_index ON messages(conversation_id, conversation_position);
CREATE INDEX message_root_status ON messages(root_status);
CREATE INDEX message_from_address_display_name_index ON messages(from_address_display_name);

CREATE TRIGGER after_delete_message AFTER DELETE ON messages
  BEGIN
    -- DELETE FROM threads WHERE threads.message_id_hash = OLD.message_id_hash;
    -- DELETE FROM attachments WHERE attachments.message_id_hash = OLD.message_id_hash;
    DELETE FROM recipients WHERE recipients.message_id_hash = OLD.message_id_hash;
    DELETE FROM notes WHERE notes.message_id_hash = OLD.message_id_hash;
    DELETE FROM addresses WHERE id = OLD.sender_address_id AND ((SELECT COUNT() FROM messages WHERE sender_address_id = OLD.sender_address_id LIMIT 1) = 0) AND ((SELECT COUNT() FROM recipients WHERE address_id = OLD.sender_address_id LIMIT 1) = 0);
    DELETE FROM folders_map WHERE folders_map.message_id_hash = OLD.message_id_hash;
    DELETE FROM message_binary_files WHERE message_binary_files.message_id_hash = OLD.message_id_hash;
    DELETE FROM list_of_searchable_files WHERE list_of_searchable_files.message_id_hash = OLD.message_id_hash;

    UPDATE messages SET fuzzy_ancestor = -1 WHERE messages.fuzzy_ancestor = OLD.message_id_hash;
  END;

-- ##########################ADDRESSES
CREATE TABLE addresses (
  id INTEGER PRIMARY KEY,
  full_address TEXT COLLATE NOCASE,
  address TEXT COLLATE NOCASE,
  display_name TEXT COLLATE NOCASE,
  username TEXT COLLATE NOCASE,
  domain TEXT COLLATE NOCASE,
  UNIQUE(address)
  );
CREATE INDEX addresses_address_index ON addresses(address);

-- ##########################ATTACHMENTS
-- CREATE TABLE attachments (id INTEGER PRIMARY KEY, message_id_hash INTEGER, name, type, UNIQUE(message_id_hash, name));
-- CREATE INDEX attachments_type_index ON attachments(type);

-- ##########################RECIPIENTS
CREATE TABLE recipients (
  id INTEGER PRIMARY KEY,
  message_id_hash TEXT NOT NULL,
  address_id INTEGER NOT NULL,
  header_name TEXT,
  FOREIGN KEY(message_id_hash) REFERENCES messages(message_id_hash),
  FOREIGN KEY(address_id) REFERENCES addresses(id)
);

CREATE INDEX recipients_address_index ON recipients(address_id);
CREATE INDEX recipients_message_id_hash_index ON recipients(message_id_hash);

-- ##########################folders
-- on delete of folders_text should cascade to delete all folders from labal_map
CREATE TABLE folders_text (
  id INTEGER PRIMARY KEY,
  user_id TEXT NOT NULL,
  text TEXT NOT NULL
);

CREATE TABLE folders_map (
  id INTEGER PRIMARY KEY,
  user_id TEXT NOT NULL,
  folders_text_id TEXT NOT NULL,
  message_id_hash TEXT NOT NULL,
  FOREIGN KEY(folders_text_id) REFERENCES folders_text(id),
  FOREIGN KEY(message_id_hash) REFERENCES messages(message_id_hash),
  UNIQUE(message_id_hash, folders_text_id, user_id)
);
CREATE INDEX folders_map_folders_text_id_index ON folders_map(folders_text_id);
CREATE INDEX folders_map_message_id_hash_index ON folders_map(message_id_hash);
CREATE INDEX folders_map_user_id_index ON folders_map(user_id);


-- ##########################THREADS
-- CREATE TABLE threads (id INTEGER PRIMARY KEY, message_id_hash INTEGER, reference, is_originator);
-- CREATE INDEX references_message_id_hash_index ON threads(message_id_hash);
-- CREATE INDEX references_reference_index ON threads(reference);


-- ##########################NOTES
CREATE TABLE notes (
  id INTEGER PRIMARY KEY,
  user_id TEXT NOT NULL,
  author_address TEXT COLLATE NOCASE,
  author_address_full  TEXT COLLATE NOCASE,
  author_address_display_name  TEXT COLLATE NOCASE,
  date_created_tzinfo TEXT NOT NULL,
  date_created_utc DATETIME NOT NULL,
  note_text TEXT NOT NULL,
  message_id_hash TEXT REFERENCES messages(message_id_hash)
);
CREATE INDEX notes_date_created_utc_index ON notes(date_created_utc);
CREATE INDEX notes_message_id_hash_index ON notes(message_id_hash);

-- ##########################BINARY FILES
CREATE TABLE message_binary_files(
  id INTEGER PRIMARY KEY,
  message_id_hash TEXT,
  content_type TEXT NOT NULL,
  encoding TEXT,
  compressed INTEGER DEFAULT 0,
  file_data BLOB,
  file_name TEXT NOT NULL,
  file_size INTEGER NOT NULL,
  UNIQUE(message_id_hash, file_name ),
  FOREIGN KEY(message_id_hash) REFERENCES messages(message_id_hash)
);

-- ##########################FULL TEXT SEARCH
  -- message_id_hash TEXT REFERENCES messages(message_id_hash),
CREATE TABLE list_of_searchable_files(
  id INTEGER PRIMARY KEY,
  message_id_hash TEXT,
  file_name TEXT NOT NULL,
  UNIQUE(message_id_hash, file_name)
);

CREATE VIRTUAL TABLE content_of_searchable_files USING fts4();

CREATE TRIGGER content_of_searchable_files_DELETE AFTER DELETE ON list_of_searchable_files
BEGIN
  DELETE FROM content_of_searchable_files WHERE rowid = OLD.rowid;
END;

CREATE VIEW fts_data AS
  SELECT list_of_searchable_files.rowid AS rowid, list_of_searchable_files.message_id_hash, list_of_searchable_files.file_name , content_of_searchable_files.Content
  FROM list_of_searchable_files JOIN content_of_searchable_files ON list_of_searchable_files.rowid = content_of_searchable_files.rowid;

CREATE TRIGGER fts_data_INSERT INSTEAD OF INSERT ON fts_data
BEGIN
  INSERT INTO list_of_searchable_files(message_id_hash, file_name ) VALUES (NEW.message_id_hash, NEW.file_name );
  INSERT INTO content_of_searchable_files (rowid, Content) VALUES (last_insert_rowid(), NEW.Content);
END;

CREATE TRIGGER fts_data_DELETE INSTEAD OF DELETE ON fts_data
BEGIN
  DELETE FROM list_of_searchable_files WHERE rowid = OLD.rowid;
  DELETE FROM content_of_searchable_files WHERE rowid = OLD.rowid;
END;

