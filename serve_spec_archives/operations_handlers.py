# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from serve_security.jsonwebtoken import json_web_token as jwtoken

# operation_handlers maps Swagger operations to a list of request handlers.
from serve_spec_archives.operations_servers import *
from serve_security.authorization_handlers import  (requires_serverowner,
                                                    requires_listowner,
                                                    requires_listmember,
                                                    requires_listmoderator, )

# When a request comes in, specserver works out which operation is being requested.
# It then executes the list of callables for that operation.
# if auth function returns True, original request will be proxied to upstream application server
# if auth function does not return True, then a 403 Forbidden is returned
# if an inbound operation is not found in this list then a 404 is returned

# a request handler MUST be compatible with the following argument signature:
# func(req, resp, config, *args, uri_fields=None, form_fields=None, **kwargs)

# changes that you make to the response will fall through to the next handler until either all handlers
# complete or one of them returns the response

# the operation names must precisely match those found in the Swagger spec.

# you can immediately return a simplehttperror such as code403Forbidden if you like

operation_handlers = {

    'ArchivesVARMessagesGET':
        [jwtoken.validate, (requires_serverowner,
                            requires_listowner,
                            requires_listmember,
                            requires_listmoderator, ), ArchivesVARMessagesGET],

    'ArchivesVARMessagesPOST':
        [ArchivesVARMessagesPOST],

    'ArchivesVARMessagesVARGET':
        [jwtoken.validate, (requires_serverowner,
                            requires_listowner,
                            requires_listmember,
                            requires_listmoderator, ), ArchivesVARMessagesVARGET],

    'ArchivesVARMessagesVARDELETE':
        [jwtoken.validate, (requires_serverowner,
                            requires_listowner,
                            requires_listmember,
                            requires_listmoderator, ), ArchivesVARMessagesVARDELETE],

    'ArchivesVARFilesVARVARGET':
        [jwtoken.validate, (requires_serverowner,
                            requires_listowner,
                            requires_listmember,
                            requires_listmoderator, ), ArchivesVARFilesVARVARGET],

    'ArchivesVARFilesVARVARPOST':
        [ArchivesVARFilesVARVARPOST],

    'ArchivesVARFilesVARVARDELETE':
        [jwtoken.validate, (requires_serverowner,
                            requires_listowner,
                            requires_listmember,
                            requires_listmoderator, ), ArchivesVARFilesVARVARDELETE],

    'ArchivesVARNotesMessageVARGET':
            [jwtoken.validate, (requires_serverowner,
                                requires_listowner,
                                requires_listmember,
                                requires_listmoderator, ), ArchivesVARNotesMessageVARGET],

    'ArchivesVARNotesMessageVARPOST':
            [jwtoken.validate, (requires_serverowner,
                                requires_listowner,
                                requires_listmember,
                                requires_listmoderator, ), ArchivesVARNotesMessageVARPOST],

    'ArchivesVARFoldersVARDELETE':
            [jwtoken.validate, (requires_serverowner,
                                requires_listowner,
                                requires_listmember,
                                requires_listmoderator, ), ArchivesVARFoldersVARDELETE],

    'ArchivesVARFoldersVARGET':
            [jwtoken.validate, (requires_serverowner,
                                requires_listowner,
                                requires_listmember,
                                requires_listmoderator, ), ArchivesVARFoldersVARGET],

    'ArchivesVARFoldersVARPOST':
            [jwtoken.validate, (requires_serverowner,
                                requires_listowner,
                                requires_listmember,
                                requires_listmoderator, ), ArchivesVARFoldersVARPOST],

    'ArchivesVARFoldersGET':
            [jwtoken.validate, (requires_serverowner,
                                requires_listowner,
                                requires_listmember,
                                requires_listmoderator, ), ArchivesVARFoldersGET],

    'ArchivesVARFoldersPOST':
            [jwtoken.validate, (requires_serverowner,
                                requires_listowner,
                                requires_listmember,
                                requires_listmoderator, ), ArchivesVARFoldersPOST],

    'ArchivesVARNotesVARDELETE':
            [jwtoken.validate, (requires_serverowner,
                                requires_listowner,
                                requires_listmember,
                                requires_listmoderator, ), ArchivesVARNotesVARDELETE],

    'ArchivesVARNotesVARGET':
            [jwtoken.validate, (requires_serverowner,
                                requires_listowner,
                                requires_listmember,
                                requires_listmoderator, ), ArchivesVARNotesVARGET],

}


