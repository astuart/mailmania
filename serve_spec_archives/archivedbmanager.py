# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from serve_spec_archives.settings import config
from serve_common.sqlalchemy_base import db
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import event, create_engine, MetaData, Table
from sqlalchemy.ext.automap import automap_base
import os
from serve_spec_archives import helpers
import sqlite3
import logging

logger = logging.getLogger('root.' + __name__)


class ArchiveDbManager():

    """
    This is effectively a dict of database objects.

    Each database object points to a different mailing list archive database.

    We cache them.
    """

    def __init__(self):
        self.cache = {}

    def db_cache_get(self, cachekey):
        logger.debug(self.cache.keys())
        return self.cache.get(cachekey, None)

    def db_cache_set(self, cachekey, value):
        self.cache[cachekey] = value
        return value

    def __getitem__(self, list_id):
        print('Fetching database: {}'.format(list_id))
        # first try to get the db object from the cache
        db = self.db_cache_get(list_id)
        if db:
            logger.debug('Got database from cache: {}'.format(list_id))
            return db
        # database object not in cache, check if the file exists on the file system
        database_name = list_id + '.db'
        database_path = os.path.join(config['archives_database_folder'] + '/' + database_name)
        if not os.path.exists(database_path):
            logger.debug('Database file does not exist, creating: {}'.format(list_id))
            # database file does not exist, create it
            self.create_archive_database(list_id)
        db = self.create_db_objects(list_id)
        self.db_cache_set(list_id, db)
        return db

    def create_db_objects(self, list_id):
        database_name = list_id + '.db'
        database_path = os.path.join(config['archives_database_folder'] + '/' + database_name)
        database_url = 'sqlite:///' + database_path
        db = {}
        db['database_url'] = database_url
        db['Base'] = automap_base()
        #db['engine'] = create_engine(database_url, echo=True)
        db['engine'] = create_engine(database_url)
        db['connection'] = db['engine'].connect()
        db['Base'].prepare(db['engine'], reflect=True)     # reflect the tables
        db['meta'] = MetaData()
        db['table_messages'] = \
            Table('messages', db['meta'], autoload=True, autoload_with=db['engine'])
        db['table_message_binary_files'] = \
            Table('message_binary_files', db['meta'], autoload=True, autoload_with=db['engine'])
        db['table_addresses'] = \
            Table('addresses', db['meta'], autoload=True, autoload_with=db['engine'])
        db['table_recipients'] = \
            Table('recipients', db['meta'], autoload=True, autoload_with=db['engine'])
        session_factory = sessionmaker(bind=db['engine'])
        Session = scoped_session(session_factory)
        db['session'] = Session()

        #logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

        @event.listens_for(db['engine'], 'connect')
        def set_sqlite_pragma(dbapi_connection, connection_record):
            logger.debug('got connect event, setting foreign_keys=ON')
            dbapi_connection.execute("PRAGMA foreign_keys=ON;")
            logger.debug('got connect event, setting journal_mode=WAL')
            dbapi_connection.execute("PRAGMA journal_mode=WAL;")
            result = dbapi_connection.execute("PRAGMA foreign_keys")
            for row in result:
                logger.debug("pragma foreign_keys: {}".format(row))
            result = dbapi_connection.execute("PRAGMA journal_mode")
            for row in result:
                logger.debug("pragma journal_mode: {}".format(row))

        """
        @event.listens_for(db['engine'], 'commit')
        def receive_commit(dbapi_connection):
            logger.debug('got commit event, doing WAL checkpoint')
            dbapi_connection.execute("PRAGMA wal_checkpoint(TRUNCATE);")
        """

        return db

    #http://zarko-gajic.iz.hr/sqlite-referential-integrity-with-full-text-search-virtual-tables-used-in-a-delphi-application/
    def create_archive_database(self, list_id):
        database_name = list_id + '.db'
        database_path = os.path.join(config['archives_database_folder'] + '/' + database_name)
        logger.debug(database_path)

        conn = sqlite3.connect(database_path)
        with open('serve_spec_archives/schema.sql') as f:
            conn.executescript(f.read())
        conn.commit()


