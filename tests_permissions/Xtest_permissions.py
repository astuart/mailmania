from tests_common.fixtures import *
from client_spec_permissions.client_permissions import *
from tests_common.common import CommonTests
from tests_common.helpers import helpers

class Test_permissionsVARVARVARVARGET(CommonTests):

    requestor_class = permissionsVARVARVARVARGET

    urlvars_dict = {    'resource_type' : 'domain',
                        'resource_id' : 'mail.example.org',
                        'role' : 'domainowner',
                        'user_identifier' : 'testuser1@mail.example.org',
    }

    # required role:
    # serverowner

    # test serverowner can do
    def do_test(self, jwt_header=None):
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        return r.status_code

    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_test_mode_serverowner()
        assert(self.do_test(jwt_header=jwt_header) == 204)


    def test_cannot_access_with_user_rights(self, requires_testuser2,
                                            requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)


class Test_permissionsVARVARVARVARDELETE(CommonTests):

    requestor_class = permissionsVARVARVARVARDELETE

    urlvars_dict = {    'resource_type' : 'domain',
                        'resource_id' : 'mail.example.org',
                        'role' : 'domainowner',
                        'user_identifier' : 'testuser1@mail.example.org',
    }

    # required role:
    # serverowner

    # test serverowner can do
    def do_test(self, jwt_header=None):
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=jwt_header)
        return r.status_code

    def test_can_access_with_serverowner_rights(self, requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_test_mode_serverowner()
        assert(self.do_test(jwt_header=jwt_header) == 204)


    def test_cannot_access_with_user_rights(self, requires_testuser2,
                                            requires_testlist1_with_subscriber_testuser1_with_role_domainowner):
        jwt_header = helpers.login_as_testuser2ATmailDOTexampleDOTorg()
        assert(self.do_test(jwt_header=jwt_header) == 401)



class Test_permissionsVARVARVARVARPOST(CommonTests):

    # todo write test
    requestor_class = permissionsVARVARVARVARPOST
    # serverowner
    # domain admin
    # list admin

    urlvars_dict = {    'resource_type' : 'domain',
                        'resource_id' : 'mail.example.org',
                        'role' : 'domainowner',
                        'user_identifier' : 'testuser1@mail.example.org',
    }


    def test_nothing(self):
        assert(True)

