from tests_common.spec_coverage import SpecCoverBase
from serve_spec_permissions.operations_handlers import operation_handlers
from serve_spec_permissions.operations_resource_locators import operation_resource_locators
import client_spec_permissions
from tests_common.helpers import helpers
from tests_common.fixtures import *

class Test_SpecCoverage(SpecCoverBase):

    requestor_module = client_spec_permissions
    excluded_operations = set([])
    swagger_spec = helpers.load_swagger_spec('serve_spec_permissions/api-docs.json')
    operations = set(helpers.get_operations_from_swagger_spec(swagger_spec))
    operations = operations - excluded_operations

    @pytest.mark.parametrize('operationId', operations)
    def test_is_operation_in_operation_handlers(self, operationId):
        assert(operationId in operation_handlers.keys())

    @pytest.mark.parametrize('operationId', operations)
    def test_is_operation_in_operation_resource_locators(self, operationId):
        assert(operationId in operation_resource_locators.keys())

    @pytest.mark.parametrize('operationId', operations)
    def test_does_operation_have_a_client_accessor_function(self, operationId):
        # do we have a class accessor function?
        try:
            # try to load the class with the same name as the operationid
            getattr(self.requestor_module, operationId)
            assert(True)
        except:
            assert(False)
