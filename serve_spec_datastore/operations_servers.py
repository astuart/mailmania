# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import logging

logger = logging.getLogger('root.' + __name__)

class datastore():

    """
    GET datastore\resource_type\visibility\resource_id
    POST datastore\resource_type\visibility\resource_id
    DELETE datastore\resource_type\visibility\resource_id
    data = {'key': 'key', 'value': 'value'}
    """

    #resource_type = ['user','domain','list','server']
    #visibility = ['private','public']
    #resource_id = whatever_mailman_users_to_identify_the_resource

    @staticmethod
    def data_get(**request_handler_args):
        pass

    @staticmethod
    def data_post(**request_handler_args):
        pass

    @staticmethod
    def data_delete(**request_handler_args):
        pass




