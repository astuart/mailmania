# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from serve_security.jsonwebtoken import json_web_token as jwtoken
from serve_spec_mailman_rest_api.operations_servers import login, proxy_to_mailman
from serve_security.authorize import requires_serverowner, requires_userowner, requires_listowner, \
    requires_listmember, requires_listmoderator


# operation_handlers maps Swagger operations to a list of request handlers.

# When a request comes in, specserver works out which operation is being requested.
# It then executes the list of callables for that operation.
# if auth function returns True, original request will be proxied to upstream application server
# if auth function does not return True, then a 403 Forbidden is returned
# if an inbound operation is not found in this list then a 404 is returned

# a request handler MUST be compatible with the following argument signature:
# func(req, resp, config, *args, uri_fields=None, form_fields=None, **kwargs)

# changes that you make to the response will fall through to the next handler until either all handlers
# complete or one of them returns the response

# the operation names must precisely match those found in the Swagger spec.
"""
[mm_login],
[jwtoken.validate],
code403Forbidden,
'AddressesPOST':                [jwtoken.validate,
                                 (requires_serverowner, requires_userowner), is user owner of resource being requested (which is a user)?
                                 proxy_to_mailman],

"""

operation_handlers = {
    'AddressesGET':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'AddressesVARGET':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'AddressesVARMembershipsGET':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'AddressesVARPreferencesGET':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'AddressesVARUnverifyPOST':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'AddressesVARUserDELETE':
        [jwtoken.validate, (requires_serverowner, ), proxy_to_mailman],

    'AddressesVARUserGET':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'AddressesVARUserPOST':
        [jwtoken.validate, (requires_serverowner, ), proxy_to_mailman],

    'AddressesVARUserPUT':
        [jwtoken.validate, (requires_serverowner, ), proxy_to_mailman],

    'AddressesVARVerifyPOST':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'DomainsGET':
        [jwtoken.validate, proxy_to_mailman],

    'DomainsPOST':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'DomainsVARDELETE':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'DomainsVARListsGET':
        [jwtoken.validate, proxy_to_mailman],

    'ListsGET':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'ListsPOST':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'ListsStylesGET':
        [jwtoken.validate, proxy_to_mailman],

    'ListsVARArchiversGET':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'ListsVARArchiversPATCH':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'ListsVARArchiversPUT':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'ListsVARDELETE':
        [jwtoken.validate, (requires_serverowner, requires_listowner,), proxy_to_mailman],

    'ListsVARGET':
        [jwtoken.validate, (requires_serverowner, requires_listmember,), proxy_to_mailman],

    'ListsVARRosterMemberGET':
        [jwtoken.validate, (requires_serverowner, requires_listmember,), proxy_to_mailman],

    'ListsVARRosterOwnerGET':
        [jwtoken.validate, (requires_serverowner, requires_listmember,), proxy_to_mailman],

    'MembersGET':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'MembersPOST':
        [jwtoken.validate, (requires_serverowner, requires_listowner, requires_listmoderator,), proxy_to_mailman],

    'MembersVARAllPreferencesGET':
        [jwtoken.validate, (requires_serverowner, requires_memberowner), proxy_to_mailman],

    'MembersVARDELETE':
        [jwtoken.validate, (requires_serverowner, requires_listowner, requires_listmoderator, requires_memberowner), proxy_to_mailman],

    'MembersVARGET':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'MembersVARPATCH':
        [jwtoken.validate, (requires_serverowner, requires_listowner, requires_listmoderator, requires_memberowner), proxy_to_mailman],

    'MembersVARPreferencesGET':
        [jwtoken.validate, (requires_serverowner, requires_listowner, requires_listmoderator, requires_memberowner), proxy_to_mailman],

    'SystemPreferencesGET':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'SystemGET':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'UsersGET':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'UsersPOST':
        [jwtoken.validate, (requires_serverowner,), proxy_to_mailman],

    'UsersVARAddressesGET':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'UsersVARAddressesPOST':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'UsersVARDELETE':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'UsersVARGET':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'UsersVARLoginPOST':
        [login],

    'UsersVARPATCH':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'UsersVARPUT':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],

    'UsersVARPreferencesGET':
        [jwtoken.validate, (requires_serverowner, requires_userowner), proxy_to_mailman],
}



