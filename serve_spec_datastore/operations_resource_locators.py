# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.


# When a request comes in, first we need to *authenticate* the user that is requesting (i.e. prove they are who they say)
# THEN we need to *authorize* that user to access the resource that is the target of the request
# SO... we need a way to inspect the request, and identify the resource that is being requested
# This is how to identify the resource being requested, which varies from operation to operation.
# SO this specifies which request field value defines the resource being requested, which in turn allows authorization
#urifields['email']
# the value is a lambda function that returns the correct value from the request_handler_args, which is passed
# in as a parameter at the location that is calling the lambda function.
operation_resource_locators = {
'AddressesGET':                 None,
'AddressesPOST':                None,
'AddressesVARGET':              (lambda request_handler_args: request_handler_args['uri_fields'].get('email', None)),
'AddressesVARMembershipsGET':   (lambda request_handler_args: request_handler_args['uri_fields'].get('email', None)),
'AddressesVARPreferencesGET':   (lambda request_handler_args: request_handler_args['uri_fields'].get('email', None)),
'AddressesVARUnverifyPOST':     (lambda request_handler_args: request_handler_args['uri_fields'].get('email', None)),
'AddressesVARUserDELETE':       (lambda request_handler_args: request_handler_args['uri_fields'].get('email', None)),
'AddressesVARUserGET':          (lambda request_handler_args: request_handler_args['uri_fields'].get('email', None)),
'AddressesVARUserPOST':         (lambda request_handler_args: request_handler_args['uri_fields'].get('email', None)),
'AddressesVARUserPUT':          (lambda request_handler_args: request_handler_args['uri_fields'].get('email', None)),
'AddressesVARVerifyPOST':       (lambda request_handler_args: request_handler_args['uri_fields'].get('email', None)),
'DomainsGET':                   (lambda request_handler_args: request_handler_args['req'].params['id']),
'DomainsPOST':                  (lambda request_handler_args: request_handler_args['req'].params['id']),
'DomainsVARDELETE':             (lambda request_handler_args: request_handler_args['req'].params['id']),
'DomainsVARListsGET':           (lambda request_handler_args: request_handler_args['req'].params['id']),
'ListsGET':                     (lambda request_handler_args: request_handler_args['req'].params['id']),
'ListsPOST':                    (lambda request_handler_args: request_handler_args['req'].params['id']),
'ListsStylesGET':               (lambda request_handler_args: request_handler_args['req'].params['id']),
'ListsVARArchiversGET':         (lambda request_handler_args: request_handler_args['req'].params['id']),
'ListsVARArchiversPATCH':       (lambda request_handler_args: request_handler_args['req'].params['id']),
'ListsVARArchiversPUT':         (lambda request_handler_args: request_handler_args['req'].params['id']),
'ListsVARDELETE':               (lambda request_handler_args: request_handler_args['req'].params['id']),
'ListsVARGET':                  (lambda request_handler_args: request_handler_args['req'].params['id']),
'ListsVARRosterMemberGET':      (lambda request_handler_args: request_handler_args['req'].params['id']),
'ListsVARRosterOwnerGET':       (lambda request_handler_args: request_handler_args['req'].params['id']),
'MembersGET':                   (lambda request_handler_args: request_handler_args['req'].params['id']),
'MembersPOST':                  (lambda request_handler_args: request_handler_args['req'].params['id']),
'MembersVARAllPreferencesGET':  (lambda request_handler_args: request_handler_args['req'].params['id']),
'MembersVARDELETE':             (lambda request_handler_args: request_handler_args['req'].params['id']),
'MembersVARGET':                (lambda request_handler_args: request_handler_args['req'].params['id']),
'MembersVARPATCH':              (lambda request_handler_args: request_handler_args['req'].params['id']),
'MembersVARPreferencesGET':     (lambda request_handler_args: request_handler_args['req'].params['id']),
'SystemPreferencesGET':         (lambda request_handler_args: request_handler_args['req'].params['id']),
'SystemGET':                    (lambda request_handler_args: request_handler_args['req'].params['id']),
'UsersGET':                     (lambda request_handler_args: request_handler_args['req'].params['id']),
'UsersPOST':                    (lambda request_handler_args: request_handler_args['req'].params['id']),
'UsersVARAddressesGET':         (lambda request_handler_args: request_handler_args['uri_fields'].get('user_identifier', None)),
'UsersVARAddressesPOST':        (lambda request_handler_args: request_handler_args['uri_fields'].get('user_identifier', None)),
'UsersVARDELETE':               (lambda request_handler_args: request_handler_args['uri_fields'].get('user_identifier', None)),
'UsersVARGET':                  (lambda request_handler_args: request_handler_args['uri_fields'].get('user_identifier', None)),
'UsersVARLoginPOST':            (lambda request_handler_args: request_handler_args['uri_fields'].get('user_identifier', None)),
'UsersVARPATCH':                (lambda request_handler_args: request_handler_args['uri_fields'].get('user_identifier', None)),
'UsersVARPUT':                  (lambda request_handler_args: request_handler_args['uri_fields'].get('user_identifier', None)),
'UsersVARPreferencesGET':       (lambda request_handler_args: request_handler_args['uri_fields'].get('user_identifier', None)),
}


