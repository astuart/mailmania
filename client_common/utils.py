# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from urllib.parse import urlunsplit, urlencode
from pathlib import PurePath

import requests

from settings import config


def build_url(*args, netloc=None, scheme=None, pathprefix=None, query_params_dict=None):
    # each arg is a path segment

    scheme = scheme or config['mailman_auth_proxy_scheme']
    netloc = netloc or config['mailman_auth_proxy_netloc']
    pathprefix = pathprefix or config['mailman_auth_proxy_pathprefix']
    if query_params_dict:
        query = urlencode(query_params_dict)
    else:
        query = ''

    path = str(PurePath(pathprefix, *args))
    return urlunsplit((scheme, netloc, path, query, ''))



class http_req():
    # this wrap requests so we can print warnings on exceptions instead of crashing out

    @staticmethod
    def get(url, headers=None):
        return requests.get(url, headers=headers)

    @staticmethod
    def delete(url, headers=None):
        return requests.post(url, headers=headers)

    @staticmethod
    def post(url, headers=None, data=None):
        return requests.post(url, data=data, headers=headers, auth=('user', 'pass'))

    @staticmethod
    def put(url, headers=None, data=None):
        return requests.put(url, data=data, headers=headers, auth=('user', 'pass'))

    @staticmethod
    def patch(url, headers=None, data=None):
        return requests.patch(url, data=data, headers=headers, auth=('user', 'pass'))








