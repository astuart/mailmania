# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

config = {
    'basicauth_user' : 'restadmin',
    'basicauth_pass' : 'restpass',
    'token_secret' : 'replace_this_with_a_32_character_random_string',
    'api-docs.json': [  'serve_spec_mailman_rest_api/api-docs.json',
                        'serve_spec_archives/api-docs.json'],
    'token_expiration_seconds' : 60000,
    'mailman_rest_api_netloc' : 'localhost:8001',
    'mailman_rest_api_scheme' : 'http',
    'mailman_rest_api_pathprefix' : '3.0',
    'mailman_auth_proxy_netloc' : 'localhost:7001',
    'mailman_auth_proxy_scheme' : 'http',
    'mailman_auth_proxy_pathprefix' : '3.0',
    'databases': {'permissions': 'sqlite:///permissions.db',
                   'datastore': 'sqlite:///datastore.db'},
    'run_in_test_mode': True,
    'test_mode_serverowner': { 'email': 'testserverowner@mail.example.org',
                        'display_name': 'Test Serverowner',
                        'password': 'replace_this_with_a_secure_password',
                        'is_server_owner': 'yes'},
    'test_mode_users': [{ 'email': 'testuser1@mail.example.org',
                        'display_name': 'Testuser1',
                        'password': 'password'},
                        { 'email': 'testuser2@mail.example.org',
                        'display_name': 'Testuser2',
                        'password': 'password'}],
    'test_mode_domains': [{'mail_host': 'mail.example.org'}],
    'test_mode_lists': [{'list_id': 'testlist1.mail.example.org'}],
    'prod_mode_users': [{ 'email': 'serverowner@example.com',
                        'display_name': 'Mailman Serverowner',
                        'password': 'replace_this_with_a_secure_password',
                        'is_server_owner': 'yes'}],
    }

'''
###############TEST MODE

set run_in_test_mode = True to run tests.
test mode insecure because a test server_owner user needs to be created to run the tests.
if such a user is present in a production environment then anyone could log in using that
account, unless the test server_owner has a secure password in settings.py

if run_in_test_mode = True then the following resources are created on initialize
test_mode_serverowner

if run_in_test_mode != True then the following resources are deleted on initialize
test_mode_serverowner
test_mode_users
test_mode_domains
test_mode_lists

#Users must be a list of Mailman users like this:
[{ 'email': 'me@mail.example.org',
  'display_name': 'The Boss',
  'cleartext_password': 'somesecurepassword',
  'is_server_owner': False}]
'''

# token_secret
# a (secure) random string that you need to generate that proves that tokens have come from this server

# token_expiration_seconds
# defines the number of seconds that tokens remain valid for

# mailman_rest_api_netloc'
# i.e 'localhost:8001'
# defines where the Mailman API is - this is where proxied requests are sent to

# The Mailman REST API has basic authentication.
# You should set a good username and password in mailman.cfg on your Mailman server
# and put them in this seetings file too.
#
# 'basicauth_user' : 'restadmin',
# 'basicauth_pass' : 'restpass',

