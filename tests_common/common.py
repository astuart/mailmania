# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.


class CommonTests():

    urlvars_dict = {}

    # test cannot access without JWT
    def test_cannot_access_without_jwt(self, requires_testuser1):
        print('self.urlvars_dict')
        print(self.urlvars_dict)
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict)
        assert(r.status_code == 401)

    # test cannot access with expired JWT
    def test_cannot_access_with_invalid_jwt(self, requires_testuser1):
        expired_jwt_header = {'X-Auth-Token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0MjQ0OTgyNDQsInVzZXJfaWRlbnRpZmllciI6InRoZWJvc3NAbWFpbC5leGFtcGxlLm9yZyJ9.yRrHTQV-XCIBySt1gCBDkgNt6xCVQZtMlkuZYcVTZEM'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=expired_jwt_header)
        assert(r.status_code == 401)

    # test cannot access with corrupted JWT
    def test_cannot_access_with_corrupted_jwt(self, requires_testuser1):
        invalid_jwt_header = {'X-Auth-Token': 'invalid'}
        r = self.requestor_class.client_request(urlvars_dict=self.urlvars_dict, headers=invalid_jwt_header)
        assert(r.status_code == 401)
