# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import json
import time

from client_spec_mailman_rest_api import *
from client_spec_permissions.client_permissions import *
from settings import config

scheme = 'http'
netloc = 'localhost:7001'
pathprefix = '3.0'

# default destination is authenticating proxy (netloc localhost:7001)
# the caller of the helper method can sent direct to Mailman REST API byt changing the netloc to locahost:8001

class helpers():

    ################### QUEUES

    @staticmethod
    def get_list_of_files_from_queue(queue_name='None', headers=None, **kwargs):
        urlvars_dict = {}
        urlvars_dict['queue_name'] = queue_name
        return QueuesVARGET.client_request(urlvars_dict=urlvars_dict,  headers=headers, netloc=netloc, **kwargs)

    @staticmethod
    def inject_message_into_queue(queue_name='None', list_id=None, text=None, headers=None, **kwargs):
        data = {}
        data['list_id'] =  list_id
        data['text'] =  text
        urlvars_dict = {}
        urlvars_dict['queue_name'] = queue_name
        QueuesVARPOST.client_request(urlvars_dict=urlvars_dict, data=data,  headers=headers, netloc=netloc, **kwargs)
        # we need to wait a few seconds for the message to appear in the held queue
        # if we don't wait, then last code won't see the message
        time.sleep(3)
        # tests
        #QueuesVARGET.client_request(urlvars_dict=urlvars_dict, headers=headers, netloc=netloc, **kwargs)
        #ListsVARHeldGET.client_request(urlvars_dict={'list_id':'testlist1.mail.example.org'}, headers=headers, netloc=netloc, **kwargs)
        #QueuesVARGET.client_request(urlvars_dict=urlvars_dict, headers=headers, netloc=netloc, **kwargs)

    @staticmethod
    def delete_all_files_from_queue(queue_name='None', headers=None, **kwargs):
        # use with caution!
        urlvars_dict = {}
        urlvars_dict['queue_name'] = queue_name
        queue_filelist = helpers.get_list_of_files_from_queue(queue_name=queue_name, headers=headers)
        queue_filelist = queue_filelist.json()
        queue_filelist = queue_filelist.get('files', [])
        for item in queue_filelist:
            urlvars_dict['file_name'] = item
            QueuesVARVARDELETE.client_request(urlvars_dict=urlvars_dict,  headers=headers, netloc=netloc, **kwargs)


    ################### MISC

    @staticmethod
    def get_operations_from_swagger_spec(swagger_spec):
        operations = []

        for k in swagger_spec['paths'].keys():
            for http_method in swagger_spec['paths'][k].keys():
                operationId = swagger_spec['paths'][k][http_method]['operationId']
                operations.append(operationId)
        return operations

    @staticmethod
    def load_swagger_spec(swagger_spec_location):
        with open(swagger_spec_location) as f:
            swagger_spec = f.read()
        try:
            swagger_spec = json.loads(swagger_spec)
            return swagger_spec
        except:
            raise Exception("Unable to load the Swagger spec JSON document.")

    @staticmethod
    def get_testuser_username_password():
        test_mode_serverowner = config.get('test_mode_serverowner', None)
        return test_mode_serverowner['email'], test_mode_serverowner['password']

    ################### ADDRESSES

    @staticmethod
    def get_all_addresses(urlvars_dict=None, headers=None, netloc=None, **kwargs):
        return AddressesGET.client_request(urlvars_dict=urlvars_dict, netloc=netloc, headers=headers)

    @staticmethod
    def get_memberships_for_address(urlvars_dict=None, headers=None, netloc=None, **kwargs):
        # Gets list memberships for an email address.
        return AddressesVARMembershipsGET.client_request(urlvars_dict=urlvars_dict, netloc=netloc, headers=headers)

    ################### USERS

    @staticmethod
    def create_user(email=None, display_name=None, password=None,
                    is_server_owner=None, headers=None, **kwargs):
        data = {'email': email,
                'display_name': display_name,
                'password': password}
        UsersPOST.client_request(data=data, headers=headers)

    @staticmethod
    def delete_user(user_identifier=None, headers=None, **kwargs):
        UsersVARDELETE.client_request(urlvars_dict={'user_identifier':user_identifier}, headers=headers)

    @staticmethod
    def get_all_users(urlvars_dict=None, headers=None, **kwargs):
        return UsersGET.client_request(urlvars_dict=urlvars_dict, headers=headers)

    @staticmethod
    def get_user_id(email=None, headers=None, **kwargs):
        _user_data = UsersVARGET.client_request(urlvars_dict={'user_identifier':email}, headers=headers)
        return _user_data.json().get('user_id', None)

    ################### PERMISSIONS


    @staticmethod
    def set_permission(resource_type=None, resource_id=None, role=None,
                user_identifier=None, headers=None, **kwargs):
        urlvars_dict = {}
        urlvars_dict['resource_type'] = resource_type
        urlvars_dict['resource_id'] = resource_id
        urlvars_dict['role'] = role
        urlvars_dict['user_identifier'] = user_identifier
        permissionsVARVARVARVARPOST.client_request(urlvars_dict=urlvars_dict, headers=headers)

    @staticmethod
    def query_permission(resource_type=None, resource_id=None, role=None,
                user_identifier=None, headers=None, **kwargs):
        urlvars_dict = {}
        urlvars_dict['resource_type'] = resource_type
        urlvars_dict['resource_id'] = resource_id
        urlvars_dict['role'] = role
        urlvars_dict['user_identifier'] = user_identifier
        permissionsVARVARVARVARGET.client_request(urlvars_dict=urlvars_dict, headers=headers)

    @staticmethod
    def delete_permission(resource_type=None, resource_id=None, role=None,
                user_identifier=None, headers=None, **kwargs):
        urlvars_dict = {}
        urlvars_dict['resource_type'] = resource_type
        urlvars_dict['resource_id'] = resource_id
        urlvars_dict['role'] = role
        urlvars_dict['user_identifier'] = user_identifier
        permissionsVARVARVARVARDELETE.client_request(urlvars_dict=urlvars_dict, headers=headers)

    ################### LISTS

    @staticmethod
    def create_list(fqdn_listname=None, style_name='', headers=None, **kwargs):
        data = {'fqdn_listname': fqdn_listname, 'style_name': style_name}
        ListsPOST.client_request(data=data, headers=headers)

    @staticmethod
    def create_list_testlist1ATmailDOTexampleDOTorg(headers=None, **kwargs):
        helpers.create_list(fqdn_listname= 'testlist1@mail.example.org', headers=headers)

    @staticmethod
    def delete_list(list_id=None, headers=None, **kwargs):
        ListsVARDELETE.client_request(urlvars_dict={'list_id':list_id}, headers=headers)

    @staticmethod
    def create_list_testlist1ATmailDOTexampleDOTorg(headers=None, **kwargs):
        ListsVARDELETE.client_request(urlvars_dict={'list_id':'testlist1.mail.example.org'}, headers=headers)

    @staticmethod
    def set_list_to_default_hold_messages(list_id=None, headers=None, **kwargs):
        data = {}
        data['default_member_action'] =  'hold'
        data['default_nonmember_action'] =  'hold'
        urlvars_dict = {}
        urlvars_dict['list_id'] = list_id
        ListsVARConfigPATCH.client_request(urlvars_dict=urlvars_dict, data=data,  headers=headers, netloc=netloc, **kwargs)

    @staticmethod
    def set_list_to_enable_mail_archive(list_id=None, headers=None, **kwargs):
        """
        data = {}
        data['archive_policy'] =  'public'
        """
        urlvars_dict = {}
        urlvars_dict['list_id'] = list_id
        #ListsVARConfigPATCH.client_request(urlvars_dict=urlvars_dict, data=data,  headers=headers, netloc=netloc, **kwargs)
        data = {}
        data['mail-archive'] =  True
        ListsVARArchiversPATCH.client_request(urlvars_dict=urlvars_dict, data=data,  headers=headers, netloc=netloc, **kwargs)

    @staticmethod
    def get_list_config(list_id=None, headers=None, **kwargs):
        urlvars_dict = {}
        urlvars_dict['list_id'] = list_id
        return ListsVARConfigGET.client_request(urlvars_dict=urlvars_dict, headers=headers, netloc=netloc, **kwargs)

    @staticmethod
    def get_list_held_messages(list_id=None, headers=None, **kwargs):
        urlvars_dict = {}
        urlvars_dict['list_id'] = list_id
        result =  ListsVARHeldGET.client_request(urlvars_dict=urlvars_dict, headers=headers, netloc=netloc, **kwargs)
        print(result)
        print(result.json())
        return result
        #ListsVARHeldGET.client_request(urlvars_dict={'list_id':'testlist1.mail.example.org'}, headers=headers, netloc=netloc, **kwargs)

    @staticmethod
    def get_list_held_messages_testlist1ATmailDOTexampleDOTorg(headers=None, **kwargs):
        return helpers.get_list_held_messages(list_id ='testlist1.mail.example.org', headers=headers)


    @staticmethod
    def discard_all_held_messages_from_list(list_id='None', headers=None, **kwargs):
        held_messages = helpers.get_list_held_messages(list_id=list_id, headers=headers)
        held_messages = held_messages.json()
        held_messages = held_messages.get('entries', [])
        for item in held_messages:
            request_id = item['request_id']
            urlvars_dict = {}
            urlvars_dict['list_id'] = list_id
            urlvars_dict['request_id'] = str(request_id)
            ListsVARHeldVARPOST.client_request(urlvars_dict=urlvars_dict,
                                               data={'action': 'discard'},
                                               headers=headers, netloc=netloc, **kwargs)

    @staticmethod
    def discard_all_held_messages_from_testlist1ATmailDOTexampleDOTorg(headers=None, **kwargs):
        helpers.discard_all_held_messages_from_list(list_id='testlist1.mail.example.org', headers=headers)



    ################### DOMAINS

    @staticmethod
    def set_domain_owner(mail_host=None, email=None, headers=None, **kwargs):
        data = {'owner': email}
        urlvars_dict = {'mail_host': mail_host}
        DomainsVAROwnersPOST.client_request(urlvars_dict=urlvars_dict, data=data, headers=headers)

    @staticmethod
    def create_domain(mail_host=None, description='', contact_address='', headers=None, **kwargs):
        data = {'mail_host': mail_host}
        DomainsPOST.client_request(data=data, headers=headers)

    @staticmethod
    def create_domain_exampleDOTorg(headers=None, **kwargs):
        helpers.create_domain(description='test suite setup', contact_address='domainowner@example.org',
                              mail_host='example.org', headers=headers)

    @staticmethod
    def get_all_domains(headers=None, **kwargs):
        DomainsGET.client_request(headers=headers)

    @staticmethod
    def delete_domain(mail_host=None, headers=None, **kwargs):
        DomainsVARDELETE.client_request(urlvars_dict={'mail_host': mail_host}, headers=headers)

    @staticmethod
    def delete_domain_exampleDOTorg(headers=None, **kwargs):
        helpers.delete_domain(mail_host='example.org', headers=headers)


    ################### LOGIN

    @staticmethod
    def login_user_to_proxy(user_id_or_emailaddress, password=None):
        urlvars_dict = {'user_identifier': user_id_or_emailaddress}
        data = {'cleartext_password': password}
        response = UsersVARLoginPOST.client_request(urlvars_dict=urlvars_dict, data=data)
        _jwt = response.headers['X-Auth-Token']
        jwt_headers = {'X-Auth-Token': _jwt}
        return jwt_headers, response

    @staticmethod
    def login_as_testuser1ATmailDOTexampleDOTorg():
        email, password = 'testuser1@mail.example.org', 'password'
        headers, _response = helpers.login_user_to_proxy(email, password=password)
        return headers

    @staticmethod
    def login_as_testuser2ATmailDOTexampleDOTorg():
        email, password = 'testuser2@mail.example.org', 'password'
        headers, _response = helpers.login_user_to_proxy(email, password=password)
        return headers

    @staticmethod
    def login_as_test_mode_serverowner():
        email, password = helpers.get_testuser_username_password()
        headers, _response = helpers.login_user_to_proxy(email, password=password)
        return headers

    ################### MEMBERS


    @staticmethod
    def subscribe_to_list(list_id=None, subscriber=None, role='', delivery_mode='', display_name='',
                           headers=None, **kwargs):
        # role = member, owner, moderator, nonmember

        data = {'list_id': list_id,
                'subscriber': subscriber,
                'role': role,
                'pre_verified': True,
                'pre_confirmed': True,
                'pre_approved': True,
                }

        MembersPOST.client_request(data=data, headers=headers)
        """
        this stuff shows what's going on when people are subscribed
        time.sleep(8)
        urlvars_dict={'list_id': 'testlist1.mail.example.org'}
        r = ListsVARRequestsGET.client_request(urlvars_dict=urlvars_dict, data=data, headers=headers)
        print('data')
        print(data)
        print('subscription requests')
        print(r.json())
        r = ListsVARConfigGET.client_request(urlvars_dict=urlvars_dict, headers=headers)
        print('list config')
        print(r.json())
        """

    @staticmethod
    def subscribe_as_list_moderator(list_id=None, subscriber=None, role='', delivery_mode='', display_name='',
                           headers=None, **kwargs):
        helpers.subscribe_to_list(list_id=list_id, subscriber=subscriber, role='moderator',
                                  delivery_mode=delivery_mode, display_name=display_name, headers=headers)

    @staticmethod
    def subscribe_as_list_member(list_id=None, subscriber=None, role='', delivery_mode='', display_name='',
                           headers=None, **kwargs):
        helpers.subscribe_to_list(list_id=list_id, subscriber=subscriber, role='member',
                                  delivery_mode=delivery_mode, display_name=display_name, headers=headers)

    @staticmethod
    def subscribe_as_list_nonmember(list_id=None, subscriber=None, role='', delivery_mode='', display_name='',
                           headers=None, **kwargs):
        helpers.subscribe_to_list(list_id=list_id, subscriber=subscriber, role='nonmember',
                                  delivery_mode=delivery_mode, display_name=display_name, headers=headers)

    @staticmethod
    def subscribe_as_list_owner(list_id=None, subscriber=None, role='', delivery_mode='', display_name='',
                           headers=None, **kwargs):
        helpers.subscribe_to_list(list_id=list_id, subscriber=subscriber, role='owner',
                                  delivery_mode=delivery_mode, display_name=display_name, headers=headers)

    @staticmethod
    def subscribe_as_list_member(list_id=None, subscriber=None, role='', delivery_mode='', display_name='',
                           headers=None, **kwargs):
        helpers.subscribe_to_list(list_id=list_id, subscriber=subscriber, role='member',
                                  delivery_mode=delivery_mode, display_name=display_name, headers=headers)

    @staticmethod
    def subscribe_as_list_nonmember(list_id=None, subscriber=None, role='', delivery_mode='', display_name='',
                           headers=None, **kwargs):
        helpers.subscribe_to_list(list_id=list_id, subscriber=subscriber, role='nonmember',
                                  delivery_mode=delivery_mode, display_name=display_name, headers=headers)

    @staticmethod
    def unsubscribe_from_list(member_id=None, headers=None, **kwargs):
        # member_id is not the same as user_id
        MembersVARDELETE.client_request(urlvars_dict={'member_id': member_id}, headers=headers, **kwargs)

    @staticmethod
    def get_member_id(email=None, list_id=None, headers=None, **kwargs):
        _memberships = AddressesVARMembershipsGET.client_request(urlvars_dict={'email': email}, headers=headers).json()
        for item in _memberships['entries']:
            if (item['email'] == email) and (item['list_id'] == list_id):
                member_id = str(item['member_id'])
        return member_id
