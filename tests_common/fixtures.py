# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import pytest
import uuid

from tests_common.helpers import helpers

# TIP use -s on pytest to see the output of stdout (i.e. print)

# all operations are executed through the authenticating proxy server using the default serverowner user

@pytest.fixture()
def requires_message_injected_into_bad_queue(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    list_id =  'testlist1.mail.example.org'
    text =  ("""\
From: testuser1@mail.example.org
To: testlist1@mail.example.org
Subject: Something
Message-ID: <{}>

Something else.
""").format(str(uuid.uuid1().int))
    helpers.inject_message_into_queue(queue_name='bad', list_id=list_id, text=text, headers=jwt_headers)
    return requires_message_injected_into_bad_queue  # provide the fixture value


@pytest.fixture()
def requires_message_injected_into_in_queue(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    list_id =  'testlist1.mail.example.org'
    text =  ("""\
From: nonmember@mail.example.org
To: testlist1@mail.example.org
Subject: Something
Message-ID: <{}>

Something else.
""").format(str(uuid.uuid1().int))
    helpers.inject_message_into_queue(queue_name='in', list_id=list_id, text=text, headers=jwt_headers)
    def fin():
        helpers.discard_all_held_messages_from_testlist1ATmailDOTexampleDOTorg(headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_message_injected_into_in_queue  # provide the fixture value

@pytest.fixture()
def requires_testuser1(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    def fin():
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testuser1  # provide the fixture value

@pytest.fixture()
def requires_testuser2(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_user(email='testuser2@mail.example.org', display_name='test user 2', password='password', headers=jwt_headers)
    def fin():
        helpers.delete_user(user_identifier='testuser2@mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testuser2  # provide the fixture value

@pytest.fixture()
def requires_testlist1_without_domain(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_list(fqdn_listname= 'testlist1@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testlist1_without_domain  # provide the fixture value

@pytest.fixture()
def requires_testlist1_with_subscriber_testuser1(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.create_list(fqdn_listname= 'testlist1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.subscribe_as_list_member(list_id='testlist1.mail.example.org', subscriber='testuser1@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testlist1_with_subscriber_testuser1  # provide the fixture value

@pytest.fixture()
def set_testlist1_to_default_hold_messages(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.set_list_to_default_hold_messages(list_id='testlist1.mail.example.org', headers=jwt_headers)
    return set_testlist1_to_default_hold_messages  # provide the fixture value


@pytest.fixture()
def set_testlist1_to_enable_mail_archive(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.set_list_to_enable_mail_archive(list_id='testlist1.mail.example.org', headers=jwt_headers)
    return set_testlist1_to_enable_mail_archive  # provide the fixture value


@pytest.fixture()
def setup_remove_domain_mailDOTexampleDOTorg(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    return setup_remove_domain_mailDOTexampleDOTorg  # provide the fixture value


@pytest.fixture()
def setup_remove_list_testlist1DOTmailDOTexampleDOTorg(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
    return setup_remove_list_testlist1DOTmailDOTexampleDOTorg  # provide the fixture value

@pytest.fixture()
def requires_testlist1_with_subscriber_testuser1_moderator_role(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.create_list(fqdn_listname= 'testlist1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.subscribe_as_list_moderator(list_id='testlist1.mail.example.org', subscriber='testuser1@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testlist1_with_subscriber_testuser1_moderator_role  # provide the fixture value

@pytest.fixture()
def requires_testlist1_with_subscriber_testuser1_member_role(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.create_list(fqdn_listname= 'testlist1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.subscribe_as_list_member(list_id='testlist1.mail.example.org', subscriber='testuser1@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testlist1_with_subscriber_testuser1_member_role  # provide the fixture value


@pytest.fixture()
def requires_testlist1_with_user_testuser1_not_subscribed(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.create_list(fqdn_listname= 'testlist1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testlist1_with_user_testuser1_not_subscribed  # provide the fixture value


@pytest.fixture()
def requires_testlist1_with_subscriber_testuser1_nonmember_role(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.create_list(fqdn_listname= 'testlist1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.subscribe_as_list_nonmember(list_id='testlist1.mail.example.org', subscriber='testuser1@mail.example.org', headers=jwt_headers)
    def fin():
        # does the nonmember automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testlist1_with_subscriber_testuser1_nonmember_role  # provide the fixture value

@pytest.fixture()
def requires_testlist1_with_subscriber_testuser1_owner_role(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.create_list(fqdn_listname= 'testlist1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.subscribe_as_list_owner(list_id='testlist1.mail.example.org', subscriber='testuser1@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testlist1_with_subscriber_testuser1_owner_role  # provide the fixture value

@pytest.fixture()
def requires_domain_mailDOTexampleDOTorg(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    def fin():
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_domain_mailDOTexampleDOTorg  # provide the fixture value

@pytest.fixture()
def requires_testlist1_with_subscriber_testuser1_with_role_domainowner(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.create_list(fqdn_listname='testlist1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.subscribe_as_list_member(list_id='testlist1.mail.example.org', subscriber='testuser1@mail.example.org', headers=jwt_headers)
    helpers.set_domain_owner(mail_host='mail.example.org', email='testuser1@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testlist1_with_subscriber_testuser1_with_role_domainowner  # provide the fixture value

@pytest.fixture()
def requires_testuser2_with_role_domainowner(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.delete_user(user_identifier='testuser2@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser2@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.set_domain_owner(mail_host='mail.example.org', email='testuser2@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser2@mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testuser2_with_role_domainowner  # provide the fixture value

@pytest.fixture()
def requires_testuser2_with_role_listowner(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.delete_user(user_identifier='testuser2@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser2@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.set_domain_owner(mail_host='mail.example.org', email='testuser2@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser2@mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testuser2_with_role_listowner  # provide the fixture value

@pytest.fixture()
def requires_testuser1_with_role_domainowner(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.set_domain_owner(mail_host='mail.example.org', email='testuser1@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testuser1_with_role_domainowner  # provide the fixture value

@pytest.fixture()
def requires_domain_mailDOTexampleDOTorg_with_user_testuser1_with_role_domainowner(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.set_domain_owner(mail_host='mail.example.org', email='testuser1@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_domain_mailDOTexampleDOTorg_with_user_testuser1_with_role_domainowner  # provide the fixture value

@pytest.fixture()
def requires_testlist1(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.create_list(fqdn_listname='testlist1@mail.example.org', headers=jwt_headers)
    def fin():
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testlist1  # provide the fixture value

@pytest.fixture()
def requires_testuser1_as_domainowner_formailexampleorg(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.set_domain_owner(mail_host='mail.example.org', email='testuser1@mail.example.org', headers=jwt_headers)
    helpers.create_list_testlist1ATmailDOTexampleDOTorg(headers=jwt_headers)
    helpers.subscribe_as_list_member(list_id='testlist1.mail.example.org', subscriber='testuser1@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testuser1_as_domainowner_formailexampleorg  # provide the fixture value




@pytest.fixture()
def requires_testlist1_with_subscriber_testuser1_and_member_testuser2(request):
    jwt_headers = helpers.login_as_test_mode_serverowner()
    helpers.create_domain(mail_host='mail.example.org', headers=jwt_headers)
    helpers.create_list_testlist1ATmailDOTexampleDOTorg(headers=jwt_headers)
    helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser1@mail.example.org', display_name='test user 1', password='password', headers=jwt_headers)
    helpers.delete_user(user_identifier='testuser2@mail.example.org', headers=jwt_headers)
    helpers.create_user(email='testuser2@mail.example.org', display_name='test user 2', password='password', headers=jwt_headers)
    helpers.subscribe_as_list_member(list_id='testlist1.mail.example.org', subscriber='testuser1@mail.example.org', headers=jwt_headers)
    helpers.subscribe_as_list_member(list_id='testlist1.mail.example.org', subscriber='testuser2@mail.example.org', headers=jwt_headers)
    def fin():
        # does the member automatically get deleted when the list is deleted?  Hope so.
        helpers.delete_user(user_identifier='testuser2@mail.example.org', headers=jwt_headers)
        helpers.delete_user(user_identifier='testuser1@mail.example.org', headers=jwt_headers)
        helpers.delete_list(list_id='testlist1.mail.example.org', headers=jwt_headers)
        helpers.delete_domain(mail_host='mail.example.org', headers=jwt_headers)
    request.addfinalizer(fin)
    return requires_testlist1_with_subscriber_testuser1_and_member_testuser2  # provide the fixture value


