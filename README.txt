# STATUS: 12 Aug 2015:
Much of the code is now working as expected.
Tests are running to completion.

This is a full working authenticating proxy server for Mailman 3.

The parts that are not fully implemented are additional, on top of
Mailman 3 REST API access, and do not prevent full usage of this software.

The parts not fully implemented are the datastore - intended to store
additional user data, and the archiver, for archiving and searching messages.
You can safely ignore this stuff, the fact that it is not fully
implemented won't get in your way.

#How to install from a clean Ubuntu installation.

# update the OS
sudo apt-get update
sudo apt-get upgrade

# you only need to do this if you are running on a machine that does not
# have a swapfile configured (i.e. an Amazon EC2 Ubuntu AMI)
# for amazon EC2 instances, set up a 1 gig swapfile
sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
sudo /sbin/mkswap /var/swap.1
sudo /sbin/swapon /var/swap.1
#To enable it by default after reboot, add this line to /etc/fstab:
#/var/swap.1 swap swap defaults 0 0
sudo vi /etc/fstab
sudo reboot

# set up a Python  venv
# for some reason Ubuntu 14's pyvenv is broken so we get the source.
sudo apt-get install git
git clone https://github.com/pypa/virtualenv.git
python virtualenv/virtualenv.py -p /usr/bin/python3 venv3.4
source venv3.4/bin/activate

# get Mailman 3
wget https://pypi.python.org/packages/source/m/mailman/mailman-3.0.0.tar.gz
tar -zxf mailman-3.0.0.tar.gz
cd mailman-3.0.0/
python setup.py install

# set the hostname of this machine
sudo hostnamectl set-hostname mail.example.org

# install postfix
sudo apt-get install postfix
# Postfix will ask two questions, here are the answers for a test setup:
General type of mail configuration: Internet Site
System mail name: example.com

# add the following to ~/var/etc/mailman.cfg
[mta]
incoming: mailman.mta.postfix.LMTP
outgoing: mailman.mta.deliver.deliver
lmtp_host: 127.0.0.1
lmtp_port: 8024
smtp_host: 127.0.0.1
smtp_port: 25


# install dependencies
sudo apt-get install python3.4-dev
pip install gunicorn
pip install requests
pip install sqlalchemy
pip install falcon
pip install PyJWT
pip install pytest

# get the authenticating proxy server
git clone https://github.com/crowdwave/mailmania.git

# run Mailman 3
cd ~
mailman start

# run the authenticating proxy
# you need to wait until the Mailman rest server is fully started - might take 30 seconds or so.
cd mailmania
gunicorn --bind 127.0.0.1:7001 startserver

# in a separate window, test it
py.test -v

# POST INSTALLATION CONFIGURATION TASKS
settings.py contains the configuration.
There are a number of settings you must update.

* set token secret
'token_secret' : 'replace_this_with_a_32_character_random_string'

* set basic auth username and password
'basicauth_user' : 'restadmin',
'basicauth_pass' : 'restpass',

* set secure password for test user
'test_mode_serverowner': { 'email': 'testserverowner@mail.example.org',
                    'display_name': 'Test Serverowner',
                    'password': 'replace_this_with_a_secure_password',
                    'is_server_owner': 'yes'},

* optionally create a server_owner
'prod_mode_users': [{ 'email': 'serverowner@example.com',
                    'display_name': 'Mailman Serverowner',
                    'password': 'replace_this_with_a_secure_password',
                    'is_server_owner': 'yes'}]

* set the Mailman port
    'mailman_rest_api_netloc' : 'localhost:8001',


* set the port for the proxy server to run on
    'mailman_auth_proxy_netloc' : 'localhost:7001',

* if you want to run the tests, you'll need to set run_in_test_mode
'run_in_test_mode': True,


#Configure nginx


