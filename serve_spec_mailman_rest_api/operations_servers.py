# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import logging

from requests import Request, Session
import falcon
from falcon.http_error import HTTPError
import falcon.status_codes as status
from requests.auth import _basic_auth_str

from settings import config
from serve_security.authorization_handlers import  get_requesting_user_id_from_jwt
from serve_security.authorize import authorize

logger = logging.getLogger('root.' + __name__)

def apidocs_get(**request_handler_args):
    # this a request handler
    req = request_handler_args['req']
    resp = request_handler_args['resp']
    if req.method != 'GET':
        raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
    try:
        with open('serve_spec_mailman_rest_api/api-docs.json') as f:
            resp.status = status.HTTP_200  # This is the default status
            resp.body = f.read()
    except:
            resp.status = status.HTTP_400


def proxy_to_mailman(**request_handler_args):
    # this a request handler
    req = request_handler_args['req']
    resp = request_handler_args['resp']
    auth_string = _basic_auth_str(config['basicauth_user'], config['basicauth_pass'])
    headers = { "Authorization" : auth_string}
    logger.debug("Headers forwarded to proxy: {}".format(headers))
    url_for_mailman_api = "{}{}{}".format('http://', config['mailman_rest_api_netloc'], req.relative_uri)
    if req.query_string:
        url_for_mailman_api = "{}?{}".format(url_for_mailman_api, req.query_string)
    logger.debug(req.env['wsgi.input'].read())
    if req.method in ['POST', 'PATCH', 'PUT']:
        # proxied content-type must be the same as inbound if POST, PATCH, PUT
        headers['content-type'] = req.get_header('content-type')
        prepared_req = Request(req.method, url_for_mailman_api, headers=headers, data=req.params)
    else:
        # Falcon has a bug where is hangs if GET & content-type is application/x-www-form-urlencoded
        headers.pop("content-type", None)
        prepared_req = Request(req.method, url_for_mailman_api, headers=headers)
    prepped = prepared_req.prepare()
    logger.debug("Proxying request to Mailman API: {} {}".format(req.method, url_for_mailman_api))
    requests_session = Session()
    mailman_api_response = requests_session.send(prepped)
    logger.debug("Got %s response from %s", mailman_api_response.status_code, mailman_api_response.url)
    logger.debug("Headers from proxy %s", mailman_api_response.headers)
    headers = dict(mailman_api_response.headers)

    # Falcon needs a complete status string, not just a code
    resp.status = "{} {}".format(mailman_api_response.status_code, mailman_api_response.reason)
    logger.debug("Mailman REST API response content is {}".format(mailman_api_response.content))
    resp.body = mailman_api_response.content
    resp.content_type = mailman_api_response.headers.get('content-type')


class validate_server_owner_field():

    # A PUT or a PATCH to a user record is able to set the is_server_owner field
    # We need to make sure the user doing this is already a server_owner

    # this is untested because at the moment UsersVARPATCH and UsersVARPUT
    # are configured to only allow access by the server_owner. It's useful to have
    # this class here anyway in case a day comes when we need to allow people
    # other than server_owners to do UsersVARPATCH and UsersVARPUT

    def __init__(self, **request_handler_args):
        # this a request handler
        self.request_handler_args = request_handler_args
        self.uri_fields = request_handler_args['uri_fields']
        self.req = request_handler_args['req']
        if self.req.method not in ['PUT', 'PATCH']:
            raise HTTPError(status.HTTP_405, title=status.HTTP_405, description=status.HTTP_405)
        self.handle_request()

    def handle_request(self):
        is_server_owner = self.req.params.get('is_server_owner', '')
        if is_server_owner.lower() in ['true', 'yes', 'on', 'enable', 'enabled', '1']:
            logger.debug("Request attempts to set is_server_owner, validating....")
            # identify the user
            requesting_user_id = get_requesting_user_id_from_jwt(**self.request_handler_args)
            # is the user a server_owner?
            if not authorize().is_user_serverowner(user_identifier=requesting_user_id):
                # return a 401 Unauthorized if response status code is anything but 204
                raise falcon.HTTPUnauthorized('You must be server_owner to do this.',
                                              'You must be server_owner to do this.')


