# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import logging

from requests.auth import _basic_auth_str

from settings import config
from client_spec_mailman_rest_api import *

logger = logging.getLogger('root.' + __name__)
__all__ = ['mm_rest_api_users', 'mm_rest_api_domains', 'mm_rest_api_lists',
           'mm_rest_api_addresses', 'mm_rest_api_members']

def get_result_data(response):
    if response.json:
        return response.json()
    else:
        return None

auth_string = _basic_auth_str(config['basicauth_user'], config['basicauth_pass'])
headers = { 'Authorization' : auth_string}

class mm_rest_api_users():

    def get_user(self, user_identifier=None):
        # Gets a Mailman user. Returns user data, else None
        if not user_identifier:
            return
        logger.debug('Getting Mailman user. user_identifier: {}'.format(user_identifier))
        response = UsersVARGET.client_request(  urlvars_dict={'user_identifier': user_identifier},
                                                netloc=config['mailman_rest_api_netloc'],
                                                headers=headers)
        if response.status_code == 200:
            return get_result_data(response)

    def does_user_exist(self, user_identifier=None):
        if not user_identifier:
            return
        return self.get_user(user_identifier=user_identifier)

    def compare_resource_id_to_user_identifier(self, resource_id=None, user_identifier=None):
        # this is most useful for authentication, to ensure that the requesting user
        # and the requested user are the same
        user_identifier_a = self.convert_user_identifier_to_valid_user_id(user_identifier=resource_id)
        user_identifier_b = self.convert_user_identifier_to_valid_user_id(user_identifier=user_identifier)
        logger.debug('compare_resource_id_to_user_identifier: {} to {}'.format(user_identifier_a, user_identifier_b))
        if not resource_id:
            return
        if not user_identifier:
            return
        logger.debug('compare_resource_id_to_user_identifier: result is {}'.format(user_identifier_a == user_identifier_b))
        return user_identifier_a == user_identifier_b

    def create_user(self, user_data):
        response = UsersPOST.client_request(netloc=config['mailman_rest_api_netloc'],
                                            data=user_data,
                                            headers=headers)
        logger.debug('Created user, {} status code {}'.format(user_data['email'], response.status_code))

    def update_user(self, user_data):
        email_address = user_data.pop('email')
        # for some reason the password field name is different when updating/PATCHing
        user_data['cleartext_password'] = user_data.pop('password')
        response = UsersVARPATCH.client_request(urlvars_dict={'user_identifier': email_address},
                                                netloc=config['mailman_rest_api_netloc'],
                                                data=user_data,
                                                headers=headers)
        logger.debug('Updated user, {} status code {}'.format(email_address, response.status_code))

    def delete_user(self, user_data):
        response = UsersVARDELETE.client_request(urlvars_dict={'user_identifier': user_data['email']},
                                                netloc=config['mailman_rest_api_netloc'],
                                                headers=headers)
        logger.debug('Deleted user, {} status code {}'.format(user_data['email'], response.status_code))

    def create_or_update_user(self, user_data):
            user = mm_rest_api_users().get_user(user_identifier=user_data['email'])
            if not user:
                # user does not exists so create it
                mm_rest_api_users().create_user(user_data)
            else:
                # user exists so patch it
                mm_rest_api_users().update_user(user_data)

    def convert_user_identifier_to_valid_user_id(self, user_identifier=None):
        """
        A Mailman user_identifier can be an email address or a user_id

        If email address, this returns the corresponding Mailman user_id
        If user_identifier not found in Mailman returns None
        """
        if not user_identifier:
            return
        user = self.get_user(user_identifier=user_identifier)
        if user:
            user_id = user.get('user_id', None)
            return str(user_id)

class mm_rest_api_domains():

    def get_domains(self):
        # Gets all domains. Returns user data, else None
        logger.debug('Getting Mailman domains.')
        response = DomainsGET.client_request(netloc=config['mailman_rest_api_netloc'],
                                                 headers=headers)
        if response.status_code == 200:
            return get_result_data(response)

    def does_domain_exist(self, resource_id=None):
        if not resource_id:
            return
        logger.debug('Checking if domain {} exists'.format(resource_id))
        all_domains = self.get_domains()
        all_domains = all_domains.get('entries', [])
        for domain in all_domains:
            if domain['mail_host'] == resource_id:
                logger.debug('Yes, domain {} does exist'.format(domain['mail_host']))
                return True

    def get_domain_owners(self, resource_id=None):
        # Gets owners of a Mailman domain. Returns data, else None
        if not resource_id:
            return
        logger.debug('Getting owners for domain resource_id: {}'.format(resource_id))
        response = DomainsVAROwnersGET.client_request(urlvars_dict={'mail_host': resource_id},
                        netloc=config['mailman_rest_api_netloc'], headers=headers)
        print(get_result_data(response))
        if response.status_code == 200:
            return get_result_data(response)

    def delete_domain(self, urlvars_dict=None):
        response = DomainsVARDELETE.client_request(urlvars_dict=urlvars_dict,
                                                netloc=config['mailman_rest_api_netloc'],
                                                headers=headers)
        logger.debug('Deleted domain, {} status code {}'.format(urlvars_dict['mail_host'], response.status_code))

class mm_rest_api_lists():

    def delete_list(self, urlvars_dict=None):
        response = ListsVARDELETE.client_request(urlvars_dict=urlvars_dict,
                                                netloc=config['mailman_rest_api_netloc'],
                                                headers=headers)
        logger.debug('Deleted list, {} status code {}'.format(urlvars_dict['list_id'], response.status_code))

    def get_list(self, resource_id=None):
        # Gets domain of a Mailman list. Returns data, else None
        if not resource_id:
            return
        logger.debug('Getting list resource_id: {}'.format(resource_id))
        response = ListsVARGET.client_request(urlvars_dict={'list_id': resource_id},
                        netloc=config['mailman_rest_api_netloc'], headers=headers)
        logger.debug('RESPONSE {}'.format(response.status_code))
        logger.debug('RESPONSE {}'.format(response.text))
        if response.status_code == 200:
            return get_result_data(response)

    def get_list_domain(self, resource_id=None):
        # Gets domain of a Mailman list. Returns data, else None
        if not resource_id:
            return
        logger.debug('Getting domain for list resource_id: {}'.format(resource_id))
        listdata = self.get_list(resource_id=resource_id)

        if listdata:
            return listdata.get('mail_host', None)

    def get_list_owners(self, resource_id=None):
        # Gets owners of a Mailman list. Returns data, else None
        if not resource_id:
            return
        logger.debug('Getting owners for list resource_id: {}'.format(resource_id))
        response = ListsVARRosterOwnerGET.client_request(urlvars_dict={'list_id': resource_id},
                        netloc=config['mailman_rest_api_netloc'], headers=headers)
        if response.status_code == 200:
            return get_result_data(response)

    def does_list_exist(self, resource_id=None):
        if not resource_id:
            return
        response = ListsVARGET.client_request(urlvars_dict={'list_id': resource_id},
                        netloc=config['mailman_rest_api_netloc'], headers=headers)
        if response.status_code == 200:
            list_found = response.json().get('list_id', None)
            if list_found == resource_id:
                return True

class mm_rest_api_addresses():

    def get_memberships_for_address(self, email=None):
        # Gets list memberships for an email address.
        if not email:
            return
        logger.debug('Getting memberships for address: {}'.format(email))
        response = AddressesVARMembershipsGET.client_request(urlvars_dict={'email': email},
                        netloc=config['mailman_rest_api_netloc'], headers=headers)
        logger.debug('Memberships data found is: {}'.format(get_result_data(response)))
        if response.status_code == 200:
            return get_result_data(response)

class mm_rest_api_members():

    def get_member(self, member_id=None):
        logger.debug('MembersVARGET: {}'.format(member_id))
        if not member_id:
            return
        logger.debug('MembersVARGET: {}'.format(member_id))
        response = MembersVARGET.client_request(urlvars_dict={'member_id': member_id}, netloc=config['mailman_rest_api_netloc'],
                                                 headers=headers)
        logger.debug('MembersVARGET: {}'.format(response.text))
        if response.status_code == 200:
            return get_result_data(response)

