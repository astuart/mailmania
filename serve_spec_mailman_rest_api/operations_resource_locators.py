# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.


# When a request comes in, first we need to *authenticate* the user that is requesting (i.e. prove they are who they say)
# THEN we need to *authorize* that user to access the resource that is the target of the request
# SO... we need a way to inspect the request, and identify the resource that is being requested
# This is how to identify the resource being requested, which varies from operation to operation.
# SO this specifies which request field value defines the resource being requested, which in turn allows authorization
#urifields['email']
# the value is a lambda function that returns the correct value from the request_handler_args, which is passed
# in as a parameter at the location that is calling the lambda function.
operation_resource_locators = {
'AddressesGET':                 None,
'AddressesPOST':                None,
'AddressesVARGET':              (lambda request_handler_args: (request_handler_args['uri_fields'].get('email', None), 'email')),
'AddressesVARDELETE':           (lambda request_handler_args: (request_handler_args['uri_fields'].get('email', None), 'email')),
'AddressesVARMembershipsGET':   (lambda request_handler_args: (request_handler_args['uri_fields'].get('email', None), 'email')),
'AddressesVARPreferencesGET':   (lambda request_handler_args: (request_handler_args['uri_fields'].get('email', None), 'email')),
'AddressesVARUnverifyPOST':     (lambda request_handler_args: (request_handler_args['uri_fields'].get('email', None), 'email')),
'AddressesVARUserDELETE':       (lambda request_handler_args: (request_handler_args['uri_fields'].get('email', None), 'email')),
'AddressesVARUserGET':          (lambda request_handler_args: (request_handler_args['uri_fields'].get('email', None), 'email')),
'AddressesVARUserPOST':         (lambda request_handler_args: (request_handler_args['uri_fields'].get('email', None), 'email')),
'AddressesVARUserPUT':          (lambda request_handler_args: (request_handler_args['uri_fields'].get('email', None), 'email')),
'AddressesVARVerifyPOST':       (lambda request_handler_args: (request_handler_args['uri_fields'].get('email', None), 'email')),
'ApidocsGET':                   None,
'DomainsGET':                   None,
'DomainsPOST':                  None,
'DomainsVARDELETE':             (lambda request_handler_args: (request_handler_args['uri_fields'].get('domain', None), 'domain')),
'DomainsVARListsGET':           (lambda request_handler_args: (request_handler_args['uri_fields'].get('domain', None), 'domain')),
'DomainsVAROwnersGET':          (lambda request_handler_args: (request_handler_args['uri_fields'].get('domain', None), 'domain')),
'DomainsVAROwnersDELETE':       (lambda request_handler_args: (request_handler_args['uri_fields'].get('domain', None), 'domain')),
'DomainsVAROwnersPOST':         (lambda request_handler_args: (request_handler_args['uri_fields'].get('domain', None), 'domain')),
'ListsGET':                     None,
'ListsPOST':                    (lambda request_handler_args: (request_handler_args['req'].params['fqdn_listname'], 'fqdn_listname')),
'ListsStylesGET':               None,
'ListsVARArchiversGET':         (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARArchiversPATCH':       (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARArchiversPUT':         (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARConfigGET':            (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARConfigPATCH':          (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARConfigPUT':            (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARConfigVARGET':         (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARConfigVARPATCH':       (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARDELETE':               (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARGET':                  (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARHeldGET':              (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARHeldVARGET':           (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARHeldVARPOST':          (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARRequestsGET':          (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARRequestsVARGET':       (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARRequestsVARPOST':      (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARRosterMemberGET':      (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARRosterModeratorGET':   (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'ListsVARRosterOwnerGET':       (lambda request_handler_args: (request_handler_args['uri_fields'].get('list_id', None), 'list_id')),
'MembersGET':                   None,
'MembersPOST':                  (lambda request_handler_args: (request_handler_args['req'].params['list_id'], 'list_id')),
'MembersVARAllPreferencesGET':  (lambda request_handler_args: (request_handler_args['uri_fields'].get('id', None), 'id')),
'MembersVARDELETE':             (lambda request_handler_args: (request_handler_args['uri_fields'].get('id', None), 'id')),
'MembersVARGET':                (lambda request_handler_args: (request_handler_args['uri_fields'].get('id', None), 'id')),
'MembersVARPATCH':              (lambda request_handler_args: (request_handler_args['uri_fields'].get('id', None), 'id')),
'MembersVARPreferencesGET':     (lambda request_handler_args: (request_handler_args['uri_fields'].get('id', None), 'id')),
'MembersVARPreferencesDELETE':  (lambda request_handler_args: (request_handler_args['uri_fields'].get('id', None), 'id')),
'MembersVARPreferencesPATCH':   (lambda request_handler_args: (request_handler_args['uri_fields'].get('id', None), 'id')),
'MembersVARPreferencesPUT':     (lambda request_handler_args: (request_handler_args['uri_fields'].get('id', None), 'id')),
'QueuesVARGET':                 (lambda request_handler_args: (request_handler_args['uri_fields'].get('queue_name', None), 'queue_name')),
'QueuesVARPOST':                (lambda request_handler_args: (request_handler_args['uri_fields'].get('queue_name', None), 'queue_name')),
'QueuesGET':                    (lambda request_handler_args: (request_handler_args['uri_fields'].get('queue_name', None), 'queue_name')),
'QueuesVARVARDELETE':           (lambda request_handler_args: (request_handler_args['uri_fields'].get('queue_name', None), 'queue_name')),
'SystemConfigurationGET':       None,
'SystemConfigurationVARGET':    None,
'SystemPreferencesGET':         None,
'SystemGET':                    None,
'UsersGET':                     None,
'UsersPOST':                    None,
'UsersVARAddressesGET':         (lambda request_handler_args: (request_handler_args['uri_fields'].get('user_identifier', None), 'user_identifier')),
'UsersVARAddressesPOST':        (lambda request_handler_args: (request_handler_args['uri_fields'].get('user_identifier', None), 'user_identifier')),
'UsersVARDELETE':               (lambda request_handler_args: (request_handler_args['uri_fields'].get('user_identifier', None), 'user_identifier')),
'UsersVARGET':                  (lambda request_handler_args: (request_handler_args['uri_fields'].get('user_identifier', None), 'user_identifier')),
'UsersVARLoginPOST':            (lambda request_handler_args: (request_handler_args['uri_fields'].get('user_identifier', None), 'user_identifier')),
'UsersVARPATCH':                (lambda request_handler_args: (request_handler_args['uri_fields'].get('user_identifier', None), 'user_identifier')),
'UsersVARPUT':                  (lambda request_handler_args: (request_handler_args['uri_fields'].get('user_identifier', None), 'user_identifier')),
'UsersVARPreferencesGET':       (lambda request_handler_args: (request_handler_args['uri_fields'].get('user_identifier', None), 'user_identifier')),
'UsersVARPreferencesPATCH':     (lambda request_handler_args: (request_handler_args['uri_fields'].get('user_identifier', None), 'user_identifier')),
'UsersVARPreferencesDELETE':    (lambda request_handler_args: (request_handler_args['uri_fields'].get('user_identifier', None), 'user_identifier')),
'UsersVARPreferencesPUT':       (lambda request_handler_args: (request_handler_args['uri_fields'].get('user_identifier', None), 'user_identifier')),
}


