# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import sys
import logging

from settings import config
from serve_spec_mailman_rest_api.backend_client_functions import *

logger = logging.getLogger('root.' + __name__)

def create_prod_mode_users():
    # when the system starts, we can create the users defined in the config file
    # useful for creating the first server_owner in the system
    for item in config.get('prod_mode_users', []):
        logger.debug('Creating user: {}'.format(item))
        mm_rest_api_users().create_or_update_user(item)

def create_test_mode_resources():
    # when the system starts, we need create the test server_owner user defined in the config file

    # the tests need to create a server_owner user for running the tests, but this is VERY insecure
    # so to run the tests you must set run_in_test_mode=True in settings.py
    # if run_in_test_mode is True, we create the test user. if not, we delete the test user.
    # that's all that run_in_test_mode does.
    if config.get('run_in_test_mode', None) == True:
        for _ in range(20):
            logger.critical('RUNNING IN TEST MODE')
        # create test serverowner
        if config.get('test_mode_serverowner', None):
            logger.critical('Creating TEST SERVEROWNER: {}'.format(config['test_mode_serverowner']))
            mm_rest_api_users().create_or_update_user(config['test_mode_serverowner'])
        else:
            logger.critical('settings.py does not define a test server_owner user to create')
            sys.exit(1)

def cleanup_test_mode_resources():
        # we are not in run_in_test_mode so remove test resources

        # delete the test server_owner user
        mm_rest_api_users().delete_user(config['test_mode_serverowner'])
        logger.info('Cleaning up test resources, deleting user: {}'.format(config['test_mode_serverowner']['email']))

        # delete the test users
        for item in config.get('test_mode_users', []):
            logger.info('Cleaning up test resources, deleting user: {}'.format(item['email']))
            mm_rest_api_users().delete_user(item)

        # delete test lists
        for item in config.get('test_mode_lists', []):
            logger.info('Cleaning up test resources, deleting list: {}'.format(item['list_id']))
            mm_rest_api_lists().delete_list(item)

        # delete test domains
        for item in config.get('test_mode_domains', []):
            logger.info('Cleaning up test resources, deleting domain: {}'.format(item['mail_host']))
            mm_rest_api_domains().delete_domain(item)


