# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import logging

import falcon
from requests import post
from requests.auth import _basic_auth_str

from serve_security.jsonwebtoken import json_web_token
from settings import config

logger = logging.getLogger('root.' + __name__)


def login(**request_handler_args):
    # this a request handler
    req = request_handler_args['req']
    uri_fields = request_handler_args['uri_fields']
    resp = request_handler_args['resp']

    # this LOGIN FUNCTION IS INSECURE UNLESS ON AN HTTPS CONNECTION!
    #try:
    user_identifier = uri_fields['id']
    #cleartext_password = form_fields['cleartext_password'][0]
    cleartext_password = req.params.get('cleartext_password', None)
    if not cleartext_password:
        # return a 401 Unauthorized if response status code is anything but 204
        raise falcon.HTTPUnauthorized('Unable to log in', 'Login failed. Check login id and password.')

    url_for_mailman_api = 'http://' + config['mailman_rest_api_netloc'] + '/3.0/users/{}/login'.format(user_identifier)
    auth_string = _basic_auth_str(config['basicauth_user'], config['basicauth_pass'])
    headers = { "Authorization" : auth_string}
    data = {'cleartext_password': cleartext_password}
    # try to log in to Mailman REST server
    r = post(url_for_mailman_api, headers=headers, data=data)
    logger.debug("User login. response code was: {}".format(r.status_code))
    if r.status_code != 204: # Mailman returns 204 if login succeeded
        # return a 401 Unauthorized if response status code is anything but 204
        raise falcon.HTTPUnauthorized('Unable to log in', 'Login failed. Check login id and password.')
    # login succeeded, add JWT to response
    json_web_token.new(resp, config['token_secret'], user_identifier=user_identifier,
                       token_expiration_seconds=config['token_expiration_seconds'])
    resp.status = falcon.HTTP_204 # Mailman returns 204 if login succeeded, we'll do the same

