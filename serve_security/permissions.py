# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import logging

from serve_spec_mailman_rest_api.backend_client_functions import *

logger = logging.getLogger('root.' + __name__)


class permissions():


    """
    Understanding permissions:

    Permissions are simple and consist of:
        a resource_id
        a user_identifier
        a role

    That's all! It's VERY simple, don't be freaked out.
    Any given permission has enough information to tell you if a given user has access to
    a given resource, and tells you what level of access (role) the user is allowed.

    The rules that work out exactly how permissions are interpreted are contained EXCLUSIVELY in the
    authorize object.

    ##############################
    The resources in Mailman:

    user
    domain
    member
    list
    server
    archive
    datastore

    ##############################
    The available roles for those resources:

    userowner
    domainowner
    serverowner
    listowner
    listmember
    listmoderator

    ##############################
    How/where each role is defined:

    userowner - in the Mailman database. The owner of the user is the user itself.
    domainowner - in the Mailman database.
    serverowner - in the Mailman database.
    listowner - in the Mailman database.
    listmember - in the Mailman database.
    listmoderator - in the Mailman database.

    """

    all_roles = ['domainowner', 'serverowner', 'userowner', 'listowner', 'listmember', 'listmoderator']

    role_to_resource_type_lookup = {
                'domainowner': 'domain',
                'serverowner': 'server',
                'userowner': 'user',
                'listowner': 'list',
                'listmember': 'list',
                'listmoderator': 'list',
    }

    def validate_params(self, role=None, resource_id=None, resource_type=None, user_identifier=None):

        valid = True
        reason={}
        if role not in permissions.all_roles:
            valid = None
            reason['role'] = 'Error: invalid role, must be one of {}'.format(permissions.all_roles)

        valid_resource_types = self.role_to_resource_type_lookup.values()
        if resource_type not in valid_resource_types:
            valid = None
            reason['resource_type'] = 'Error: resource_type must be one of {}'.format(set(valid_resource_types))

        if not resource_id:
            valid = None
            reason['resource_id'] = 'Error: resource_id is required'

        if not user_identifier:
            valid = None
            reason['user_identifier'] = 'Error: user_identifier is required'

        return valid, reason


    def query(self, role=None, resource_type=None, resource_id=None, user_identifier=None):
        # returns True if user, role and resource all match, or None

        # validate the params
        valid, reason = self.validate_params(role=role, resource_id=resource_id, resource_type=resource_type,
                                                user_identifier=user_identifier)
        if not valid:
            logger.debug('Error during permissions query: invalid parameters {}'.format(reason))
            return

        user_id = mm_rest_api_users().convert_user_identifier_to_valid_user_id(user_identifier=user_identifier)
        if not user_id:
            logger.debug('Error during permissions query: Mailman user could not be found')
            return

        if role == 'userowner':
            """
            In Mailman, a user_identifier is either an email address or the user_id

            Only the user has access to itself, so the provided resource_id and user_identifier must match.
            """

            return None

        elif role in ['listowner', 'listmoderator', 'listmember']:
            logger.debug('Checking for {} permission.'.format(role))
            print('Checking for {} permission.'.format(role))

            if '@' not in user_identifier:
                print('Error during permissions query: when querying lists, '
                         'user_identifier must be email address')
                logger.debug('Error during permissions query: when querying lists, '
                         'user_identifier must be email address')
                return

            role_map =  {
                            'listowner': 'owner',
                            'listmember': 'member',
                            'listmoderator': 'moderator',
                        }

            memberships = mm_rest_api_addresses().get_memberships_for_address(email=user_identifier)
            print('memberships')
            print(memberships)
            memberships = memberships.get('entries', [])
            for membership in memberships:
                if (membership['list_id'] == resource_id) and (membership['role'] == role_map[role]):
                    print('Permissions check succeeded {} {}'.format(resource_id, role))
                    logger.debug('Permissions check succeeded {} {}'.format(resource_id, role))
                    return True
            print('Checking for {} permission FAILED.'.format(role))
            logger.debug('Checking for {} permission FAILED.'.format(role))
            return None

        elif role == 'domainowner':
            logger.debug('Checking for domainowner permission.')
            domain_owners = mm_rest_api_domains().get_domain_owners(resource_id=resource_id)
            print(domain_owners)
            if not domain_owners:
                print('Failed domainowner permissions query: {}'.format(user_identifier))
                return None
            domain_owners = domain_owners.get('entries', [])
            for domain_owner in domain_owners:
                print(domain_owner)
                print(domain_owner['user_id'])
                print(user_id)
                if (int(domain_owner['user_id']) == int(user_id)):
                    logger.debug('Domain ownership check succeeded {} {}'.format(resource_id, user_id))
                    print('Domain ownership check succeeded {} {}'.format(resource_id, user_id))
                    return True
            logger.debug('Failed domainowner permissions query: {}'.format(user_identifier))
            print('Failed domainowner permissions query: {}'.format(user_identifier))
            return None

        elif role == 'serverowner':
            logger.debug('Checking for serverowner permission.')
            user = mm_rest_api_users().get_user(user_identifier=user_identifier)
            if user.get('is_server_owner', None) == True:
                return True
            logger.debug('Failed serverowner permissions query: {}'.format(user_identifier))
            return None

        #shouldn't be any way to get here, but since this is security code lets be explicit
        return None


