import logging
logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

from serve_spec_permissions.db_global import db_permissions
from serve_spec_permissions.model import Permission

#from client_spec_mailman_rest_api.client_addresses import AddressesVARUserGET
#from client_spec_mailman_rest_api.client_users import UsersVARAddressesGET, UsersPOST, UsersVARPATCH
from client_spec_mailman_rest_api.client_members import MembersPOST

from settings import config
from serve_spec_mailman_rest_api.backend_client_functions import *

class permissions():

    """
    This is the permissions object and provides a single object for adding, querying and deleting permissions.

    The permissions object gets and sets permissions whereever they reside.  Specifically, there is a
    local database of permissions, plus Mailman also has some permissions built into it for certain resources.
    The permissions object provides a single interface to all permissions.

    VERY IMPORTANT!!!!!!
    This permissions object DOES NOT implement the business logic that relates permissions to resources
    and does not decide if a user has authority to interact with a given resource in a given way. That is done
    in the authorize object.

    Effectively, this gives unrestrained access to modify the security of the entire system.  You have been warned.

    """

    """
    Understanding permissions:

    Permissions are simple and consist of:
        a resource_id
        a user_identifier
        a role

    That's all! It's VERY simple, don't be freaked out.
    Any given permission has enough information to tell you if a given user has access to
    a given resource, and tells you what level of access (role) the user is allowed.

    The rules that work out exactly how permissions are interpreted are contained EXCLUSIVELY in the
    authorize object.

    ##############################
    The resources in Mailman:

    user
    domain
    member
    list
    server
    archive
    datastore

    ##############################
    The available roles for those resources:

    userowner
    domainowner
    serverowner
    listowner
    listmember
    listmoderator

    ##############################
    How/where each role is defined:

    userowner - in the Mailman database. The owner of the user is the user itself.
    domainowner - in the permissions database.
    serverowner - in the permissions database.
    listowner - in the Mailman database.
    listmember - in the Mailman database.
    listmoderator - in the Mailman database.


    ##############################
    The REST API to modify permissions.

    Remember that the REST API for permission is subject to access control.

    GET      /permissions/{role}/{resource_id}/{user_identifier}
    POST     /permissions/{role}/{resource_id}/{user_identifier}
    DELETE   /permissions/{role}/{resource_id}/{user_identifier}

    params = {'user_identifier': user_identifier}

    user_identifier is the mailman_user_id
    resource_id is whatever mailman uses to identify the resource
    """

    all_roles = ['domainowner', 'serverowner', 'userowner', 'listowner', 'listmember', 'listmoderator']

    role_to_resource_type_lookup = {
                'domainowner': 'domain',
                'serverowner': 'server',
                'userowner': 'user',
                'listowner': 'list',
                'listmember': 'list',
                'listmoderator': 'list',
    }

    def validate_params(self, role=None, resource_id=None, resource_type=None, user_identifier=None):

        print('role')
        print(role)
        print('resource_id')
        print(resource_id)
        print('resource_type')
        print(resource_type)
        print('user_identifier')
        print(user_identifier)

        valid = True
        reason={}
        if role not in permissions.all_roles:
            valid = None
            reason['role'] = 'Error: invalid role, must be one of {}'.format(permissions.all_roles)

        valid_resource_types = self.role_to_resource_type_lookup.values()
        if resource_type not in valid_resource_types:
            valid = None
            reason['resource_type'] = 'Error: resource_type must be one of {}'.format(set(valid_resource_types))

        if not resource_id:
            valid = None
            reason['resource_id'] = 'Error: resource_id is required'

        if not user_identifier:
            valid = None
            reason['user_identifier'] = 'Error: user_identifier is required'

        # if the requested resource does not exist in Mailman, return None
        if not self.check_exists(resource_type=resource_type, identifier=resource_id):
            valid = None
            reason['check_resource_exists'] = 'Error: resource_id {} does not exist'.format(resource_id)

        # if the requested user does not exist in Mailman, return None
        if not self.check_exists(resource_type='user', identifier=user_identifier):
            valid = None
            reason['check_user_exists'] = 'Error: user_identifier {} does not exist'.format(user_identifier)

        return valid, reason

    def check_exists(self, resource_type=None, identifier=None):
        # we should check if resources actually exist in Mailman before modifying our permissions data
        if not resource_type:
            return None
        logger.debug('Checking resource_type {} resource_id {}'.format(resource_type, identifier))
        if resource_type == 'domain':
            return mm_rest_api_domains().does_domain_exist(resource_id=identifier)
        elif resource_type == 'list':
            return mm_rest_api_lists().does_list_exist(resource_id=identifier)
        elif resource_type == 'user':
            return mm_rest_api_users().does_user_exist(user_identifier=identifier)
        elif resource_type == 'server':
            # server always exists
            return True
        return None

    def set(self, role=None, resource_id=None, resource_type=None, user_identifier=None):
        # this is the meat of where permissions are set

        # validate the params
        valid, reason = self.validate_params(role=role,
                                             resource_id=resource_id,
                                             resource_type=resource_type,
                                             user_identifier=user_identifier)
        if not valid:
            logger.debug('Error during permissions query: invalid parameters {}'.format(reason))
            return

        logger.debug('Set permission role: {} resource_id: {} resource_type: {} user_identifier: {}'.format(role,
                                                                                      resource_id,
                                                                                      resource_type,
                                                                                      user_identifier))

        # if this permission level is already set, return success
        if self.query(role=role, resource_id=resource_id, resource_type=resource_type, user_identifier=user_identifier):
            return True

        if role == 'userowner':
            # It does not make sense to allow changes to the userowner role. The user itself is the only userowner.
            # this code is here to ensure we have code handling every role
            logger.debug('Error: it is not possible to modify userowner permission')
            return None

        elif role in ['listowner', 'listmember', 'listmoderator']:
            result = self._add_member_to_list_with_role(user_identifier=user_identifier, role=role, \
                                                       resource_id=resource_id)
            if result:
                return True
            return None

        elif role == 'domainowner':
            self._save_permission_to_database(user_identifier=user_identifier, role='domainowner', \
                                              resource_id=resource_id, resource_type=resource_type)
            # failure to write the record to the database should result in an exception so no need for code to return None
            return True

        elif role == 'serverowner':
            self._save_permission_to_database(user_identifier=user_identifier, role='serverowner', \
                                              resource_id=resource_id, resource_type=resource_type)
            # failure to write the record to the database should result in an exception so no need for code to return None
            return True

        #shouldn't be any way to get here, but since this is security code lets be explicit
        return None

    def _add_member_to_list_with_role(self, user_identifier=None, role=None, resource_id=None, resource_type=None):
        """
        This is effectively duplicating functionality that can be done directly through the Mailman REST API,
        but here it is being done through a consistent permissions interface. Not sure if that's a good idea
        but doing it anyway....... It gives a consistent interface to permissions, but how important is that?

        For future reference, how list permissions work, from Barry:

        A member represents a subscription to a mailing list, so the role is read-only.  You don't change a
        list owner into a regular member, etc.  Instead you simply subscribe with the new role and unsubscribe
        the member if that role is removed.  A user/address can be subscribed multiple times to the same
        mailing list, but only once per role.
        """

        resource_type='list'

        # validate the params
        valid, reason = self.validate_params(role=role, resource_id=resource_id, resource_type=resource_type,
                                                user_identifier=user_identifier)
        if not valid:
            logger.debug('Error during permissions query: invalid parameters {}'.format(reason))
            return


        data = {'list_id': resource_id, 'subscriber': user_identifier, 'role': role}
        response = MembersPOST.client_request(urlvars_dict=user_identifier, netloc=config['mailman_rest_api_netloc'], \
                                              data=data, headers=config['basicauthheaders'])
        """
        Either of these responses indicate the outcome we need.
        HTTP/1.0 409 Conflict (this means that the user is already subscribed with this role, which is fine)
        HTTP/1.0 201 Created
        """
        if response.status_code == (201 or 409):
            return True

    def _save_permission_to_database(self, user_identifier=None, role=None, resource_id=None, resource_type=None):
        # user_identifier = convert_user_identifier_to_user_id(user_identifier)
        user_id = mm_rest_api_users().convert_user_identifier_to_valid_user_id(user_identifier=user_identifier)
        user_id = str(user_id)
        if not user_id:
            logger.debug('Error: Mailman user not found: {}'.format(user_id))
            return
        row = Permission(user_id = user_id, role=role, resource_id=resource_id,
                         resource_type=resource_type)
        if self._query_database(role='domainowner', user_identifier=user_id, resource_id=resource_id):
            # record exists in database already, do not duplicate
            return True
        db_permissions.session.add(row)
        db_permissions.session.commit()
        return True

    def query(self, role=None, resource_type=None, resource_id=None, user_identifier=None):
        # returns True if user, role and resource all match, or None

        # validate the params
        valid, reason = self.validate_params(role=role, resource_id=resource_id, resource_type=resource_type,
                                                user_identifier=user_identifier)
        if not valid:
            logger.debug('Error during permissions query: invalid parameters {}'.format(reason))
            return

        user_id = mm_rest_api_users().convert_user_identifier_to_valid_user_id(user_identifier=user_identifier)
        if not user_id:
            logger.debug('Error during permissions query: Mailman user could not be found')
            return

        if role == 'userowner':
            """
            In Mailman, a user_identifier is either an email address or the user_id

            Only the user has access to itself, so the provided resource_id and user_identifier must match.
            """

            return None

        elif role in ['listowner', 'listmoderator', 'listmember']:
            logger.debug('Checking for {} permission.'.format(role))

            if '@' not in user_identifier:
                logger.debug('Error during permissions query: when querying lists, user_identifier must be email address')
                return

            role_map =  {
                            'listowner': 'owner',
                            'listmember': 'member',
                            'listmoderator': 'moderator',
                        }

            memberships = mm_rest_api_addresses().get_memberships_for_address(email=user_identifier)
            memberships = memberships.get('entries', [])
            for membership in memberships:
                logger.debug('Checking for {} permission, membership info: {}.'.format(role, membership))
                if (membership['list_id'] == resource_id) and (membership['role'] == role_map[role]):
                    logger.debug('Permissions check succeeded {} {}'.format(resource_id, role))
                    logger.debug('Checking for {} permission SUCCEEDED.'.format(role))
                    return True
            logger.debug('Checking for {} permission FAILED.'.format(role))
            return None

        elif role == 'domainowner':
            # select from permissions database where role = domainowner and user=user_identifier and resource_id=resource_id
            logger.debug('Checking for domainowner permission.')
            if self._query_database(role='domainowner', user_identifier=user_id, resource_id=resource_id):
                return True
            logger.debug('Check for domainowner permission....FAILED')
            return None

        elif role == 'serverowner':
            # select from permissions database where role = serverowner and user=user_identifier and resource_id=resource_id
            logger.debug('Checking for serverowner permission.')
            user = mm_rest_api_users().get_user(user_identifier=user_identifier)
            if user.get('is_server_owner') == True:
            if self._query_database(role='serverowner', user_identifier=user_id, resource_id='server'):
                return True
            logger.debug('Check for serverowner permission....FAILED')
            return None

        #shouldn't be any way to get here, but since this is security code lets be explicit
        return None

    def _query_database(self, user_identifier=None, role=None, resource_id=None):
        user_id = mm_rest_api_users().convert_user_identifier_to_valid_user_id(user_identifier=user_identifier)
        user_id = str(user_id)
        resource_type = self.role_to_resource_type_lookup[role]
        result = db_permissions.session.query(Permission).\
            filter_by(role=role, user_id=user_id, resource_id=resource_id, resource_type=resource_type).first()
        #result = db_permissions.session.query(Permission).all()
        logger.debug('Querying permissions database, params {} {} {} {}'.format(role, user_id, resource_id, resource_type))
        logger.debug('Querying permissions database, result is {}'.format(result))
        return result

    def delete(self, role=None, resource_type=None, resource_id=None, user_identifier=None):
        # deletes a permission

        # validate the params
        valid, reason = self.validate_params(role=role, resource_id=resource_id, resource_type=resource_type,
                                                user_identifier=user_identifier)
        if not valid:
            logger.debug('Error during permissions query: invalid parameters {}'.format(reason))
            return

        user_id = mm_rest_api_users().convert_user_identifier_to_valid_user_id(user_identifier=user_identifier)

        if role == 'userowner':
            # It does not make sense to allow changes to the userowner role. The user itself is the only userowner.
            # this code is here to ensure we have code handling every role
            logger.debug('Error: it is not possible to delete userowner permission')
            return None

        elif role in ['listowner', 'listmoderator', 'listmember']:
            logger.debug('List permissions must be modified by unsubscribing user from the list.')
            return None

        elif role == 'domainowner':
            # delete from permissions database where role = domainowner and user=user_identifier and resource_id=resource_id
            logger.debug('Deleting domainowner permissions from database.')
            self._delete_permissions_from_database(role='domainowner', user_identifier=user_id, resource_id=resource_id)
            return True

        elif role == 'serverowner':
            # delete from permissions database where role = serverowner and user=user_identifier and resource_id=resource_id
            logger.debug('Deleting serverowner permissions from database.')
            self._delete_permissions_from_database(role='serverowner', user_identifier=user_id, resource_id='server')
            return True


        #shouldn't be any way to get here, but since this is security code lets be explicit
        return None

    def _delete_permissions_from_database(self, user_identifier=None, role=None, resource_id=None):
        user_id = mm_rest_api_users().convert_user_identifier_to_valid_user_id(user_identifier=user_identifier)
        resource_type = self.role_to_resource_type_lookup[role]
        db_permissions.session.query(Permission).\
            filter_by(role=role, user_id=user_id, resource_id=resource_id, resource_type=resource_type).delete()
