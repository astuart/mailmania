# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

from serve_spec_mailman_rest_api.backend_client_functions import *
from serve_security.permissions import permissions


class authorize():

    """
    This handles all authorization logic.

    The permissions object provides a single object for adding, querying and deleting permissions.
    The permissions object gets and sets permissions whereever they reside.  Specifically, there is a
    local database of permissions, plus Mailman also has some permissions built into it for certain resources.
    The permissions object provides a single interface to all permissions.

    HOWEVER the permissions object does not implement the business logic that relates permissions to resources
    and does not decide if a user has authority to interact with a given resource in a given way. That is done
    in the authorize object.
    """

    def __init__(self):
        self.permissions = permissions()

    def is_user_userowner(self, resource_id=None, user_identifier=None):
        if not user_identifier:
            return
        if not resource_id:
            return
        return mm_rest_api_users().compare_resource_id_to_user_identifier(resource_id=resource_id, \
                                                                      user_identifier=user_identifier)

    def is_user_serverowner(self, user_identifier=None):
        if not user_identifier:
            return
        if self.permissions.query(role='serverowner', resource_id='server', \
                                  user_identifier=user_identifier, resource_type='server'):
            return True

    def is_user_domainowner(self, user_identifier=None, domain_id=None):
        if not (user_identifier and domain_id):
            return None
        print('Checking is_user_domainowner')
        if self.permissions.query(role='domainowner', resource_id=domain_id, \
                                  user_identifier=user_identifier, resource_type='domain'):
            return True

    def is_user_listowner(self, user_identifier=None, list_id=None):
        if not user_identifier:
            return None

        if not list_id:
            return None

        if self.permissions.query(role='listowner', resource_id=list_id, \
                                  user_identifier=user_identifier, resource_type='list'):
            return True

    def is_user_listmoderator(self, user_identifier=None, list_id=None):
        if not user_identifier:
            return None

        if not list_id:
            return None

        if self.permissions.query(role='listmoderator', resource_id=list_id, \
                                  user_identifier=user_identifier, resource_type='list'):
            return True

    def is_user_listmember(self, user_identifier=None, list_id=None):
        if not user_identifier:
            return None

        if not list_id:
            return None

        if self.permissions.query(role='listmember', resource_id=list_id, \
                                  user_identifier=user_identifier, resource_type='list'):
            return True

    def is_user_listnonmember(self, user_identifier=None, list_id=None):
        if not user_identifier:
            return None

        if not list_id:
            return None

        if self.permissions.query(role='listnonmember', resource_id=list_id, \
                                  user_identifier=user_identifier, resource_type='list'):
            return True




