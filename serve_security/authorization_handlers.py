# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import falcon

from serve_security.jsonwebtoken import json_web_token
from settings import config

#from mailman_auth_proxy.server.operations_map import operation_handlers
from serve_spec_mailman_rest_api.operations_resource_locators import operation_resource_locators
#from serve_spec_permissions.operations_resource_locators import operation_resource_locators
from serve_spec_mailman_rest_api.backend_client_functions import *
from serve_security.authorize import authorize
import logging

logger = logging.getLogger('root.' + __name__)


##################################################
######################AUTHORIZATION HANDLER HELPERS

def extract_resource_id_from_request(**request_handler_args):
    operation_id = request_handler_args['operation_id']
    resource_id = operation_resource_locators[operation_id](request_handler_args)
    if not resource_id:
        raise falcon.HTTPUnauthorized('Unauthorized.', 'Not able to identify the resource being requested.')
    return resource_id

def get_requesting_user_id_from_jwt(**request_handler_args):
    req = request_handler_args['req']
    requesting_user_id, sessiondata = json_web_token.get(req, config['token_secret'])
    if not requesting_user_id:
        raise falcon.HTTPUnauthorized('Unauthorized.', 'Not able to identify the requesting user_id.')
    return requesting_user_id



###################################################
#############################AUTHORIZATION HANDLERS

def requires_userowner(**request_handler_args):
    """
    This is an authorization request handler. Must return True to indicate that authorization succeeded, otherwise None.

    It checks that the user_identifier from the JWT == the requested user_identifer
    """

    requesting_user_id = get_requesting_user_id_from_jwt(**request_handler_args)
    requested_resource_id, resource_locator_type = extract_resource_id_from_request(**request_handler_args)
    logger.debug("Authorization requires_userowner: requested_resource_id {} resource_locator_type {}".format(requested_resource_id, resource_locator_type))
    if resource_locator_type == 'email':
        requested_resource_id = requested_resource_id
        logger.debug("requires_userowner: resource_locator_type {} requested_resource_id {}".format(resource_locator_type, requested_resource_id))
    elif resource_locator_type == 'id':
        # id is list member id so we have to look up its corresponding email address
        response_data = mm_rest_api_members().get_member(member_id=requested_resource_id)
        if response_data:
            email = response_data.get('email', None)
            if email:
                requested_resource_id = email
    if not requested_resource_id:
        # invalid resource locator type
        logger.debug("Authorization requires userowner failed, requested_resource_id is None")
        return  None

    logger.debug("Authorization requires_userowner: requesting_user_id {} requested_resource_id {}".format(requesting_user_id, requested_resource_id))
    if authorize().is_user_userowner(resource_id=requested_resource_id, user_identifier=requesting_user_id):
        return True
    logger.debug("Authorization requires userowner failed")

def requires_serverowner(**request_handler_args):
    """
    This is an authorization request handler. Must return True to indicate that authorization succeeded, otherwise None.

    It checks that the user_identifier from the JWT is present in the permissions database and role=serverowner
    """

    requesting_user_id = get_requesting_user_id_from_jwt(**request_handler_args)
    logger.debug("Authorization requires_serverowner: requesting_user_id {}".format(requesting_user_id))
    if authorize().is_user_serverowner(user_identifier=requesting_user_id):
        return True
    logger.debug("Authorization requires serverowner failed")

def requires_domainowner(**request_handler_args):
    """
    This is an authorization request handler. Must return True to indicate that authorization succeeded, otherwise None.

    It checks that the user_identifier from the JWT is present in the permissions database and role=domainowner
    """

    requesting_user_id = get_requesting_user_id_from_jwt(**request_handler_args)
    requested_resource_id, resource_locator_type = extract_resource_id_from_request(**request_handler_args)
    logger.debug("resource_locator_type: {} requested_resource_id: {}".format(resource_locator_type, requested_resource_id))
    # if the requested resource is actually a fqdn_listname i.e. a mailing list email address

    if resource_locator_type == 'domain':
        requested_domain = requested_resource_id
    if resource_locator_type == 'list_id':
        requested_domain = mm_rest_api_lists().get_list_domain(resource_id=requested_resource_id)
    if resource_locator_type == 'fqdn_listname':
        listname, requested_domain = requested_resource_id.split('@', 1)
    logger.debug("Authorization requires_domainowner: requesting_user_id: {} requested_domain: {}".\
             format(requesting_user_id, requested_domain))
    print("Authorization requires_domainowner: requesting_user_id: {} requested_domain: {}".\
             format(requesting_user_id, requested_domain))
    if authorize().is_user_domainowner(user_identifier=requesting_user_id, domain_id=requested_domain):
        print("Authorization requires_domainowner succeeded")
        return True
    logger.debug("Authorization requires domainowner failed")

def requires_listowner(**request_handler_args):
    """
    This is an authorization request handler. Must return True to indicate that authorization succeeded, otherwise None.

    It checks that the user_identifier from the JWT is present in the permissions database and role=listowner
    """

    requesting_user_id = get_requesting_user_id_from_jwt(**request_handler_args)
    requested_resource_id, resource_locator_type = extract_resource_id_from_request(**request_handler_args)
    logger.debug("resource_locator_type: {} requested_resource_id: {}".format(resource_locator_type, requested_resource_id))

    if resource_locator_type == ('list_id' or 'fqdn_listname'):
        requested_list = requested_resource_id
    else:
        logger.debug("Authorization requires listowner failed, fqdn_listname or list_id not found")
        return None
    logger.debug("Authorization listowner: requesting_user_id: {} requested_list: {}".\
             format(requesting_user_id, requested_list))
    if authorize().is_user_listowner(user_identifier=requesting_user_id, list_id=requested_list):
        logger.debug("Authorization requires listowner succeeded")
        return True
    logger.debug("Authorization requires listowner failed")


def requires_listmoderator(**request_handler_args):
    """
    This is an authorization request handler. Must return True to indicate that authorization succeeded, otherwise None.

    It checks that the user_identifier from the JWT is present in the permissions database and role=listmoderator
    """

    requesting_user_id = get_requesting_user_id_from_jwt(**request_handler_args)
    requested_resource_id, resource_locator_type = extract_resource_id_from_request(**request_handler_args)
    logger.debug("resource_locator_type: {} requested_resource_id: {}".format(resource_locator_type, requested_resource_id))

    if resource_locator_type == ('list_id' or 'fqdn_listname'):
        requested_list = requested_resource_id
    else:
        logger.debug("Authorization requires listmoderator failed, fqdn_listname or list_id not found")
        return None
    logger.debug("Authorization listowner: requesting_user_id: {} requested_list: {}".\
             format(requesting_user_id, requested_list))
    if authorize().is_user_listmoderator(user_identifier=requesting_user_id, list_id=requested_list):
        logger.debug("Authorization requires listmoderator succeeded")
        return True
    logger.debug("Authorization requires listmoderator failed")

def requires_listmember(**request_handler_args):
    """
    This is an authorization request handler. Must return True to indicate that authorization succeeded, otherwise None.

    It checks that the user_identifier from the JWT is present in the permissions database and role=listmember
    """
    print('requires_listmember')
    requesting_user_id = get_requesting_user_id_from_jwt(**request_handler_args)
    requested_resource_id, resource_locator_type = extract_resource_id_from_request(**request_handler_args)
    logger.debug("resource_locator_type: {} requested_resource_id: {}".format(resource_locator_type, requested_resource_id))
    print("resource_locator_type: {} requested_resource_id: {}".format(resource_locator_type, requested_resource_id))

    if resource_locator_type == ('list_id' or 'fqdn_listname'):
        requested_list = requested_resource_id
    else:
        logger.debug("Authorization requires listmember failed, fqdn_listname or list_id not found")
        return None
    print("Authorization listowner: requesting_user_id: {} requested_list: {}".\
             format(requesting_user_id, requested_list))
    logger.debug("Authorization listowner: requesting_user_id: {} requested_list: {}".\
             format(requesting_user_id, requested_list))
    if authorize().is_user_listmember(user_identifier=requesting_user_id, list_id=requested_list):
        print("Authorization requires listmember succeeded")
        logger.debug("Authorization requires listmember succeeded")
        return True
    logger.debug("Authorization requires listmember failed")
    print("Authorization requires listmember failed")



