# Copyright (C) 2006-2015 by the Free Software Foundation, Inc.
#
#
# This is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this software.  If not, see <http://www.gnu.org/licenses/>.

import jwt
from datetime import datetime, timedelta
import falcon
import logging

logger = logging.getLogger('root.' + __name__)

class json_web_token():


    @staticmethod
    # given a user identifier, this will add a new token to the response
    # Typically you would call this from within your login function, after the back end has OK'd the username/password
    def new(resp, token_secret, user_identifier=None, session_data=None, token_expiration_seconds=None):
        # add a JSON web token to the response headers
        if not user_identifier:
            raise Exception('Empty user_identifer passed to set JWT')
        logger.debug("Creating new JWT, user_identifier is: {}".format(user_identifier))
        token = jwt.encode({'user_identifier': user_identifier,
                            'exp': datetime.utcnow() + timedelta(seconds=token_expiration_seconds)},
                           token_secret,
                           algorithm='HS256').decode("utf-8")
        resp.set_header('X-Auth-Token', token)


    @staticmethod
    # given a request containing a VALID token, this will  a refreshed token to the response
    # This is how you would either add to the user's session_data or this is how you would refresh token expiration
    # existing session data is retained but overwritten by new values in the session_data dict, if any
    def update(req, resp, token_secret, session_data=None, token_expiration_seconds=2419200):
        token = req.get_header('X-Auth-Token')
        user_identifier, session_data_from_token = json_web_token.get(req, token_secret)
        session_data_from_token.update(session_data)
        # add a JSON web token to the response headers
        token = jwt.encode({'user_identifier': user_identifier,
                            'exp': datetime.utcnow() + timedelta(seconds=token_expiration_seconds)},
                           token_secret,
                           algorithm='HS256').decode("utf-8")
        resp.set_header('X-Auth-Token', token)


    @staticmethod
    def get(req, token_secret):
        # tries to get token data, returns None if token not valid
        try:
            token = req.get_header('X-Auth-Token')
            logger.debug("X-Auth-Token is: {}".format(token))
            token_contents = jwt.decode(token, token_secret, algorithm='HS256')
            logger.debug("token_contents is: {}".format(token_contents))
            # in the next line, we use dict get for the session data because its optional
            # we do not use dict get for the user identifier because its required and SHOULD raise an exception
            return token_contents['user_identifier'], token_contents.get('session_data', None)
        except:
            return None


    @staticmethod
    def validate(**request_handler_args):
        # this is a request handler
        #
        # tries to validate token, returns True if token valid
        req = request_handler_args['req']
        config = request_handler_args['config']

        try:
            token = req.get_header('X-Auth-Token')
            jwt.decode(token, config['token_secret'], algorithm='HS256')
            return True
        except:
            logger.debug('The provided JSON web token is not valid')
            raise falcon.HTTPUnauthorized('JWT validation error', 'The provided JSON web token is not valid.')
        return None
